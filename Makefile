PATH:=${PATH}:node_modules/.bin

all: run

version:
	@echo $(shell grep '"version":' package.json | cut -d\" -f4)

install:
	npm install --no-optional

list-installed:
	npm list --depth 0

lint:
	eslint *.js src

unittest:
	jest --verbose
	@echo "Full report in browser: file://${PWD}/coverage/index.html"

unittest-update:
	jest -u

unittest-watch:
	jest --watch

coverage:
	jest --coverageReporters text html
	@echo "Full report in browser: file://${PWD}/coverage/index.html"

test: lint unittest

run-android: test
	react-native run-android

reload-android:
	adb shell input text "RR"

start:
	react-native start

apk:
	cd android && ./gradlew assembleRelease
	test -d dist || mkdir dist
	mv android/app/build/outputs/apk/release/app-release.apk ./dist/ylitse.apk

clean:
	find . -type f -name '*~' -exec rm -f {} \;
	cd android && ./gradlew clean
	rm -rf android/build android/.gradle
	rm -rf $${TMPDIR:-/tmp}/metro-bundler-cache-*
	rm -rf $${TMPDIR:-/tmp}/react-native-packager-cache-*
	rm -rf coverage
	rm -rf dist
	jest --clearCache

.PHONY: all version install list-installed lint unittest unittest-update unittest-watch coverage test run-android reload-android start clean
