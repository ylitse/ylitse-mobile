import { AppRegistry, YellowBox } from 'react-native';
import Root from './src/Root';

// temporary solution, more info:
// https://github.com/react-navigation/react-navigation/issues/3956
YellowBox.ignoreWarnings(
    ['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader'],
);

AppRegistry.registerComponent('ylitse', () => Root);
