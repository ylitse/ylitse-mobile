import { Dimensions } from 'react-native';

const { width } = Dimensions.get('window');

/** Base number for all absolute dimensions: font sizes, paddings etc.
 * Maps to nice integers across common screen sizes:
 * - 480p  -> 24
 * - 720p  -> 36
 * - 1080p -> 54
 * - 1440p -> 72
 */
const BASELINE = width / 20;

/** Constant set of colors to be used in the app */
export const colors = {
    GREEN_PRIMARY: '#76B856',
    GREEN_SECONDARY: '#E8F2DF',
    ORANGE: '#F7931E',
    BEIGE: '#FDEDDD',
    YELLOW: '#FFC952',
    GREY_VERY_LIGHT: '#F4F5F5',
    GREY_LIGHT: '#F0F1F1',
    GREY_10: '#E6E7E8',
    GREY_30: '#C0C0C0',
    GREY_50: '#888888',
    GREY_DARK: '#505050',
    WHITE: '#FFFFFF',
};

/** Fontset */
export const fonts = {
    ITALIC: 'Lato-Italic',
    TITLE: 'Paprika-Regular',
    PRIMARY: 'Lato-Regular',
};

/** Responsive font sizes */
export const fontSizes = {
    HEADING_1: BASELINE * 3,
    HEADING_2: BASELINE * 2,
    HEADING_3: BASELINE * (6 / 4),
    HEADING_4: BASELINE * (5 / 4),
    BODY_TEXT: BASELINE * (3 / 4),
    SMALL: BASELINE * (1 / 2),
};

/** Responsive spacing tools */
export const spacings = {
    1: BASELINE * (1 / 2),
    2: BASELINE * (3 / 4),
    3: BASELINE,
    4: BASELINE * (5 / 4),
    5: BASELINE * (6 / 4),
    6: BASELINE * 2,
    7: BASELINE * (5 / 2),
    8: BASELINE * 3,
};

