import React from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';

import rootReducer from './reducers';
import Navigation from './Navigation';

const store = createStore(rootReducer, compose(applyMiddleware(thunk)));

const Root = () => (
    <Provider store={store}>
        <Navigation />
    </Provider>
);

export default Root;
