import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { View, BackHandler } from 'react-native';
import firebase from 'react-native-firebase';

import LoginScreen from './containers/LoginScreen';
import RegisterScreen from './containers/RegisterScreen';
import RegisterPrivacyScreen from './containers/RegisterPrivacyScreen';
import RegisterEmailScreen from './containers/RegisterEmailScreen';
import RegisterDisplayNameScreen
    from './containers/RegisterDisplayNameScreen';
import MentorsScreen from './containers/MentorsScreen';
import MentorProfileScreen from './containers/MentorProfileScreen';
import MyProfileScreen from './containers/MyProfileScreen';
import ContactListScreen from './containers/ContactListScreen';
import ChattingScreen from './containers/ChattingScreen';

import { back } from './actions/navigation';
import { tryRestoreLogin } from './actions/login';

class Navigation extends React.Component {
    screens = {
        MentorList: () => <MentorsScreen />,
        Login: () => <LoginScreen />,
        Register: () => <RegisterScreen />,
        RegisterDisplayName: () => <RegisterDisplayNameScreen />,
        RegisterEmail: () => <RegisterEmailScreen />,
        RegisterPrivacy: () => <RegisterPrivacyScreen />,
        LoggedInMentorList: () => <MentorsScreen />,
        MentorProfile: () => <MentorProfileScreen />,
        MyProfile: () => <MyProfileScreen />,
        ContactList: () => <ContactListScreen />,
        Chatting: () => <ChattingScreen />,
    };

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', () => {
            const {
                history: { length },
                goBack,
                dropdown,
                closeDropdown,
            } = this.props;
            if (dropdown) {
                closeDropdown();
                return true;
            }
            if (length) {
                goBack();
                return true;
            }
            return false;
        });
        const { restoreLogin } = this.props;
        restoreLogin();

        firebase.messaging().hasPermission()
            .then((enabled) => {
                if (!enabled) {
                    firebase.messaging().requestPermission().then(
                        () => {},
                        () => {},
                    );
                }
            });
    }

    render() {
        const { screen } = this.props;
        const { [screen]: definition } = this.screens;
        return (
            <View style={{ flex: 1 }}>
                {definition()}
            </View>
        );
    }
}

Navigation.propTypes = {
    goBack: PropTypes.func.isRequired,
    restoreLogin: PropTypes.func.isRequired,
    screen: PropTypes.string.isRequired,
    history: PropTypes.arrayOf(PropTypes.shape({
        screen: PropTypes.string.isRequired,
        title: PropTypes.string.isRequired,
        left: PropTypes.string.isRequired,
        right: PropTypes.string.isRequired,
        headerVisible: PropTypes.bool.isRequired,
    })).isRequired,
    dropdown: PropTypes.bool.isRequired,
};

export default connect(
    ({
        navigation: {
            dropdown,
            history,
            currentScreen: {
                screen,
                headerVisible,
            },
        },
    }) => ({
        headerVisible,
        dropdown,
        history,
        screen,
    }),
    dispatch => ({
        goBack: () => dispatch(back()),
        restoreLogin: () => dispatch(tryRestoreLogin()),
    }),
)(Navigation);
