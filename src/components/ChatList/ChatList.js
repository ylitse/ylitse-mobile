import React from 'react';
import { ScrollView } from 'react-native';
import PropTypes from 'prop-types';

import I18n from '../../i18n';
import Modal from '../Modal';
import ChatListItem from '../ChatListItem';

import { FaintText } from '../Text';
import { HeaderScreenContainer } from '../Container';
import { VSpacer } from '../Spacer';

class ChatList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            selectedBuddyId: '',
        };
    }

    openModal = ({ id }) => this.setState({
        selectedBuddyId: id,
        modalVisible: true,
    })

    closeModal = () => this.setState({
        modalVisible: false,
        selectedBuddyId: '',
    })

    deleteConversation = () => {
        const { selectedBuddyId } = this.state;
        const { deleteConversation } = this.props;
        deleteConversation(selectedBuddyId);
        this.closeModal();
    }

    render() {
        const {
            buddies,
            unseen,
            sortedIds,
            goToConversation,
        } = this.props;
        const { modalVisible } = this.state;

        const chatList = (
            <ScrollView style={{ width: '100%' }}>
                {sortedIds.filter(x => x in buddies).map((id) => {
                    const { [id]: buddy } = buddies;
                    const yellowDot = id in unseen;
                    return (
                        <ChatListItem
                            key={id}
                            goToConversation={goToConversation}
                            buddy={buddy}
                            yellowDot={yellowDot}
                            openModal={this.openModal}
                        />
                    );
                })}
            </ScrollView>
        );

        const emptyList = (
            <React.Fragment>
                <VSpacer size={4} />
                <FaintText>{I18n.t('chatListEmpty')}</FaintText>
            </React.Fragment>
        );

        return (
            <HeaderScreenContainer>
                {
                    modalVisible
                    && (
                        <Modal
                            message={I18n.t('areYouSureRemoveConversation')}
                            type="yes-no"
                            handleYes={this.deleteConversation}
                            closeModal={this.closeModal}
                        />
                    )
                }
                {
                    sortedIds.length === 0
                        ? emptyList
                        : chatList
                }
            </HeaderScreenContainer>
        );
    }
}

ChatList.propTypes = {
    goToConversation: PropTypes.func.isRequired,
    deleteConversation: PropTypes.func.isRequired,
    unseen: PropTypes.objectOf(PropTypes.string).isRequired,
    sortedIds: PropTypes.arrayOf(PropTypes.string).isRequired,
    buddies: PropTypes.objectOf(PropTypes.shape({
        displayName: PropTypes.string.isRequired,
        id: PropTypes.string.isRequired,
    })).isRequired,
};

export default ChatList;
