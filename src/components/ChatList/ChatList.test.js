import React from 'react';
import 'react-native';
import { shallow } from 'enzyme';

import ChatList from './ChatList';

describe('ChatList', () => {
    const props = {
        messages: {
            asdf: [{
                id: 'bb',
                isOurs: false,
                opened: true,
                created: (new Date(2147483647)).toISOString(),
                content: 'Message 1 content goes here.',
            }],
        },
        buddies: [
            {
                displayName: 'janteri',
                id: 'asdf',
            },
        ],
        sortedIds: ['asdf'],
        unseen: {},
        goToConversation: jest.fn(),
    };

    test('renders correctly', () => {
        const wrapper = shallow(<ChatList {...props} />);

        expect(wrapper).toMatchSnapshot();
    });

    test('renders with not opened message', () => {
        const cs = {
            cc: {
                messages: [{
                    id: 'bb',
                    isOurs: false,
                    opened: false,
                    date: (new Date(2147483647)),
                    content: 'Message 1 content goes here.',
                }],
            },
            date: new Date(2147483647),
            isAllOpened: false,
        };

        const wrapper = shallow(
            <ChatList
                {...props}
                conversations={cs}
                messages={{}}
                buddies={{}}
                unseen={{}}
                sortedIds={[]}
            />,
        );

        expect(wrapper).toMatchSnapshot();
    });

    test('renders with no conversations', () => {
        const wrapper = shallow(
            <ChatList
                {...props}
                messages={{}}
                buddies={{}}
                unseen={{}}
                sortedIds={[]}
            />,
        );

        expect(wrapper).toMatchSnapshot();
    });

    test('renders with no users', () => {
        const wrapper = shallow(
            <ChatList
                {...props}
                messages={{}}
                buddies={{}}
                unseen={{}}
                sortedIds={[]}
            />,
        );
        expect(wrapper).toMatchSnapshot();
    });
});
