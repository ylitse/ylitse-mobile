import React from 'react';
import 'react-native';
import { shallow } from 'enzyme';

import ChatListItem from './ChatListItem';

describe('ChatListItem', () => {
    const mockFunc = jest.fn();
    const props = {
        buddy: {
            displayName: 'J. Anteri',
            id: 'qwer',
        },
        goToConversation: mockFunc,
        yellowDot: true,
    };

    test('renders correctly', () => {
        const femaleWrapper = shallow(<ChatListItem {...props} />);
        const maleWrapper = shallow(<ChatListItem
            {...props}
        />);

        expect(femaleWrapper).toMatchSnapshot();
        expect(maleWrapper).toMatchSnapshot();
    });

    test('function is called on press', () => {
        const wrapper = shallow(<ChatListItem
            {...props}
        />);

        wrapper.find('TouchableOpacity').simulate('Press');
        expect(mockFunc).toHaveBeenCalled();
    });
});
