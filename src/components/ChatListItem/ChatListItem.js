import React from 'react';
import PropTypes from 'prop-types';
import { View, Image, Text, TouchableOpacity, Vibration } from 'react-native';
import Icon from 'react-native-vector-icons/SimpleLineIcons';

import FemaleIcon from '../../images/female_icon.png';
import MaleIcon from '../../images/male_icon.png';
import UnreadIcon from '../../images/unread.png';

import styles from './styles';

class ChatListItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showDelete: false,
        };
    }

    goDeletion = () => {
        Vibration.vibrate();
        this.setState({ showDelete: true });
    }

    goChatting = () => {
        const { showDelete } = this.state;
        if (!showDelete) {
            const { buddy: { id }, goToConversation } = this.props;
            goToConversation(id);
        }
    }

    delete = () => {
        const { buddy, openModal } = this.props;
        openModal(buddy);
    }

    goBack = () => {
        this.setState({ showDelete: false });
    }

    render() {
        const {
            buddy, yellowDot,
        } = this.props;
        const { showDelete } = this.state;

        if (showDelete) {
            return (
                <View style={styles.itemContainer}>
                    <TouchableOpacity
                        onPress={this.goBack}
                    >
                        <View style={styles.chatButton}>
                            <Icon
                                name="arrow-left"
                                color="white"
                                size={25}
                            />
                            {
                                yellowDot
                                && (
                                    <Image
                                        source={UnreadIcon}
                                        style={styles.unreadIcon}
                                    />
                                )
                            }
                        </View>
                    </TouchableOpacity>

                    <Image
                        source={buddy.gender === 'male' ? MaleIcon : FemaleIcon}
                        style={styles.personIcon}
                    />

                    <View style={styles.textRow}>
                        <Text style={styles.nameText}>
                            {buddy.displayName}
                        </Text>
                    </View>

                    <TouchableOpacity
                        onPress={this.delete}
                    >
                        <View style={styles.deleteButton}>
                            <Icon
                                name="trash"
                                color="white"
                                size={25}
                            />
                            {
                                yellowDot
                                && (
                                    <Image
                                        source={UnreadIcon}
                                        style={styles.unreadIcon}
                                    />
                                )
                            }
                        </View>
                    </TouchableOpacity>
                </View>
            );
        }

        return (
            <TouchableOpacity
                onPress={this.goChatting}
                onLongPress={this.goDeletion}
            >
                <View style={styles.itemContainer}>
                    <Image
                        source={buddy.gender === 'male' ? MaleIcon : FemaleIcon}
                        style={styles.personIcon}
                    />

                    <View style={styles.textRow}>
                        <Text style={styles.nameText}>
                            {buddy.displayName}
                        </Text>
                    </View>

                    <View style={styles.chatButton}>
                        <Icon
                            name="speech"
                            color="white"
                            size={25}
                        />
                        {
                            yellowDot
                            && (
                                <Image
                                    source={UnreadIcon}
                                    style={styles.unreadIcon}
                                />
                            )
                        }
                    </View>

                </View>
            </TouchableOpacity>
        );
    }
}

ChatListItem.propTypes = {
    buddy: PropTypes.shape({
        displayName: PropTypes.string.isRequired,
        id: PropTypes.string.isRequired,
    }).isRequired,
    yellowDot: PropTypes.bool.isRequired,
    goToConversation: PropTypes.func.isRequired,
    openModal: PropTypes.func.isRequired,
};

export default ChatListItem;
