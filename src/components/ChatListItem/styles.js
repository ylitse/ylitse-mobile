import { StyleSheet } from 'react-native';

import { colors, fonts, fontSizes } from '../../styles';

export default StyleSheet.create({
    itemContainer: {
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderBottomColor: colors.GREY_10,
        alignItems: 'center',
        justifyContent: 'space-between',
        height: 90,
    },
    personIcon: {
        width: 40,
        height: 40,
        marginLeft: 20,
        marginRight: 15,
    },
    textRow: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'flex-start',
    },
    nameText: {
        color: colors.GREY_DARK,
        fontFamily: fonts.PRIMARY,
        fontSize: fontSizes.HEADING_4,
    },
    chatButton: {
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomColor: '#CEF4AB',
        borderBottomWidth: 1,
        width: 70,
        height: 90,
        backgroundColor: colors.GREEN_PRIMARY,
    },
    deleteButton: {
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomColor: '#CEF4AB',
        borderBottomWidth: 1,
        width: 70,
        height: 90,
        backgroundColor: colors.ORANGE,
    },
    unreadIcon: {
        height: 11,
        width: 11,
        position: 'absolute',
        left: 40,
        top: 27,
    },
});
