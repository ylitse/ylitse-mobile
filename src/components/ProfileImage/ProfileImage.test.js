import React from 'react';
import 'react-native';
import { shallow } from 'enzyme';

import ProfileImage from './ProfileImage';

describe('ProfileImage', () => {
    test('renders correctly', () => {
        const props = {
            languages: [],
            gender: 'female',
            align: 'middle',
            isFavorite: true,
        };
        const wrapper = shallow(<ProfileImage {...props} />);

        expect(wrapper).toMatchSnapshot();
    });
    test('all languages render correctly', () => {
        const props = {
            languages: ['fi', 'se', 'en', 'ru'],
            gender: 'male',
            align: 'bottom',
        };

        let wrapper = shallow(<ProfileImage {...props} />);
        expect(wrapper).toMatchSnapshot();

        wrapper = shallow(
            <ProfileImage {...props} align="middle" gender="female" />,
        );
        expect(wrapper).toMatchSnapshot();
    });
});
