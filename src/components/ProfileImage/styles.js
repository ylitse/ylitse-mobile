import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        justifyContent: 'center',
        flex: 1,
        flexDirection: 'row',
        elevation: 7,
        zIndex: 1200,
    },
    middle: {
        justifyContent: 'center',
    },
    bottom: {
        justifyContent: 'flex-end',
    },
    elevated: {
        elevation: 7,
        zIndex: 9999,
    },
    languageContainerLeft: {
        flex: 1,
        flexDirection: 'column',
        width: 33,
        alignItems: 'flex-end',
    },
    languageContainerRight: {
        flex: 1,
        flexDirection: 'column',
        width: 33,
        alignItems: 'flex-start',
    },
    personIcon: {
        width: 100,
        height: 100,
        marginLeft: 15,
        marginRight: 15,
    },
    favoriteIcon: {
        width: 32,
        height: 32,
        position: 'absolute',
        left: 41,
    },
    favoriteIconImage: {
        width: 32,
        height: 32,
    },
    languageIcon: {
        width: 32,
        height: 30,
        marginTop: 10,
    },
});
