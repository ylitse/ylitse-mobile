import React from 'react';
import { View, Image } from 'react-native';
import PropTypes from 'prop-types';

import FemaleIcon from '../../images/female_icon.png';
import MaleIcon from '../../images/male_icon.png';
import FinnishIcon from '../../images/finnish.png';
import SwedishIcon from '../../images/swedish.png';
import EnglishIcon from '../../images/english.png';
import ItalianIcon from '../../images/italian.png';
import FavoriteIcon from '../../images/favorite.png';

import styles from './styles';

const fi = 'Finnish';
const sv = 'Swedish';
const en = 'English';
const it = 'Italian';

const languageMap = {
    [fi]: FinnishIcon,
    [sv]: SwedishIcon,
    [en]: EnglishIcon,
    [it]: ItalianIcon,
};

const ProfileImage = (props) => {
    const {
        languages, gender, align, isFavorite,
    } = props;
    const langSides = {
        left: [fi, sv],
        right: [en, it],
    };

    const getIcons = (languageProp, side) => {
        const langs = languageProp.filter(x => langSides[side].includes(x));
        return (
            <View style={[styles.languageContainerRight, styles[align]]}>
                {langs.map(language => (
                    <Image
                        source={languageMap[language]}
                        style={styles.languageIcon}
                        key={language}
                    />
                ))}
            </View>
        );
    };

    return (
        <View style={styles.container}>
            <View style={styles.elevated}>
                {getIcons(languages, 'left')}
            </View>
            {
                isFavorite
                && (
                    <View style={[styles.elevated, styles.favoriteIcon]}>
                        <Image
                            source={FavoriteIcon}
                            style={styles.favoriteIconImage}
                        />
                    </View>
                )
            }
            <Image
                source={gender === 'male' ? MaleIcon : FemaleIcon}
                style={styles.personIcon}
            />
            <View style={styles.elevated}>
                {getIcons(languages, 'right')}
            </View>
        </View>
    );
};

ProfileImage.propTypes = {
    gender: PropTypes.string,
    align: PropTypes.string,
    languages: PropTypes.arrayOf(PropTypes.string),
    isFavorite: PropTypes.bool,
};

ProfileImage.defaultProps = {
    gender: 'female',
    align: 'bottom',
    languages: [],
    isFavorite: false,
};

export default ProfileImage;
