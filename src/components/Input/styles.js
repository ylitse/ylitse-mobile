import { StyleSheet, Dimensions } from 'react-native';

import { spacings, colors, fontSizes } from '../../styles';

const { width } = Dimensions.get('window');

export default StyleSheet.create({
    inputContainer: {
        marginBottom: spacings[1],
        height: spacings[6],
        width: 0.8 * width,
        borderRadius: spacings[1] / 2,
        borderWidth: 2,
        borderColor: colors.GREY_10,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-around',
    },
    inputField: {
        paddingLeft: spacings[2],
        height: spacings[7],
        width: '100%',
    },
    passwordInputField: {
        height: spacings[7],
        width: '80%',
    },
    iconButton: {
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        height: spacings[6],
        width: '20%',
    },
    icon: {
        fontSize: fontSizes.BODY_TEXT,
        color: '#AAAAAA',
    },
});
