import React from 'react';
import { View, TextInput, TouchableWithoutFeedback } from 'react-native';
import Icon from 'react-native-vector-icons/SimpleLineIcons';
import PropTypes from 'prop-types';

import styles from './styles';

export const Input = ({
    value,
    placeholder,
    type,
    changeCallback,
    submitCallback,
    referenceCallback,
    returnKeyType,
}) => (
    <View style={styles.inputContainer}>
        <TextInput
            style={styles.inputField}
            placeholder={placeholder}
            onChangeText={changeCallback}
            onSubmitEditing={submitCallback}
            value={value}
            autoCapitalize="none"
            autoCorrect={false}
            returnKeyType={returnKeyType}
            underlineColorAndroid="transparent"
            placeholderTextColor="#AAAAAA"
            keyboardType={type}
            blurOnSubmit={false}
            ref={referenceCallback}
        />
    </View>
);

export const PasswordInput = ({
    value,
    placeholder,
    hiddenState,
    changeCallback,
    submitCallback,
    iconCallback,
    referenceCallback,
    returnKeyType,
}) => (
    <View style={styles.inputContainer}>
        <TextInput
            style={styles.passwordInputField}
            value={value}
            placeholder={placeholder}
            secureTextEntry={hiddenState}
            onChangeText={changeCallback}
            onSubmitEditing={submitCallback}
            ref={referenceCallback}
            returnKeyType={returnKeyType}
            underlineColorAndroid="transparent"
            placeholderTextColor="#AAAAAA"
            blurOnSubmit={false}
            autoCapitalize="none"
        />
        <TouchableWithoutFeedback
            onPress={iconCallback}
            style={styles.iconButton}
        >
            <Icon
                name="eye"
                style={styles.icon}
            />
        </TouchableWithoutFeedback>
    </View>
);

Input.propTypes = {
    value: PropTypes.string.isRequired,
    placeholder: PropTypes.string.isRequired,
    type: PropTypes.string,
    changeCallback: PropTypes.func.isRequired,
    submitCallback: PropTypes.func.isRequired,
    referenceCallback: PropTypes.func,
    returnKeyType: PropTypes.string,
};

PasswordInput.propTypes = {
    value: PropTypes.string.isRequired,
    placeholder: PropTypes.string.isRequired,
    hiddenState: PropTypes.bool.isRequired,
    iconCallback: PropTypes.func.isRequired,
    changeCallback: PropTypes.func.isRequired,
    submitCallback: PropTypes.func,
    referenceCallback: PropTypes.func,
    returnKeyType: PropTypes.string,
};

Input.defaultProps = {
    type: null,
    referenceCallback: () => { },
    returnKeyType: 'next',
};

PasswordInput.defaultProps = {
    submitCallback: () => { },
    referenceCallback: () => { },
    returnKeyType: 'done',
};
