import { StyleSheet } from 'react-native';

import { spacings, colors, fonts, fontSizes } from '../../styles';

export default StyleSheet.create({
    title: {
        color: colors.GREY_DARK,
        fontFamily: fonts.TITLE,
        fontSize: fontSizes.HEADING_3,
        textAlign: 'center',

        width: '100%',
    },

    subtitle: {
        color: colors.GREY_50,
        fontFamily: fonts.PRIMARY,
        fontSize: fontSizes.HEADING_4,
    },

    blockText: {
        color: colors.GREY_DARK,
        fontFamily: fonts.PRIMARY,
        fontSize: fontSizes.BODY_TEXT,
        textAlign: 'left',

        width: '100%',
        marginVertical: spacings[2],
    },

    centeredBlockText: {
        color: colors.GREY_DARK,
        fontFamily: fonts.PRIMARY,
        fontSize: fontSizes.BODY_TEXT,
        textAlign: 'center',

        width: '100%',
        marginVertical: spacings[2],
    },

    italicText: {
        color: colors.GREY_DARK,
        fontFamily: fonts.ITALIC,
        fontSize: fontSizes.BODY_TEXT,
        textAlign: 'left',

        width: '100%',
        marginVertical: spacings[2],
    },

    profileTitleText: {
        color: colors.GREY_50,
        fontFamily: fonts.PRIMARY,
        fontSize: fontSizes.HEADING_4,
        textAlign: 'left',
        width: '100%',
    },

    faintText: {
        color: colors.GREY_30,
        fontFamily: fonts.PRIMARY,
        fontSize: fontSizes.HEADING_3,
        textAlign: 'center',
    },
});
