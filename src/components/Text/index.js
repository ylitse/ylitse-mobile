import Text from './Text';

export {
    BlockText,
    CenteredBlockText,
    ItalicText,
    FaintText,
    Title,
    Subtitle,
    ProfileTitleText,
} from './Text';

export default Text;
