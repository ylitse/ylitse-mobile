import React from 'react';
import PropTypes from 'prop-types';
import { Text } from 'react-native';

import styles from './styles';

/** Basic text element for most use cases */
export const BlockText = ({ children, ...rest }) => (
    <Text style={styles.blockText} {...rest}>
        {children}
    </Text>
);

BlockText.propTypes = {
    children: PropTypes.node.isRequired,
};

/** Centered Basic text element for other cases */
export const CenteredBlockText = ({ children, ...rest }) => (
    <Text style={styles.centeredBlockText} {...rest}>
        {children}
    </Text>
);

CenteredBlockText.propTypes = {
    children: PropTypes.node.isRequired,
};

/** Basic italic text element */
export const ItalicText = ({ children }) => (
    <Text style={styles.italicText}>
        {children}
    </Text>
);

ItalicText.propTypes = {
    children: PropTypes.node.isRequired,
};

/** Big text for titles and headings */
export const Title = ({ children }) => (
    <Text style={styles.title}>
        {children}
    </Text>
);

Title.propTypes = {
    children: PropTypes.node.isRequired,
};

/** Smaller text for subheadings */
export const Subtitle = ({ children }) => (
    <Text style={styles.subtitle}>
        {children}
    </Text>
);

Subtitle.propTypes = {
    children: PropTypes.node.isRequired,
};

/** Bigger text for left aligned titles */
export const ProfileTitleText = ({ children }) => (
    <Text style={styles.profileTitleText}>
        {children}
    </Text>
);

ProfileTitleText.propTypes = {
    children: PropTypes.node.isRequired,
};

/** Faint text for placeholders */
export const FaintText = ({ children }) => (
    <Text style={styles.faintText}>
        {children}
    </Text>
);

FaintText.propTypes = {
    children: PropTypes.node.isRequired,
};

export default BlockText;
