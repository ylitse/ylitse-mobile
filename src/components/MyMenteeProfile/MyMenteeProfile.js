import React from 'react';
import PropTypes from 'prop-types';
import {
    View,
    Text,
    Linking,
} from 'react-native';
import ModalContainer from '../../containers/ModalContainer';

import { colors } from '../../styles';
import Modal from '../Modal';
import I18n from '../../i18n';
import Button from '../Button';
import {
    USER_GUIDE_URL,
    TERMS_URL,
    FEEDBACK_URL,
} from '../../../config';
import {
    HeaderScreenContainer,
    BlockContainer,
} from '../Container';
import { PasswordInput } from '../Input';
import {
    Title,
    ProfileTitleText,
} from '../Text';
import { VSpacer } from '../Spacer';
import styles from './styles';

class MyMenteeProfile extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentPassword: '',
            newPassword: '',
            repeatNewPassword: '',
            hidden: true,
            isObscured: true,
            isModalOpen: false,
            modal: {
                type: 'yes-no',
                message: 'asdf',
                handleYes: null,
                closeModal: () => this.setState({ isModalOpen: false }),
            },
        };
    }

    setRef = (component, target) => {
        this[target] = component;
    };

    setFocus = (target) => {
        this[target].focus();
    };

    toggleObscuration = () => this.setState(s => ({
        isObscured: !s.isObscured,
    }));

    togglePasswordField = () => this.setState(s => ({
        hidden: !s.hidden,
    }));

    changePassword = () => {
        const { newPassword, repeatNewPassword } = this.state;
        if (newPassword !== repeatNewPassword || newPassword.length < 4) {
            return;
        }
        const { changePassword } = this.props;
        this.setState(s => ({
            isModalOpen: true,
            modal: {
                ...s.modal,
                message: I18n.t('passwordUpdateYouSure'),
                handleYes: () => {
                    changePassword(
                        s.currentPassword,
                        s.newPassword,
                    );
                    this.setState({
                        currentPassword: '',
                        newPassword: '',
                        repeatNewPassword: '',
                        isModalOpen: false,
                    });
                },
            },
        }));
    }

    logout = () => {
        const { logout } = this.props;
        this.setState(s => ({
            ...s,
            isModalOpen: true,
            modal: {
                ...s.modal,
                message: I18n.t('LogOutYouSure'),
                handleYes: () => logout(),
            },
        }));
    }

    deleteAccount = () => {
        const { deleteAccount } = this.props;
        this.setState(s => ({
            ...s,
            isModalOpen: true,
            modal: {
                ...s.modal,
                message: I18n.t('deleteAccountYouSure'),
                handleYes: () => deleteAccount(),
            },
        }));
    }

    render() {
        const {
            hidden,
            currentPassword,
            newPassword,
            repeatNewPassword,
            isObscured,
            isModalOpen,
            modal,
        } = this.state;
        const { user, account } = this.props;
        const { GREEN_PRIMARY } = colors;

        const changeState = key => value => this.setState({ [key]: value });

        return (
            <HeaderScreenContainer>
                {
                    isModalOpen
                    && (
                        <Modal
                            {...modal}
                        />
                    )
                }

                <ModalContainer type="ok" />

                <React.Fragment>
                    <VSpacer size={2} />

                    <View>
                        <Title>
                            {user.displayName}
                        </Title>
                    </View>

                    <VSpacer size={2} />

                    <BlockContainer>
                        <Text style={styles.leftText}>
                            {I18n.t('myMenteeProfileUsername')}
                        </Text>
                        <ProfileTitleText>
                            {account.loginName}
                        </ProfileTitleText>

                        <VSpacer size={2} />

                        <Text style={styles.leftText}>
                            {I18n.t('myMenteeProfilePassword')}
                        </Text>
                        {
                            hidden
                                ? (
                                    <View style={styles.leftAlignedContainer}>
                                        <Text
                                            style={styles.greenText}
                                            onPress={this.togglePasswordField}
                                        >
                                            {I18n.t('myMenteeProfileChangePassword')}
                                        </Text>
                                    </View>
                                )
                                : (
                                    <View>

                                        <VSpacer size={2} />

                                        <PasswordInput
                                            value={currentPassword}
                                            hiddenState={isObscured}
                                            changeCallback={changeState('currentPassword')}
                                            placeholder={I18n.t('myMenteeProfileCurrentPassword')}
                                            iconCallback={this.toggleObscuration}
                                            text={currentPassword}
                                            submitCallback={() => this.setFocus('newPasswordInput')}
                                        />

                                        <PasswordInput
                                            value={newPassword}
                                            placeholder={I18n.t('myMenteeProfileNewPassword')}
                                            hiddenState={isObscured}
                                            changeCallback={changeState('newPassword')}
                                            iconCallback={this.toggleObscuration}
                                            referenceCallback={c => this.setRef(c, 'newPasswordInput')}
                                            submitCallback={() => this.setFocus('newPasswordRepeat')}
                                        />

                                        <PasswordInput
                                            value={repeatNewPassword}
                                            hiddenState={isObscured}
                                            changeCallback={changeState('repeatNewPassword')}
                                            placeholder={I18n.t('myMenteeProfileRepeatPassword')}
                                            iconCallback={this.toggleObscuration}
                                            text={repeatNewPassword}
                                            referenceCallback={component => this.setRef(component, 'newPasswordRepeat')}
                                        />

                                        <View style={styles.horizontalContainer}>
                                            <View style={{ paddingRight: 25 }}>
                                                <Button
                                                    onPress={this.togglePasswordField}
                                                    color={GREEN_PRIMARY}
                                                    text={I18n.t('myMenteeProfilePasswordCancel')}
                                                    primary={false}
                                                />
                                            </View>

                                            <View style={{ paddingLeft: 25 }}>
                                                <Button
                                                    onPress={() => { this.changePassword(); }}
                                                    text={I18n.t('myMenteeProfilePasswordSave')}
                                                />
                                            </View>
                                        </View>
                                    </View>
                                )
                        }
                    </BlockContainer>
                    <VSpacer size={2} />
                </React.Fragment>

                <View style={styles.separationLine} />

                <React.Fragment>
                    <VSpacer size={2} />
                    <BlockContainer>
                        <View style={styles.leftAlignedContainer}>
                            <Text style={styles.leftText}>
                                {I18n.t('myMenteeProfileOtherInformation')}
                            </Text>

                            <VSpacer size={2} />

                            <Text
                                style={styles.greenText}
                                onPress={() => Linking.openURL(FEEDBACK_URL)}
                            >
                                {I18n.t('myMenteeProfileFeedback')}
                            </Text>

                            <VSpacer size={2} />

                            <Text
                                style={styles.greenText}
                                onPress={() => Linking.openURL(USER_GUIDE_URL)}
                            >
                                {I18n.t('myMenteeProfileUserGuide')}
                            </Text>

                            <VSpacer size={2} />

                            <Text
                                style={styles.greenText}
                                onPress={() => Linking.openURL(TERMS_URL)}
                            >
                                {I18n.t('myMenteeProfileTermsConditions')}
                            </Text>
                        </View>
                    </BlockContainer>
                    <VSpacer size={2} />
                </React.Fragment>

                <View style={styles.separationLine} />

                <React.Fragment>
                    <VSpacer size={2} />
                    <BlockContainer>
                        <Button
                            onPress={() => { this.logout(); }}
                            color={GREEN_PRIMARY}
                            primary={false}
                            text={I18n.t('myMenteeProfileLogout')}
                        />
                        <Button
                            onPress={() => { this.deleteAccount(); }}
                            text={I18n.t('myMenteeProfileDeleteAccount')}
                            primary={false}
                        />
                    </BlockContainer>
                    <VSpacer size={2} />
                </React.Fragment>
            </HeaderScreenContainer>
        );
    }
}

MyMenteeProfile.propTypes = {
    account: PropTypes.shape({
        loginName: PropTypes.string,
    }).isRequired,
    user: PropTypes.shape({
        displayName: PropTypes.string,
    }).isRequired,
    logout: PropTypes.func.isRequired,
    changePassword: PropTypes.func.isRequired,
    deleteAccount: PropTypes.func.isRequired,
};

export default MyMenteeProfile;
