import React from 'react';
import 'react-native';
import { shallow } from 'enzyme';

import MyMenteeProfile from './MyMenteeProfile';

describe('MyMenteeProfile', () => {
    const mockAccount = {
        loginName: 'JANTERI',
    };
    const mockUser = {
        displayName: 'SANTERI',
    };

    const mockLogout = jest.fn();
    const mockDeleteAccount = jest.fn();
    const mockChangePassword = jest.fn();

    const props = {
        account: mockAccount,
        user: mockUser,
        logout: mockLogout,
        deleteAcocunt: mockDeleteAccount,
        changePassword: mockChangePassword,
    };

    test('renders correctly', () => {
        const wrapper = shallow(<MyMenteeProfile {...props} />);

        expect(wrapper).toMatchSnapshot();
    });
});
