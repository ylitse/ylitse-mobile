import { StyleSheet } from 'react-native';

import { colors, fonts, fontSizes } from '../../styles';

export default StyleSheet.create({
    horizontalContainer: {
        flexDirection: 'row',
    },
    leftAlignedContainer: {
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        width: '100%',
    },
    separationLine: {
        width: '100%',
        borderTopWidth: 1,
        borderTopColor: colors.GREY_30,
    },
    leftText: {
        textAlign: 'left',
        width: '100%',
    },
    greenText: {
        color: colors.GREEN_PRIMARY,
        fontFamily: fonts.PRIMARY,
        fontSize: fontSizes.HEADING_4,
        textAlign: 'left',
    },
});
