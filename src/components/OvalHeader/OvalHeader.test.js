import React from 'react';
import 'react-native';
import { shallow } from 'enzyme';

import OvalHeader from './OvalHeader';

describe('OvalHeader', () => {
    test('renders correctly with big title', () => {
        // bigTitle === true by default
        const props = { title: 'Title', subtitle: 'subtitle' };
        const wrapper = shallow(<OvalHeader {...props} />);

        expect(wrapper).toMatchSnapshot();
    });
    test('renders correctly with small title', () => {
        const props = { title: 'Title', subtitle: 'subtitle', bigTitle: false };
        const wrapper = shallow(<OvalHeader {...props} />);

        expect(wrapper).toMatchSnapshot();
    });
});
