import React from 'react';
import PropTypes from 'prop-types';
import { View, Text } from 'react-native';

import styles from './styles';
import { colors } from '../../styles';

const OvalHeader = (props) => {
    const {
        title,
        subtitle,
        backgroundColor,
        textColor,
        bigTitle,
    } = props;

    return (
        <View style={{ alignItems: 'center' }}>
            <View style={[styles.headerContainer, { backgroundColor }]}>
                <View style={styles.headerContent}>
                    <Text
                        style={[
                            bigTitle
                                ? styles.titleTextBig
                                : styles.titleTextSmall,
                            { color: textColor },
                        ]}
                    >
                        {title}
                    </Text>
                    <Text style={[styles.subtitleText, { color: textColor }]}>
                        {subtitle}
                    </Text>
                </View>
            </View>
        </View>
    );
};

OvalHeader.propTypes = {
    title: PropTypes.string,
    subtitle: PropTypes.string,
    backgroundColor: PropTypes.string,
    textColor: PropTypes.string,
    bigTitle: PropTypes.bool,
};

OvalHeader.defaultProps = {
    title: '',
    subtitle: '',
    backgroundColor: colors.ORANGE,
    textColor: colors.GREY_VERY_LIGHT,
    bigTitle: true,
};

export default OvalHeader;
