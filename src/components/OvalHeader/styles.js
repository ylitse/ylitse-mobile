import { StyleSheet, Dimensions } from 'react-native';

import { fonts, fontSizes } from '../../styles';

const { width, height } = Dimensions.get('window');

export default StyleSheet.create({
    headerContainer: {
        width: 1.5 * width,
        height: height / 2,
        borderBottomLeftRadius: 2 * width,
        borderBottomRightRadius: 2 * width,
        marginTop: -140,
        paddingTop: 120,
        justifyContent: 'center',
    },
    headerContent: {
        alignItems: 'center',
    },

    titleTextBig: {
        fontFamily: fonts.TITLE,
        fontSize: fontSizes.HEADING_1,
    },
    titleTextSmall: {
        fontFamily: fonts.TITLE,
        fontSize: fontSizes.HEADING_3,
    },
    subtitleText: {
        fontFamily: fonts.PRIMARY,
        fontSize: fontSizes.BODY_TEXT,
        textAlign: 'center',
    },
});
