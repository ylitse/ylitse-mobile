import React from 'react';
import 'react-native';
import { shallow } from 'enzyme';

import RegisterForm from './RegisterForm';

describe('RegisterForm', () => {
    test('renders correctly', () => {
        const wrapper = shallow(<RegisterForm />);
        wrapper.dive().find('TextInput').at(0).props()
            .onChangeText('a');
        wrapper.dive().find('PasswordInput').at(0).props()
            .onChangeText('b');

        expect(wrapper).toMatchSnapshot();
    });

    test('default function is called on press', () => {
        const wrapper = shallow(<RegisterForm />);

        wrapper.find('Button').dive().simulate('Press');
        wrapper.find('TextInput').props().onChangeText('test');
        wrapper.dive().find('PasswordInput').at(0).props()
            .onChangeText('testpw');
        wrapper.find('Button').dive().simulate('Press');

        wrapper.find('TextButton').dive().simulate('Press');

        expect(wrapper).toMatchSnapshot();
    });

    test('handleSubmit is called on press when form OK', () => {
        const mockFunc = jest.fn();
        const props = { handleSubmit: mockFunc };
        const wrapper = shallow(<RegisterForm {...props} />);

        wrapper.setState({
            loginName: 'test',
            password: 'testpw',
            repeat: 'testpw',
        });

        wrapper.find('Button').dive().simulate('Press');

        expect(mockFunc).toHaveBeenCalled();
    });

    test('checkRegister does not submit when form not OK', () => {
        const mockFunc = jest.fn();
        const mockFuncCheck = jest.fn();
        const props = {
            handleSubmit: mockFunc,
            handleCheck: mockFuncCheck,
        };

        const wrapper = shallow(<RegisterForm {...props} />);

        wrapper.setState({
            loginName: 'te',
            password: 't',
        });
        wrapper.find('Button').dive().simulate('Press');

        wrapper.setState({
            loginName: 'tet',
            password: 'a',
        });
        wrapper.find('Button').dive().simulate('Press');

        expect(mockFuncCheck).toHaveBeenCalledTimes(2);
    });

    test('handlePress changes state', () => {
        const wrapper = shallow(<RegisterForm />);

        wrapper.find('PasswordInput').at(0).simulate('Press');
        expect(wrapper.instance().state.hidden).toEqual(false);
    });

    test('check if the form is ok before submitting', () => {
        const wrapper = shallow(<RegisterForm />);

        expect(wrapper).toMatchSnapshot();
    });

    test('function setRef is called', () => {
        const wrapper = shallow(<RegisterForm />);
        const spy = jest.spyOn(wrapper.instance(), 'setRef');
        wrapper.update();

        wrapper.find('PasswordInput').at(0).props().reference();
        expect(spy).toHaveBeenCalled();
    });

    test('function setFocus is called', () => {
        const wrapper = shallow(<RegisterForm />);
        const password = wrapper.dive().find('PasswordInput')
            .at(0).dive()
            .find('TextInput')
            .dive()
            .instance();
        wrapper.instance().setRef(password, 'passwordInput');
        const spy = jest.spyOn(wrapper.instance(), 'setFocus');
        wrapper.update();

        wrapper.find('TextInput').simulate('SubmitEditing');
        expect(spy).toHaveBeenCalled();
        wrapper.find('PasswordInput').at(0).simulate('SubmitEditing');
        expect(spy).toHaveBeenCalled();
    });

    test('submits after last input', () => {
        const wrapper = shallow(<RegisterForm />);
        const spy = jest.spyOn(wrapper.instance(), 'checkRegister');

        wrapper.find('PasswordInput').at(0).simulate('SubmitEditing');
        expect(spy).toHaveBeenCalled();
    });
});
