import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';

import { ScreenContainer } from '../Container';
import { Subtitle, BlockText } from '../Text';
import Button from '../Button';
import TextButton from '../TextButton';
import OvalHeader from '../OvalHeader';
import ModalContainer from '../../containers/ModalContainer';
import { Input, PasswordInput } from '../Input';

import I18n from '../../i18n';

class RegisterForm extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            loginName: '',
            password: '',
            hidden: true,
        };
    }

    setRef = (component, target) => {
        this[target] = component;
    };

    setFocus = (target) => {
        this[target].focus();
    };

    handlePress = () => this.setState(s => ({ hidden: !s.hidden }))

    handleCheckBox = () => { }

    submitRegister = () => {
        const { loginName, password } = this.state;
        const { handleSubmit } = this.props;
        handleSubmit({ loginName, password });
    };

    checkRegister = () => {
        const { loginName, password } = this.state;
        const { handleCheck } = this.props;
        if (password === '') {
            handleCheck('registerFormPasswordEmpty');
            return;
        }

        if (loginName.split('').length <= 3) {
            handleCheck('registerFormUsernameTooShort');
            return;
        }

        this.submitRegister();
    };

    changePassword = password => this.setState({ password })

    changeLoginName = loginName => this.setState({ loginName })

    render() {
        const { loginName, password, hidden } = this.state;
        const { goBack, loginHere } = this.props;
        return (
            <ScreenContainer>
                <ModalContainer type="ok" />
                <OvalHeader
                    title="Ylitse"
                    subtitle={I18n.t('registerFormHeaderSubtitle')}
                />

                <Subtitle>
                    {I18n.t('registerFormSignupTitle')}
                </Subtitle>

                <View>
                    <Input
                        value={loginName}
                        placeholder={
                            I18n.t('registerFormPrefilledUsername')}
                        type="email-address"
                        changeCallback={this.changeLoginName}
                        submitCallback={() => this.setFocus('passwordInput')}
                    />
                    <PasswordInput
                        value={password}
                        placeholder={I18n.t('registerFormPrefilledPassword')}
                        hiddenState={hidden}
                        changeCallback={this.changePassword}
                        iconCallback={this.handlePress}
                        submitCallback={() => this.checkRegister()}
                        referenceCallback={c => this.setRef(c, 'passwordInput')}
                    />
                </View>

                <Button
                    text={I18n.t('registerFormSignupButton')}
                    onPress={this.checkRegister}
                />

                <Button
                    primary={false}
                    text={I18n.t('goBack')}
                    onPress={goBack}
                />

                <View>
                    <BlockText>
                        {I18n.t('registerFormOldAccount')}
                    </BlockText>
                    <TextButton
                        text={I18n.t('registerFormLoginHere')}
                        onPress={loginHere}
                    />
                </View>
            </ScreenContainer>
        );
    }
}

RegisterForm.propTypes = {
    goBack: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    handleCheck: PropTypes.func.isRequired,
    loginHere: PropTypes.func.isRequired,
};

export default RegisterForm;
