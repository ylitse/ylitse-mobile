import { StyleSheet } from 'react-native';
import { colors, fonts, fontSizes } from '../../styles';

export default StyleSheet.create({
    separationLine: {
        width: '100%',
        borderTopWidth: 1,
        borderTopColor: colors.GREY_30,
    },
    infoText: {
        color: colors.GREY_DARK,
        fontFamily: fonts.PRIMARY,
        fontSize: fontSizes.BODY_TEXT,
        marginBottom: 10,
    },
    loginNameText: {
        color: colors.GREY_DARK,
        fontFamily: fonts.PRIMARY,
        fontSize: fontSizes.HEADING_3,
        marginTop: 12,
        marginBottom: 5,
    },
    leftAlignedContainer: {
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        width: '100%',
    },
    leftText: {
        textAlign: 'left',
        width: '100%',
    },
    greenText: {
        color: colors.GREEN_PRIMARY,
        textAlign: 'left',
        fontFamily: fonts.PRIMARY,
        fontSize: fontSizes.HEADING_4,
    },
});
