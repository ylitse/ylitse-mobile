import React from 'react';
import 'react-native';
import renderer from 'react-test-renderer';

import MyMentorProfile from './MyMentorProfile';

describe('MyMentorProfile', () => {
    const mockUser = {
        id: '123456',
        is_active: true,
        created: '2018-01-01T12:00:00Z',
        updated: '',
        loginName: 'Mentor_user',
        role: 'mentee',
        real_name: 'Mentor User',
        email: 'mentor@example.com',
        phone: '+3580123456789',
        gender: 'male',
        birthYear: 1990,
        region: 'Helsinki',
        languages: [
            'fi',
            'se',
        ],
        skills: [
            '111111',
            '222222',
        ],
        comm_channels: [
            'email',
            'chat',
        ],
        story: `Iusto viderer est cu, vix scripta luptatum conceptam
    ei. Ne movet iudico conceptam quo, an vim quas iudico audiam,
     ut deleniti inimicus salutandi pri.

     Accumsan suscipiantur cu vel, insolens efficiendi instructior vix at.
     Ut esse graece neglegentur pri, qui id legere latine eripuit.
     `,
        starred_contacts: [
            '111111',
            '222222',
        ],
    };
    const mockFunc = jest.fn();
    const props = {
        mentor: mockUser,
        logout: mockFunc,
    };

    test('renders correctly', () => {
        const wrapper = renderer.create(<MyMentorProfile {...props} />);

        expect(wrapper).toMatchSnapshot();
    });

    test('default data works', () => {
        const wrapper = renderer.create(<MyMentorProfile mentor={{}} />);

        expect(wrapper).toMatchSnapshot();
    });
});
