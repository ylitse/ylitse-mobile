import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, Linking } from 'react-native';

import Icon from 'react-native-vector-icons/SimpleLineIcons';
import I18n from '../../i18n';
import {
    USER_GUIDE_URL,
    TERMS_URL,
    MANAGE_ACCOUNT_URL,
    FEEDBACK_URL,
} from '../../../config';

import Modal from '../Modal';
import Button from '../Button';
import ChipWrapper from '../ChipWrapper';
import ProfileImage from '../ProfileImage/ProfileImage';
import styles from './styles';
import { HeaderScreenContainer, BlockContainer } from '../Container';
import { VSpacer } from '../Spacer';
import { BlockText } from '../Text';

import { colors } from '../../styles';

class MyMentorProfile extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isModalOpen: false,
            modal: {
                type: 'yes-no',
                message: 'asdf',
                handleYes: null,
                closeModal: () => this.setState({ isModalOpen: false }),
            },
        };
    }

    setRef(component) {
        this.refScrollView = component;
    }

    logout = () => {
        const { logout } = this.props;
        this.setState(s => ({
            ...s,
            isModalOpen: true,
            modal: {
                ...s.modal,
                message: I18n.t('LogOutYouSure'),
                handleYes: () => logout(),
            },
        }));
    }

    openYlitseFi = () => Linking.openURL(MANAGE_ACCOUNT_URL);

    openTerms = () => Linking.openURL(TERMS_URL);

    openFeedback = () => Linking.openURL(FEEDBACK_URL);

    openGuide = () => Linking.openURL(USER_GUIDE_URL);

    modal() {
        const { isModalOpen, modal } = this.state;
        return (
            isModalOpen
            && <Modal {...modal} />
        );
    }

    renderProfile() {
        const { GREEN_PRIMARY } = colors;
        const { mentor } = this.props;
        const currentYear = new Date().getFullYear();
        const mentorAge = currentYear - mentor.birthYear;
        const mentorAgeText = mentorAge
            ? `${mentorAge} ${I18n.t('MyProfileAge')}`
            : `${I18n.t('MyProfileAgeDefault')}`;

        return (
            <React.Fragment>
                <VSpacer size={2} />

                <BlockContainer>
                    <ProfileImage
                        align="middle"
                        {...mentor}
                    />

                    <Text style={styles.loginNameText}>
                        {mentor.displayName}
                    </Text>

                    <Text style={styles.infoText}>
                        <Icon
                            name="diamond"
                            style={{ fontSize: 12 }}
                        />
                        {`   ${mentorAgeText}      `}
                        <Icon
                            name="location-pin"
                            style={{ fontSize: 12 }}
                        />
                        {`   ${mentor.region
                        || I18n.t('MyProfileAreaDefault')}`}
                    </Text>

                    <BlockText
                        ellipsizeMode="tail"
                        numberOfLines={15}
                    >
                        {
                            mentor.story
                            || I18n.t('MyProfileStoryDefault')
                        }
                    </BlockText>
                </BlockContainer>

                <VSpacer size={2} />

                <View style={styles.separationLine} />

                <VSpacer size={2} />

                <ChipWrapper skills={mentor.skills} />

                <VSpacer size={2} />

                <View style={styles.separationLine} />

                <VSpacer size={2} />

                <BlockContainer>
                    <View style={styles.leftAlignedContainer}>
                        <VSpacer size={2} />

                        <Text style={styles.leftText}>
                            {I18n.t('myMenteeProfileOtherInformation')}
                        </Text>

                        <VSpacer size={2} />

                        <Text
                            style={styles.greenText}
                            onPress={this.openFeedback}
                        >
                            {I18n.t('myMenteeProfileFeedback')}
                        </Text>

                        <VSpacer size={2} />

                        <Text
                            style={styles.greenText}
                            onPress={this.openGuide}
                        >
                            {I18n.t('myMenteeProfileUserGuide')}
                        </Text>

                        <VSpacer size={2} />

                        <Text
                            style={styles.greenText}
                            onPress={this.openTerms}
                        >
                            {I18n.t('myMenteeProfileTermsConditions')}
                        </Text>

                        <VSpacer size={2} />
                    </View>
                </BlockContainer>

                <VSpacer size={2} />

                <View style={styles.separationLine} />

                <VSpacer size={2} />

                <BlockContainer>
                    <Button
                        text={I18n.t('manageMyAccountOnWeb')}
                        onPress={this.openYlitseFi}
                    />

                    <Button
                        color={GREEN_PRIMARY}
                        text={I18n.t('myMenteeProfileLogout')}
                        primary={false}
                        onPress={() => this.logout()}
                    />
                </BlockContainer>

                <VSpacer size={2} />

            </React.Fragment>
        );
    }

    render() {
        return (
            <HeaderScreenContainer>
                {this.modal()}

                {this.renderProfile()}
            </HeaderScreenContainer>
        );
    }
}

MyMentorProfile.propTypes = {
    account: PropTypes.shape({
        loginName: PropTypes.string,
    }).isRequired,
    user: PropTypes.shape({
        displayName: PropTypes.string,
    }).isRequired,
    mentor: PropTypes.shape({
        loginName: PropTypes.string,
        birthYear: PropTypes.number,
        region: PropTypes.string,
        story: PropTypes.string,
        skills: PropTypes.arrayOf(PropTypes.string),
        languages: PropTypes.arrayOf(PropTypes.string),
    }).isRequired,
    logout: PropTypes.func.isRequired,
};

export default MyMentorProfile;
