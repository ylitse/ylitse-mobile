import React from 'react';
import PropTypes from 'prop-types';

import { ScreenContainer, BlockContainer } from '../Container';
import { Subtitle } from '../Text';
import Button from '../Button';
import OvalHeader from '../OvalHeader';
import ModalContainer from '../../containers/ModalContainer';
import { Input, PasswordInput } from '../Input';

import I18n from '../../i18n';

class LoginForm extends React.Component {
    constructor(props) {
        super(props);
        this.handleSubmit = props.handleSubmit;

        this.state = {
            loginName: '',
            password: '',
            hidden: true,
        };
    }

    setRef = (component) => {
        this.passwordInput = component;
    }

    setFocus = () => {
        this.passwordInput.focus();
    }

    handlePress = () => this.setState(s => ({ hidden: !s.hidden }))

    submitLogin = () => {
        const { loginName, password } = this.state;
        this.handleSubmit(loginName, password);
    };

    render() {
        const { goBack } = this.props;
        const { hidden, loginName, password } = this.state;
        return (
            <ScreenContainer>
                <ModalContainer type="ok" />
                <OvalHeader
                    title="Ylitse"
                    subtitle={I18n.t('loginFormHeaderSubtitle')}
                />

                <Subtitle>
                    {I18n.t('loginFormSignupTitle')}
                </Subtitle>

                <BlockContainer>
                    <Input
                        value={loginName}
                        placeholder={I18n.t('loginFormPrefilledUsername')}
                        changeCallback={s => this.setState({ loginName: s })}
                        submitCallback={() => this.setFocus()}
                    />
                    <PasswordInput
                        value={password}
                        placeholder={I18n.t('loginFormPrefilledPassword')}
                        hiddenState={hidden}
                        changeCallback={s => this.setState({ password: s })}
                        iconCallback={this.handlePress}
                        submitCallback={() => this.submitLogin()}
                        referenceCallback={component => this.setRef(component)}
                    />

                </BlockContainer>

                <Button
                    text={I18n.t('loginFormSignInButton')}
                    onPress={this.submitLogin}
                />

                <Button
                    primary={false}
                    text={I18n.t('goBack')}
                    onPress={goBack}
                />

            </ScreenContainer>
        );
    }
}

LoginForm.propTypes = {
    handleSubmit: PropTypes.func.isRequired,
    goBack: PropTypes.func.isRequired,
};

export default LoginForm;
