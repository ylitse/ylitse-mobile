import React from 'react';
import 'react-native';
import { shallow } from 'enzyme';

import LoginForm from './LoginForm';

describe('LoginForm', () => {
    test('renders correctly', () => {
        const wrapper = shallow(<LoginForm />);
        wrapper.dive().find('TextInput').first().props()
            .onChangeText('a');
        wrapper.dive().find('PasswordInput').props()
            .onChangeText('b');

        expect(wrapper).toMatchSnapshot();
    });

    test('default function is called on press', () => {
        const wrapper = shallow(<LoginForm />);

        wrapper.find('Button').dive().simulate('Press');
        expect(wrapper).toMatchSnapshot();
    });

    test('passed function is called on press', () => {
        const mockFunc = jest.fn();
        const props = { handleSubmit: mockFunc };
        const wrapper = shallow(<LoginForm {...props} />);

        wrapper.find('Button').dive().simulate('Press');
        expect(mockFunc).toHaveBeenCalled();
    });

    test('handlePress changes state of LoginForm', () => {
        const wrapper = shallow(<LoginForm />);

        wrapper.find('PasswordInput').simulate('Press');
        expect(wrapper.instance().state.hidden).toEqual(false);
    });

    test('function is called on submit editing for PasswordInput', () => {
        const mockFunc = jest.fn();
        const props = { handleSubmit: mockFunc };

        const wrapper = shallow(<LoginForm {...props} />);
        wrapper.find('PasswordInput').simulate('SubmitEditing');
        expect(mockFunc).toHaveBeenCalled();
    });

    test('function setRef is called', () => {
        const wrapper = shallow(<LoginForm />);
        const spy = jest.spyOn(wrapper.instance(), 'setRef');
        wrapper.update();

        wrapper.find('PasswordInput').props().reference();
        expect(spy).toHaveBeenCalled();
    });

    test('function setFocus is called', () => {
        const wrapper = shallow(<LoginForm />);
        const input = wrapper.dive().find('PasswordInput')
            .dive().find('TextInput')
            .dive()
            .instance();
        wrapper.instance().setRef(input);
        const spy = jest.spyOn(wrapper.instance(), 'setFocus');
        wrapper.update();

        wrapper.find('TextInput').simulate('SubmitEditing');
        expect(spy).toHaveBeenCalled();
    });
});
