import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    checkBoxContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        width: '80%',
    },
    checkBox: {
        marginRight: 5,
    },
});
