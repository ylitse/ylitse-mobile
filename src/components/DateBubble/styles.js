import { StyleSheet } from 'react-native';

import { colors, fontSizes } from '../../styles';

export default StyleSheet.create({
    bubbleContainer: {
    },
    bubble: {
        alignItems: 'center',
        alignSelf: 'center',
        padding: 5,
        paddingHorizontal: 12,
        marginTop: 5,
        marginBottom: 10,
        borderRadius: 10,
        backgroundColor: '#E4F3FA',
    },
    bubbleText: {
        color: colors.GREY_DARK,
        fontSize: fontSizes.SMALL,
    },
});
