import React from 'react';
import PropTypes from 'prop-types';
import { View, Text } from 'react-native';
import styles from './styles';
import I18n from '../../i18n';

const DateBubble = (props) => {
    const { created } = props;
    const [messageDate] = created.split('T');

    let today = (new Date(Date.now())).toISOString();
    [today] = today.split('T');

    let yesterday = (new Date(Date.now() - 86400000)).toISOString();
    [yesterday] = yesterday.split('T');

    let bubbleText = '';
    if (messageDate === today) {
        bubbleText = I18n.t('today');
    }
    if (messageDate === yesterday) {
        bubbleText = I18n.t('yesterday');
    }
    if (!bubbleText) {
        const [y, m, d] = messageDate.split('-');
        bubbleText = `${parseInt(d, 10)} ${I18n.t('months')[m - 1]} ${y}`;
    }

    return (
        <View style={styles.bubbleContainer}>
            <View style={styles.bubble}>
                <Text style={styles.bubbleText}>
                    {bubbleText}
                </Text>
            </View>
        </View>
    );
};

DateBubble.propTypes = {
    created: PropTypes.string.isRequired,
};

export default DateBubble;
