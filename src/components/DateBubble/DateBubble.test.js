import React from 'react';
import 'react-native';
import { shallow } from 'enzyme';

import DateBubble from './DateBubble';

describe('Datebubble', () => {
    const created = (new Date(1000000000000)).toISOString();

    test('renders a day', () => {
        const wrapper = shallow(<DateBubble created={created} />);
        const textDate = wrapper.find('Text').dive().children().debug();

        expect(textDate).toBe('9 September 2001');
        expect(wrapper).toMatchSnapshot();
    });
});
