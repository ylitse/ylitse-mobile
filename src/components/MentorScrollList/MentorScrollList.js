import React from 'react';
import PropTypes from 'prop-types';
import { FlatList } from 'react-native';

import MentorCard from '../MentorCard';

const MentorScrollList = ({ mentorList, loggedIn, onPressCallback }) => (
    <FlatList
        data={mentorList}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({ item }) => (
            <MentorCard
                mentor={item}
                key={item.id}
                isLoggedIn={loggedIn}
                onPress={() => onPressCallback(item)}
            />
        )}
        horizontal
        showsHorizontalScrollIndicator={false}
    />
);

MentorScrollList.propTypes = {
    mentorList: PropTypes.arrayOf(PropTypes.shape({
        displayName: PropTypes.string.isRequired,
        birthYear: PropTypes.number.isRequired,
        region: PropTypes.string.isRequired,
        story: PropTypes.string.isRequired,
        skills: PropTypes.arrayOf(PropTypes.string),
        languages: PropTypes.arrayOf(PropTypes.string),
    })).isRequired,
    loggedIn: PropTypes.bool,
    onPressCallback: PropTypes.func,
};

MentorScrollList.defaultProps = {
    loggedIn: false,
    onPressCallback: () => { },
};

export default MentorScrollList;
