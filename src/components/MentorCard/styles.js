import { StyleSheet, Dimensions } from 'react-native';

import { spacings, colors, fonts, fontSizes } from '../../styles';

const { width } = Dimensions.get('window');

const ovalDiameter = width * 0.65;

export default StyleSheet.create({
    container: {
        backgroundColor: colors.WHITE,
        padding: 10,
        paddingTop: 60,
        alignItems: 'center',
        justifyContent: 'center',
        width: 0.92 * width,
    },
    cardContainer: {
        width: 0.88 * width,
        justifyContent: 'center',
        alignItems: 'center',
    },
    profileImageContainer: {
        position: 'absolute',
        top: -55,
        zIndex: 1200,
        elevation: 6,
    },
    profileContainer: {
        width: '100%',
        borderRadius: 5,
        alignItems: 'center',
        shadowColor: '#000',
        shadowOpacity: 0.19,
        shadowOffset: { width: 0, height: 3 },
        shadowRadius: 2,
        elevation: 5,
        zIndex: 1100,
        backgroundColor: 'white',
    },
    innerProfileContainer: {
        alignItems: 'center',
        backgroundColor: colors.WHITE,
        width: '100%',
        borderRadius: 5,
        overflow: 'hidden',
        shadowColor: '#000',
        shadowOpacity: 0.19,
        shadowOffset: { width: 0, height: 10 },
        shadowRadius: 20,
        zIndex: 1100,
    },
    profileMargin: {
        backgroundColor: colors.ORANGE,
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5,
        height: spacings[3],
        width: '100%',
    },
    profileBackground: {
        backgroundColor: colors.ORANGE,
        paddingTop: 30,
        alignItems: 'center',
        width: '100%',
    },
    profileOval: {
        elevation: -1,
        zIndex: -100,
        width: ovalDiameter,
        height: 200,
        marginTop: -145,
        borderBottomRightRadius: ovalDiameter * 0.5,
        borderBottomLeftRadius: ovalDiameter * 0.5,
        transform: [
            { scaleX: 2 },
        ],
        backgroundColor: colors.ORANGE,
    },
    profileContent: {
        zIndex: 9000,
        width: 0.8 * width,
        alignItems: 'center',
        justifyContent: 'center',
    },
    textContainer: {
        backgroundColor: colors.ORANGE,
        zIndex: 9000,
        marginTop: 3,
        marginLeft: 10,
        marginRight: 10,
    },
    buttonContainer: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 15,
    },

    smallText: {
        color: colors.GREY_VERY_LIGHT,
        fontFamily: fonts.PRIMARY,
        fontSize: fontSizes.SMALL,
    },
    nameText: {
        color: colors.GREY_VERY_LIGHT,
        fontFamily: fonts.PRIMARY,
        fontSize: fontSizes.HEADING_2,
    },
    bodyText: {
        textAlign: 'center',
        color: colors.GREY_VERY_LIGHT,
        fontFamily: fonts.PRIMARY,
        fontSize: fontSizes.BODY_TEXT,
    },
});
