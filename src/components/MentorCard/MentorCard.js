import React from 'react';
import PropTypes from 'prop-types';
import { View, Text } from 'react-native';
import Icon from 'react-native-vector-icons/SimpleLineIcons';
import I18n from '../../i18n';

import styles from './styles';

import Button from '../Button';
import ChipWrapper from '../ChipWrapper';
import ProfileImage from '../ProfileImage/ProfileImage';
import { VSpacer } from '../Spacer';

const MentorCard = ({ isLoggedIn, mentor, onPress }) => {
    const {
        displayName, region, story, skills, languages, gender, birthYear,
    } = mentor;

    const action = () => onPress(mentor);

    const currentYear = new Date().getFullYear();

    return (
        <View style={styles.container}>
            <View style={styles.cardContainer}>
                <View style={styles.profileImageContainer}>
                    <ProfileImage
                        languages={languages}
                        align="middle"
                        gender={gender}
                    />
                </View>
                <View style={styles.profileContainer}>
                    <View style={styles.innerProfileContainer}>

                        <View style={styles.profileMargin} />

                        <View style={styles.profileBackground}>

                            <View style={styles.profileContent}>
                                <View style={styles.textContainer}>
                                    <Text style={styles.nameText}>
                                        {displayName}
                                    </Text>
                                </View>
                                <View style={styles.textContainer}>
                                    <Text style={styles.smallText}>
                                        <Icon
                                            name="diamond"
                                            style={{ fontSize: 8 }}
                                        />
                                        {'  '}{currentYear - birthYear}{' v.   |    '}
                                        <Icon
                                            name="location-pin"
                                            style={{ fontSize: 8 }}
                                        />
                                        {'  '}{region}
                                    </Text>
                                </View>
                                <View style={styles.textContainer}>
                                    <Text
                                        ellipsizeMode="tail"
                                        numberOfLines={3}
                                        style={styles.bodyText}
                                    >
                                        {story}
                                    </Text>
                                </View>
                            </View>

                        </View>
                        <View style={styles.profileOval} />

                        <VSpacer size={2} />
                        <ChipWrapper skills={skills.slice(0, 3)} />
                        <VSpacer size={2} />

                        {
                            isLoggedIn
                            && (
                                <View style={styles.buttonContainer}>
                                    <Button
                                        text={I18n.t('mentorCardFullStory')}
                                        onPress={action}
                                    />
                                </View>
                            )
                        }
                    </View>
                </View>
            </View>
        </View>
    );
};

MentorCard.propTypes = {
    mentor: PropTypes.shape({
        displayName: PropTypes.string.isRequired,
        birthYear: PropTypes.number.isRequired,
        region: PropTypes.string.isRequired,
        story: PropTypes.string.isRequired,
        skills: PropTypes.arrayOf(PropTypes.string),
        languages: PropTypes.arrayOf(PropTypes.string),
    }).isRequired,
    onPress: PropTypes.func,
    isLoggedIn: PropTypes.bool,
};

MentorCard.defaultProps = {
    isLoggedIn: false,
    onPress: () => {},
};

export default MentorCard;
