import React from 'react';
import 'react-native';
import { shallow } from 'enzyme';

import MentorCard from './MentorCard';

describe('MentorCard', () => {
    const janteri = {
        id: 'asdf',
        displayName: 'J. Anteri',
        region: 'Kontula',
        story: 'This is a story',
        languages: ['Finnish', 'Swedish', 'English', 'Russian'],
        skills: ['Coffeemaking', 'Vim'],
        birthYear: 2000,
    };

    test('renders correctly', () => {
        const props = { mentor: janteri };
        const wrapper = shallow(<MentorCard {...props} />);

        expect(wrapper).toMatchSnapshot();
    });

    test('passed function is called on press', () => {
        const mockFunc = jest.fn();
        const props = { mentor: janteri, onPress: mockFunc, isLoggedIn: true };
        const wrapper = shallow(<MentorCard {...props} />);

        wrapper.find('Button').simulate('Press');
        expect(mockFunc).toHaveBeenCalled();
    });
});
