import React from 'react';
import 'react-native';
import { shallow } from 'enzyme';

import HeaderDropdown from './HeaderDropdown';

describe('HeaderDropdown', () => {
    const props = {
        entries: [{
            routeName: '',
            current: false,
            icon: 'bubbles',
            text: 'Bubbly',
            callback: () => { },
        }, {
            routeName: '',
            current: true,
            icon: 'bubbles',
            text: 'Bubbly',
            callback: () => { },
        }],
    };

    test('renders correctly', () => {
        const wrapper = shallow(<HeaderDropdown {...props} />);

        expect(wrapper).toMatchSnapshot();
    });
});
