import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';

import IconButton from '../IconButton';

import styles from './styles';

const HeaderDropdown = ({ entries }) => {
    const { dropdownContainer, textContainer, itemText } = styles;
    return (
        <View style={dropdownContainer}>
            {entries.map(row => (
                <TouchableOpacity
                    style={row.current
                        ? styles.rowContainerCurrent
                        : styles.rowContainer}
                    onPress={row.callback}
                    key={row.text}
                    disabled={row.current}
                >
                    <View style={styles.iconContainer}>
                        <IconButton
                            icon={row.icon}
                            onPress={row.current
                                ? null
                                : row.callback}
                        />
                    </View>
                    <View style={textContainer}>
                        <Text style={itemText}>
                            {row.text}
                        </Text>
                    </View>
                </TouchableOpacity>

            ))}
        </View>
    );
};

HeaderDropdown.propTypes = {
    entries: PropTypes.arrayOf(PropTypes.shape({
        icon: PropTypes.string,
        text: PropTypes.string,
        callback: PropTypes.func,
    })).isRequired,
};

export default HeaderDropdown;
