import { StyleSheet } from 'react-native';

import { colors, fonts, fontSizes } from '../../styles';

export default StyleSheet.create({
    dropdownContainer: {
        backgroundColor: colors.WHITE,
        width: '100%',
    },
    rowContainerCurrent: {
        height: 60,
        width: '100%',
        flexDirection: 'row',
        backgroundColor: colors.BEIGE,
        borderTopWidth: 2,
        borderTopColor: colors.WHITE,
    },
    rowContainer: {
        height: 60,
        width: '100%',
        flexDirection: 'row',
        backgroundColor: colors.GREY_VERY_LIGHT,
        borderTopWidth: 2,
        borderTopColor: colors.WHITE,
    },
    textContainer: {
        flex: 1,
        alignItems: 'flex-start',
        justifyContent: 'center',
    },
    itemText: {
        color: colors.ORANGE,
        fontFamily: fonts.PRIMARY,
        fontSize: fontSizes.HEADING_4,
    },
    iconContainer: {
        minWidth: 70,
        alignItems: 'center',
        justifyContent: 'center',
    },
});
