import React from 'react';
import 'react-native';
import { shallow } from 'enzyme';

import RegisterFormDisplayName from './RegisterFormDisplayName';

describe('RegisterFormDisplayName', () => {
    test('renders correctly', () => {
        const wrapper = shallow(<RegisterFormDisplayName />);
        wrapper.dive().find('TextInput').at(0).props()
            .onChangeText('a');

        expect(wrapper).toMatchSnapshot();
    });

    test('default function is called on press', () => {
        const wrapper = shallow(
            <RegisterFormDisplayName
                handleSubmit={jest.fn()}
            />,
        );

        wrapper.find('Button').dive().simulate('Press');
        expect(wrapper).toMatchSnapshot();
    });

    test('passed function is called on press', () => {
        const mockFunc = jest.fn();
        const props = { handleSubmit: mockFunc };
        const wrapper = shallow(<RegisterFormDisplayName {...props} />);

        wrapper.find('Button').dive().simulate('Press');
        expect(mockFunc).toHaveBeenCalled();
    });

    test('function setRef is called', () => {
        const wrapper = shallow(
            <RegisterFormDisplayName
                handleSubmit={jest.fn()}
            />,
        );
        const spy = jest.spyOn(wrapper.instance(), 'setRef');
        const spyFocus = jest.spyOn(wrapper.instance(), 'setFocus');
        wrapper.update();

        const textInput = wrapper.dive().find('TextInput')
            .at(0)
            .dive()
            .instance();

        wrapper.instance().setRef(textInput, '_region');

        wrapper.instance().setFocus('_region');
        wrapper.dive().find('TextInput').first().dive()
            .instance().props.onSubmitEditing();

        expect(spy).toHaveBeenCalled();
        expect(spyFocus).toHaveBeenCalled();
    });

    test('submits after last input', () => {
        const wrapper = shallow(
            <RegisterFormDisplayName
                handleSubmit={jest.fn()}
            />,
        );
        const spy = jest.spyOn(wrapper.instance(), 'submitRegister');

        wrapper.find('TextInput').at(0).simulate('SubmitEditing');
        expect(spy).toHaveBeenCalled();
    });
});
