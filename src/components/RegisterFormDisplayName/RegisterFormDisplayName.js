import React from 'react';
import PropTypes from 'prop-types';

import { ScreenContainer, BlockContainer } from '../Container';
import { BlockText, ItalicText } from '../Text';
import Button from '../Button';
import OvalHeader from '../OvalHeader';
import { Input } from '../Input';
import ModalContainer from '../../containers/ModalContainer';
import I18n from '../../i18n';
import { colors } from '../../styles';

class RegisterFormDisplayName extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            displayName: props.loginName,
        };
    }

    setRef = (component, target) => {
        this[target] = component;
    };

    setFocus = (target) => {
        this[target].focus();
    };

    submitRegister = () => {
        const { handleSubmit } = this.props;
        handleSubmit(this.state);
    };

    changeState = text => this.setState({ displayName: text })

    render() {
        const { displayName } = this.state;
        const { goBack } = this.props;
        return (
            <ScreenContainer>
                <ModalContainer type="ok" />
                <OvalHeader
                    title={I18n.t('RegisterFormDisplayNameHeaderTitle')}
                    backgroundColor={colors.GREY_VERY_LIGHT}
                    textColor={colors.ORANGE}
                    bigTitle={false}
                />

                <BlockContainer>
                    <BlockText>
                        {I18n.t('RegisterFormDisplayNameTextName')}
                    </BlockText>
                    <Input
                        value={displayName}
                        placeholder={
                            I18n.t('RegisterFormDisplayNamePrefilledName')}
                        changeCallback={this.changeState}
                        submitCallback={this.submitRegister}
                    />
                    <ItalicText>
                        {I18n.t('RegisterFormDisplayNameDescription')}
                    </ItalicText>
                </BlockContainer>

                <Button
                    text={I18n.t('RegisterFormDisplayNameButton')}
                    onPress={this.submitRegister}
                />

                <Button
                    primary={false}
                    text={I18n.t('goBack')}
                    onPress={goBack}
                />

            </ScreenContainer>
        );
    }
}

RegisterFormDisplayName.propTypes = {
    goBack: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    loginName: PropTypes.string.isRequired,
};

export default RegisterFormDisplayName;
