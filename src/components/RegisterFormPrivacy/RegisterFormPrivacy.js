import React from 'react';
import PropTypes from 'prop-types';
import { Linking } from 'react-native';

import { ScreenContainer, BlockContainer } from '../Container';
import { BlockText } from '../Text';
import Button from '../Button';
import TextButton from '../TextButton';
import OvalHeader from '../OvalHeader';
import ModalContainer from '../../containers/ModalContainer';
import { VSpacer } from '../Spacer';

import I18n from '../../i18n';
import { colors } from '../../styles';
import { TERMS_URL } from '../../../config';

const RegisterFormPrivacy = ({ goBack, register }) => (
    <ScreenContainer>
        <ModalContainer type="ok" />
        <OvalHeader
            title={I18n.t('RegisterFormPrivacyHeaderTitle')}
            backgroundColor={colors.ORANGE}
            textColor={colors.GREY_VERY_LIGHT}
            bigTitle={false}
        />

        <BlockContainer>

            <VSpacer size={1} />

            <BlockText>
                {I18n.t('RegisterFormPrivacyDescription1')}
            </BlockText>

            <BlockText>
                {I18n.t('RegisterFormPrivacyDescription2')}
            </BlockText>

            <BlockText>
                {I18n.t('RegisterFormPrivacyDescription3')}
            </BlockText>

            <VSpacer size={1} />

            <TextButton
                onPress={() => Linking.openURL(TERMS_URL)}
                text={I18n.t('RegisterFormPrivacyLink')}
            />

            <VSpacer size={2} />

            <Button
                text={I18n.t('RegisterFormPrivacyButton')}
                onPress={register}
            />

            <Button
                primary={false}
                text={I18n.t('goBack')}
                onPress={goBack}
            />

            <VSpacer size={1} />

        </BlockContainer>
    </ScreenContainer>
);

RegisterFormPrivacy.propTypes = {
    goBack: PropTypes.func.isRequired,
    register: PropTypes.func.isRequired,
};

export default RegisterFormPrivacy;
