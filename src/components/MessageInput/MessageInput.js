import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import RN from 'react-native';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';

import I18n from '../../i18n';

import { sendMessage, setMessage } from '../../actions/chats';

import { colors, fonts, fontSizes } from '../../styles';

const minHeight = fontSizes.BODY_TEXT * 5;

const styles = RN.StyleSheet.create({
    inputContainer: {
        minHeight,
        flexDirection: 'row',
        borderStyle: 'solid',
        borderTopWidth: 0,
        backgroundColor: colors.WHITE,
    },
    inputField: {
        minHeight,
        fontFamily: fonts.PRIMARY,
        fontSize: fontSizes.BODY_TEXT,
        color: colors.GREY_DARK,
        flex: 1,
        paddingHorizontal: 21,
        borderColor: colors.GREY_10,
        borderTopWidth: 1,
    },
    sendButtonContainer: {
        minHeight,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.GREEN_PRIMARY,
        width: 55,
    },
    sendButtonImage: {
        fontSize: 20,
        color: colors.WHITE,
        transform: [{ rotate: '330deg' }],
    },
});

class MessageInput extends React.Component {
    sendMessage = () => {
        const { send, recipientId, setContent, messageDrafts } = this.props;
        const content = messageDrafts[recipientId] || '';

        if (content.trim().length === 0) {
            return;
        }

        send(
            recipientId,
            content,
        );
        setContent(recipientId, '');
    }

    render() {
        const { onFocus, recipientId, setContent, messageDrafts } = this.props;

        const content = messageDrafts[recipientId] || '';

        const onChange = str => setContent(recipientId, str);
        return (
            <RN.View style={styles.inputContainer}>
                <RN.TextInput
                    onFocus={onFocus}
                    onChangeText={onChange}
                    value={content}
                    style={styles.inputField}
                    placeholder={I18n.t('chatInputPrefill')}
                    placeholderTextColor={colors.GREY_LIGHT}
                    multiline
                    underlineColorAndroid="transparent"
                    textAlignVertical="top"
                />
                <RN.TouchableOpacity
                    style={styles.sendButtonContainer}
                    onPress={() => this.sendMessage()}
                >
                    <MaterialIcon
                        name="send"
                        style={styles.sendButtonImage}
                    />
                </RN.TouchableOpacity>
            </RN.View>
        );
    }
}

MessageInput.propTypes = {
    onFocus: PropTypes.func.isRequired,
    setContent: PropTypes.func.isRequired,
    send: PropTypes.func.isRequired,
    recipientId: PropTypes.string.isRequired,
    messageDrafts: PropTypes.objectOf(PropTypes.string).isRequired,
};

export default connect(
    ({ chats: { recipientId, messageDrafts } }) => ({
        recipientId,
        messageDrafts,
    }),
    dispatch => ({
        send: (recipientId, content) => dispatch(
            sendMessage(recipientId, content),
        ),
        setContent: (recipientId, content) => dispatch(
            setMessage(recipientId, content),
        ),
    }),
)(MessageInput);
