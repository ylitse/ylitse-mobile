import React from 'react';
import 'react-native';
import renderer from 'react-test-renderer';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';

import MessageInput from './MessageInput';

describe('MessageInput', () => {
    const mockStore = configureMockStore([thunk]);
    const state = { chats: { recipientId: 'asdfqwerasdf' } };
    test('renders correctly', () => {
        const store = mockStore(state);
        const wrapper = renderer.create(
            <Provider store={store}>
                <MessageInput />
            </Provider>,
        );
        expect(wrapper).toMatchSnapshot();
    });
});

