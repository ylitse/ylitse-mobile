import React from 'react';
import PropTypes from 'prop-types';
import { View, StyleSheet } from 'react-native';

import { spacings } from '../../styles';

const styles = StyleSheet.create({
    vSpacer: {
        width: '100%',
    },
    hSpacer: {
        height: '100%',
    },
});

export const VSpacer = ({ size }) => (
    <View style={[styles.vSpacer, { height: spacings[size] }]} />
);

export const HSpacer = ({ size }) => (
    <View style={[styles.hSpacer, { width: spacings[size] }]} />
);

VSpacer.propTypes = {
    size: PropTypes.number.isRequired,
};

HSpacer.propTypes = {
    size: PropTypes.number.isRequired,
};

export default VSpacer;
