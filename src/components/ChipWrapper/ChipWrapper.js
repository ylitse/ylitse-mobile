import React from 'react';
import PropTypes from 'prop-types';
import { View, Text } from 'react-native';
import I18n from '../../i18n';

import Chip from '../Chip';

import styles from './styles';

const ChipWrapper = (props) => {
    const { skills } = props;

    return (
        <View style={styles.container}>
            <Text style={styles.helpText}>
                {I18n.t('chipWrapperICanHelpWith')}
            </Text>
            <View style={styles.chipContainer}>
                {skills.map(skill => (
                    <Chip text={skill} key={skill} />
                ))}
            </View>
        </View>
    );
};

ChipWrapper.propTypes = {
    skills: PropTypes.arrayOf(PropTypes.string),
};

ChipWrapper.defaultProps = {
    skills: [
        'Coffeemaking',
        'Installing Gentoo',
    ],
};

export default ChipWrapper;
