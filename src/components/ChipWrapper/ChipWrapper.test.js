import React from 'react';
import 'react-native';
import { shallow } from 'enzyme';

import ChipWrapper from './ChipWrapper';

describe('ChipWrapper', () => {
    test('renders correctly', () => {
        const wrapper = shallow(<ChipWrapper />);

        expect(wrapper).toMatchSnapshot();
    });
});
