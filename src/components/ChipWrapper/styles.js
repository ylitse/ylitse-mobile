import { StyleSheet } from 'react-native';

import { colors, fonts, fontSizes } from '../../styles';

export default StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10,
        flexShrink: 1,
    },
    chipContainer: {
        paddingTop: 8,
        paddingBottom: 8,
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'flex-start',
        justifyContent: 'center',
    },
    helpText: {
        color: colors.GREY_DARK,
        fontFamily: fonts.PRIMARY,
        fontSize: fontSizes.HEADING_4,
    },
});
