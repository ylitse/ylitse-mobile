import { StyleSheet } from 'react-native';

import { colors, fonts } from '../../styles';

export default StyleSheet.create({
    button: {
        paddingVertical: 7,
        paddingHorizontal: 36,
        borderRadius: 40,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 10,
    },
    buttonSecondary: {
        backgroundColor: 'transparent',
        borderWidth: 1,
        paddingVertical: 7,
        paddingHorizontal: 36,
        borderRadius: 40,
        margin: 10,
        alignSelf: 'center',
    },
    buttonTextSecondary: {
        fontFamily: fonts.PRIMARY,
        fontSize: 17,
    },
    buttonText: {
        color: colors.GREY_VERY_LIGHT,
        fontFamily: fonts.PRIMARY,
        fontSize: 17,
    },
});
