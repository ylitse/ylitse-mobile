import React from 'react';
import 'react-native';
import { shallow } from 'enzyme';

import Button from './Button';

describe('Button', () => {
    test('renders primary correctly', () => {
        const props = {
            text: 'Lorem ipsum', onPress: () => { },
        };
        const wrapper = shallow(<Button {...props} primary />);

        expect(wrapper).toMatchSnapshot();
    });

    test('renders secondary correctly', () => {
        const props = {
            text: 'Lorem ipsum', onPress: () => { }, color: 'red',
        };
        const wrapper = shallow(<Button {...props} primary={false} />);

        expect(wrapper).toMatchSnapshot();
    });

    test('renders correctly when pressed', () => {
        const props = { text: 'Lorem ipsum' };
        const wrapper = shallow(<Button {...props} />);

        wrapper.simulate('Press');
        expect(wrapper).toMatchSnapshot();
    });

    test('function is called on press', () => {
        const mockFunc = jest.fn();
        const props = {
            text: 'Lorem ipsum', onPress: mockFunc,
        };
        const wrapper = shallow(<Button {...props} />);

        wrapper.simulate('Press');
        expect(mockFunc).toHaveBeenCalled();
    });
});
