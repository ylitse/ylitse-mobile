import React from 'react';
import PropTypes from 'prop-types';
import { Text, TouchableOpacity } from 'react-native';
import { colors } from '../../styles';

import styles from './styles';

const Button = (props) => {
    const { GREEN_PRIMARY, ORANGE } = colors;
    const {
        text = 'A button',
        primary = true,
        color = primary ? GREEN_PRIMARY : ORANGE,
        onPress = () => {},
    } = props;

    return (
        <TouchableOpacity
            style={primary
                ? [styles.button, { backgroundColor: color }]
                : [styles.buttonSecondary, { borderColor: color }]}
            onPress={onPress}
        >
            <Text
                style={primary
                    ? styles.buttonText
                    : [styles.buttonTextSecondary, { color }]}
            >
                {text}
            </Text>
        </TouchableOpacity>
    );
};

Button.propTypes = {
    text: PropTypes.string,
    primary: PropTypes.bool,
    color: PropTypes.string,
    onPress: PropTypes.func,
};

Button.defaultProps = {
    text: undefined,
    primary: undefined,
    color: undefined,
    onPress: undefined,
};

export default Button;
