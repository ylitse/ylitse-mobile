import React from 'react';
import 'react-native';
import { shallow } from 'enzyme';

import ChatBubble from './ChatBubble';

describe('ChatBubble', () => {
    test('renders correctly', () => {
        const messageA = {
            isOurs: true,
            content: 'A',
            created: (new Date(1348932)).toISOString(),
            bubbleSpike: true,
        };
        const messageB = {
            isOurs: false,
            content: 'B',
            created: (new Date(1310900)).toISOString(),
            bubbleSpike: false,
        };
        const sentWrapper = shallow(<ChatBubble message={messageA} />);
        const receivedWrapper = shallow(<ChatBubble message={messageB} />);

        expect(sentWrapper).toMatchSnapshot();
        expect(receivedWrapper).toMatchSnapshot();
    });
});
