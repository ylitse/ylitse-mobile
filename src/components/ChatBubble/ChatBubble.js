import React from 'react';
import PropTypes from 'prop-types';
import { View, Text } from 'react-native';

import styles from './styles';

const ChatBubble = (props) => {
    const {
        message,
    } = props;
    const {
        bubbleSpike,
        isOurs,
        created,
        content,
    } = message;

    const date = new Date(created);
    const prefixByZero = x => (x < 10 && `0${x}`) || `${x}`;
    const timehhmm = `${date.getHours()}:${prefixByZero(date.getMinutes())}`;

    const spikeStyle = !bubbleSpike && styles.noSpike;
    const spaceStyle = !bubbleSpike && styles.sameUser;

    if (isOurs) {
        return (
            <View style={[styles.containerByUser, spaceStyle]}>
                <View style={[styles.bubbleMain, styles.bubbleByUser]}>
                    <View>
                        <Text
                            style={[styles.text, styles.textByUser]}
                            selectable
                        >
                            {content}
                        </Text>
                    </View>
                    <View style={styles.viewTime}>
                        <Text style={styles.textTime}>
                            {timehhmm}
                        </Text>
                    </View>
                </View>
                <View style={[styles.bubbleSpikeByUser, spikeStyle]} />
            </View>
        );
    }
    return (
        <View style={[styles.containerNotByUser, spaceStyle]}>
            <View style={[styles.bubbleSpikeNotByUser, spikeStyle]} />
            <View style={[styles.bubbleMain, styles.bubbleNotByUser]}>
                <View>
                    <Text style={[styles.text, styles.textNotByUser]}>
                        {content}
                    </Text>
                </View>
                <View style={styles.viewTime}>
                    <Text style={styles.textTime}>
                        {timehhmm}
                    </Text>
                </View>
            </View>
        </View>
    );
};

ChatBubble.propTypes = {
    message: PropTypes.shape({
        bubbleSpike: PropTypes.bool.isRequired,
        content: PropTypes.string.isRequired,
        created: PropTypes.string.isRequired,
        isOurs: PropTypes.bool.isRequired,
    }).isRequired,
};

export default ChatBubble;
