import { StyleSheet } from 'react-native';

import { colors, fonts, fontSizes } from '../../styles';

export default StyleSheet.create({
    containerByUser: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
        marginBottom: 5,
        marginTop: 5,
        marginLeft: 20,
    },
    containerNotByUser: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 5,
        marginTop: 5,
    },
    sameUser: {
        marginBottom: 2,
        marginTop: 2,
    },
    noSpike: {
        borderBottomColor: 'transparent',
    },
    noSpikeByUser: {
        marginRight: 24,
    },
    bubbleSpikeByUser: {
        width: 0,
        height: 0,
        backgroundColor: 'transparent',
        borderStyle: 'solid',
        borderLeftWidth: 12,
        borderRightWidth: 12,
        borderBottomWidth: 16,
        borderLeftColor: 'transparent',
        borderRightColor: 'transparent',
        transform: [
            { rotate: '90deg' },
        ],
        borderBottomColor: colors.BEIGE,
        right: 2,
    },
    bubbleSpikeNotByUser: {
        width: 0,
        height: 0,
        backgroundColor: 'transparent',
        borderStyle: 'solid',
        borderLeftWidth: 12,
        borderRightWidth: 12,
        borderBottomWidth: 16,
        borderLeftColor: 'transparent',
        borderRightColor: 'transparent',
        transform: [
            { rotate: '-90deg' },
        ],
        borderBottomColor: colors.GREY_VERY_LIGHT,
        left: 0,
    },
    bubbleMain: {
        borderRadius: 10,
        padding: 10,
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'flex-end',
    },
    bubbleNotByUser: {
        backgroundColor: colors.GREY_VERY_LIGHT,
        marginLeft: -7,
        marginRight: 60,
    },
    bubbleByUser: {
        backgroundColor: colors.BEIGE,
        marginRight: -7,
        marginLeft: 60,
    },
    text: {
        fontFamily: fonts.PRIMARY,
        fontSize: fontSizes.BODY_TEXT,
    },
    textNotByUser: {
        color: colors.GREY_DARK,
    },
    textByUser: {
        color: colors.ORANGE,
    },
    viewTime: {
        marginLeft: 20,
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
    },
    textTime: {
        color: colors.GREY_30,
        fontSize: fontSizes.SMALL,
    },
});
