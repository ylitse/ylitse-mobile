import { StyleSheet } from 'react-native';

import { colors, fonts, fontSizes } from '../../styles';

export default StyleSheet.create({
    chip: {
        backgroundColor: colors.BEIGE,
        paddingVertical: 5,
        paddingHorizontal: 10,
        borderRadius: 40,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 3,
    },
    chipText: {
        color: colors.ORANGE,
        fontFamily: fonts.PRIMARY,
        fontSize: fontSizes.BODY_TEXT,
    },
});
