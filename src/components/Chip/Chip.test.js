import React from 'react';
import 'react-native';
import { shallow } from 'enzyme';

import Chip from './Chip';

describe('Chip', () => {
    test('renders correctly', () => {
        const props = {
            text: 'Hipsun hapsun',
        };
        const wrapper = shallow(<Chip {...props} />);

        expect(wrapper).toMatchSnapshot();
    });
});
