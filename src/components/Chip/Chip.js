import React from 'react';
import PropTypes from 'prop-types';
import { View, Text } from 'react-native';

import styles from './styles';

const Chip = (props) => {
    const { text } = props;

    return (
        <View style={styles.chip}>
            <Text style={styles.chipText}>
                {text}
            </Text>
        </View>
    );
};

Chip.propTypes = {
    text: PropTypes.string,
};

Chip.defaultProps = {
    text: 'A chip',
};

export default Chip;
