import React from 'react';
import 'react-native';
import { shallow } from 'enzyme';

import MentorList from './MentorList';

describe('MentorList', () => {
    const mentors = {
        asdf: {
            id: 'asdf',
            displayName: 'J. Anteri',
            region: 'Kontula',
            story: 'This is a story',
            languages: ['Finnish', 'Swedish', 'English', 'Russian'],
            skills: ['Coffeemaking', 'Vim'],
            birthYear: 2000,
        },
    };
    const mockFunc = jest.fn();

    test('renders correctly', () => {
        const props = {
            mentorObject: mentors,
            handleSubmit: () => {},
        };
        const wrapper = shallow(<MentorList {...props} />);

        expect(wrapper).toMatchSnapshot();
    });

    test('default submit function is called on press', () => {
        const props = {
            mentorObject: mentors,
            handleSubmit: () => {},
        };
        const wrapper = shallow(<MentorList {...props} />);

        wrapper.find('Button').dive().simulate('Press');
        expect(wrapper).toMatchSnapshot();
    });

    test('passed submit function is called on press', () => {
        const props = {
            mentorObject: mentors,
            handleSubmit: mockFunc,
        };
        const wrapper = shallow(<MentorList {...props} />);

        wrapper.find('Button').dive().simulate('Press');
        expect(mockFunc).toHaveBeenCalled();
    });

    test('FlatList renders correctly', () => {
        const props = {
            mentorObject: mentors,
            handleSubmit: () => {},
        };
        const wrapper = shallow(<MentorList {...props} />);

        wrapper.find('FlatList').dive().props().renderItem(
            { item: mentors.asdf },
        );
        wrapper.find('FlatList').dive().props().keyExtractor(
            { item: mentors.asdf },
            'asdf',
        );

        expect(wrapper).toMatchSnapshot();
    });
});
