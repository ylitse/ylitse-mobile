import React from 'react';
import PropTypes from 'prop-types';
import { Platform } from 'react-native';

import { ScreenContainer } from '../Container';
import { Title, CenteredBlockText } from '../Text';
import { VSpacer } from '../Spacer';
import Button from '../Button';
import MentorScrollList from '../MentorScrollList';

import I18n from '../../i18n';
import ModalContainer from '../../containers/ModalContainer';

import compareMentors from '../../utils/compareMentors';

const MentorList = ({ handleSubmit, mentorObject }) => {
    const upperSpacing = Platform.OS === 'ios' ? 3 : 1;
    const mentors = Object.values(mentorObject).sort(compareMentors);
    return (
        <ScreenContainer>
            <ModalContainer type="ok" />
            <VSpacer size={upperSpacing} />

            <Title>
                {I18n.t('mentorListMeetOurMentors')}
            </Title>

            <MentorScrollList mentorList={mentors} />

            <CenteredBlockText>
                {I18n.t('mentorListCreateAccount')}
            </CenteredBlockText>
            <Button
                text={I18n.t('mentorListGetStarted')}
                onPress={handleSubmit}
            />

            <VSpacer size={1} />
        </ScreenContainer>
    );
};

MentorList.propTypes = {
    handleSubmit: PropTypes.func.isRequired,
    mentorObject: PropTypes.objectOf(PropTypes.shape({
        displayName: PropTypes.string.isRequired,
        birthYear: PropTypes.number.isRequired,
        region: PropTypes.string.isRequired,
        story: PropTypes.string.isRequired,
        skills: PropTypes.arrayOf(PropTypes.string),
        languages: PropTypes.arrayOf(PropTypes.string),
    })).isRequired,
};

export default MentorList;
