import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';

import { ScreenContainer, BlockContainer } from '../Container';
import { BlockText, ItalicText } from '../Text';
import Button from '../Button';
import { Input } from '../Input';
import OvalHeader from '../OvalHeader';
import ModalContainer from '../../containers/ModalContainer';

import I18n from '../../i18n';
import { colors } from '../../styles';

class RegisterFormEmail extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            email: '',
        };
    }

    submitRegister = () => {
        const { handleSubmit } = this.props;
        handleSubmit(this.state);
    };

    skipRegister = () => {
        const { skipRegister } = this.props;
        skipRegister();
    };

    changeEmail = email => this.setState({ email })

    render() {
        const { goBack } = this.props;
        const { email } = this.state;
        const {
            GREEN_PRIMARY,
            GREY_VERY_LIGHT,
            ORANGE,
        } = colors;
        return (
            <ScreenContainer>
                <ModalContainer type="ok" />
                <OvalHeader
                    title={I18n.t('registerFormEmailHeaderTitle')}
                    backgroundColor={GREY_VERY_LIGHT}
                    textColor={ORANGE}
                    bigTitle={false}
                />

                <BlockContainer>
                    <ItalicText>
                        {I18n.t('registerFormEmailInputTitle')}
                    </ItalicText>
                    <Input
                        value={email}
                        placeholder={
                            I18n.t('registerFormEmailInputPlaceholder')}
                        changeCallback={this.changeEmail}
                        submitCallback={this.submitRegister}
                    />
                    <BlockText>
                        {I18n.t('registerFormEmailDescription')}
                    </BlockText>
                </BlockContainer>

                <View>
                    <Button
                        text={I18n.t('registerFormEmailSaveButton')}
                        onPress={this.submitRegister}
                    />
                    <Button
                        text={I18n.t('registerFormSkipButton')}
                        color={GREEN_PRIMARY}
                        onPress={this.skipRegister}
                        primary={false}
                    />
                    <Button
                        primary={false}
                        text={I18n.t('goBack')}
                        onPress={goBack}
                    />
                </View>
            </ScreenContainer>
        );
    }
}

RegisterFormEmail.propTypes = {
    goBack: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    skipRegister: PropTypes.func.isRequired,
};

export default RegisterFormEmail;
