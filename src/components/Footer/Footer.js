import React from 'react';
import { View, Text } from 'react-native';

import styles from './styles';
import I18n from '../../i18n';

const Footer = () => (
    <View style={styles.footer}>
        <Text style={styles.footerText}>
            {I18n.t('footerBroughtToYou')}
        </Text>
    </View>
);

export default Footer;
