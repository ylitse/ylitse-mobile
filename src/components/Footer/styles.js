import { StyleSheet } from 'react-native';

import { spacings, colors, fonts, fontSizes } from '../../styles';

export default StyleSheet.create({
    footer: {
        width: '100%',
        paddingVertical: spacings[1],
        backgroundColor: colors.GREY_VERY_LIGHT,
        alignItems: 'center',
        justifyContent: 'center',
    },
    footerText: {
        color: colors.GREY_50,
        fontFamily: fonts.PRIMARY,
        fontSize: fontSizes.SMALL,
    },
});
