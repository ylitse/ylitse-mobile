import React from 'react';
import 'react-native';
import { shallow } from 'enzyme';

import TextButton from './TextButton';

describe('TextButton', () => {
    test('renders correctly', () => {
        const props = {
            text: 'Lorem ipsum', onPress: () => {},
        };
        const wrapper = shallow(<TextButton {...props} />);

        expect(wrapper).toMatchSnapshot();
    });

    test('renders correctly when pressed', () => {
        const props = { text: 'Lorem ipsum' };
        const wrapper = shallow(<TextButton {...props} />);

        wrapper.simulate('Press');
        expect(wrapper).toMatchSnapshot();
    });

    test('function is called on press', () => {
        const mockFunc = jest.fn();
        const props = {
            text: 'Lorem ipsum', onPress: mockFunc,
        };
        const wrapper = shallow(<TextButton {...props} />);

        wrapper.simulate('Press');
        expect(mockFunc).toHaveBeenCalled();
    });
});
