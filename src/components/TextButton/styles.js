import { StyleSheet } from 'react-native';

import { colors, fonts, fontSizes } from '../../styles';

export default StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    text: {
        color: colors.GREEN_PRIMARY,
        fontFamily: fonts.PRIMARY,
        fontSize: fontSizes.BODY_TEXT,
    },
});
