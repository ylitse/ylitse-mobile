import React from 'react';
import PropTypes from 'prop-types';
import { Text, TouchableOpacity } from 'react-native';

import styles from './styles';

const TextButton = (props) => {
    const { text, onPress } = props;

    return (
        <TouchableOpacity style={styles.container} onPress={onPress}>
            <Text style={styles.text}>
                {text}
            </Text>
        </TouchableOpacity>
    );
};

TextButton.propTypes = {
    text: PropTypes.string,
    onPress: PropTypes.func,
};

TextButton.defaultProps = {
    text: 'A button',
    onPress: () => {},
};

export default TextButton;
