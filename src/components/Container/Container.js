import React from 'react';
import * as RN from 'react-native';
import PropTypes from 'prop-types';
import { colors } from '../../styles';
import Footer from '../Footer';
import HeaderContainer from '../../containers/HeaderContainer';

/** Component styles */
const styles = RN.StyleSheet.create({
    scrollContentContainer: {
        backgroundColor: colors.WHITE,
        flexGrow: 1,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'space-between',
    },

    headerScreenContainer: {
        flex: 1,
    },

    blockContainer: {
        alignItems: 'center',
        width: '80%',
    },
});

/** Container for wrapping every screen. Takes care of
 * overflow scrolling, centering and component flow.
 */
export const ScreenContainer = ({ children }) => (
    <RN.ScrollView
        style={styles.scrollContainer}
        contentContainerStyle={styles.scrollContentContainer}
    >
        {children}
        <Footer />
    </RN.ScrollView>
);

/** Container for screens with Header visible */
export const HeaderScreenContainer = ({ children }) => (
    <RN.SafeAreaView style={styles.headerScreenContainer}>
        <HeaderContainer />
        <ScreenContainer>
            {children}
        </ScreenContainer>
    </RN.SafeAreaView>
);

/** Container including Header for logged in users */
export const LoggedInContainer = ({ children }) => (
    <RN.SafeAreaView style={styles.container}>
        <HeaderContainer />
        <RN.ScrollView
            style={styles.scrollContainer}
            contentContainerStyle={styles.scrollContentContainer}
        >
            {children}
            <Footer />
        </RN.ScrollView>
    </RN.SafeAreaView>
);

/** Container for a block of tightly-grouped components */
export const BlockContainer = ({ children }) => (
    <RN.View style={styles.blockContainer}>
        {children}
    </RN.View>
);

ScreenContainer.propTypes = {
    children: PropTypes.node.isRequired,
};

LoggedInContainer.propTypes = {
    children: PropTypes.node.isRequired,
};

BlockContainer.propTypes = {
    children: PropTypes.node.isRequired,
};

HeaderScreenContainer.propTypes = {
    children: PropTypes.node.isRequired,
};

export default ScreenContainer;
