import { StyleSheet, Dimensions } from 'react-native';

import { colors, fonts, fontSizes } from '../../styles';

const { width } = Dimensions.get('window');

export default StyleSheet.create({
    container: {
        backgroundColor: colors.WHITE,
        justifyContent: 'center',
        alignItems: 'center',
        height: '100%',
        width,
    },
    scrollContainer: {
        padding: 15,
    },
    sectionContainer: {
        padding: 10,
        alignItems: 'center',
    },

    smallText: {
        color: colors.GREY_DARK,
        fontFamily: fonts.PRIMARY,
        fontSize: fontSizes.SMALL,
    },
    nameText: {
        color: colors.GREY_DARK,
        fontFamily: fonts.PRIMARY,
        fontSize: fontSizes.HEADING_3,
    },
    bodyText: {
        color: colors.GREY_DARK,
        fontFamily: fonts.PRIMARY,
        fontSize: fontSizes.BODY_TEXT,
        textAlign: 'center',
    },
});
