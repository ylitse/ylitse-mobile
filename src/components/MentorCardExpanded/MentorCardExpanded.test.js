import React from 'react';
import 'react-native';
import { shallow } from 'enzyme';

import MentorCardExpanded from './MentorCardExpanded';

const props = {
    mentor: {
        displayName: 'J. Anteri',
        gender: 'male',
        region: 'Kontula',
        story: 'This is a story',
        languages: ['fi', 'sv', 'en', 'ru'],
        skills: [],
        birthYear: 2000,
        userId: 'asdf',
    },
    goToChat: jest.fn(),
};

describe('MentorCardExpanded', () => {
    test('renders correctly', () => {
        const wrapper = shallow(<MentorCardExpanded {...props} />);

        wrapper.find('Button').dive().simulate('Press');
        expect(wrapper).toMatchSnapshot();
    });

    test('function is called on press', () => {
        const mockFunc = jest.fn();
        const wrapper = shallow(
            <MentorCardExpanded
                mentor={props.mentor}
                goToChat={mockFunc}
            />,
        );

        wrapper.find('Button').dive().simulate('Press');
        expect(mockFunc).toHaveBeenCalled();
    });
});
