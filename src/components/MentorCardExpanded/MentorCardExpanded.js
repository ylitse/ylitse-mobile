import React from 'react';
import PropTypes from 'prop-types';
import * as RN from 'react-native';

import ChipWrapper from '../ChipWrapper';
import Button from '../Button';
import ProfileImage from '../ProfileImage';
import Footer from '../Footer';

import styles from './styles';
import I18n from '../../i18n';
import HeaderContainer from '../../containers/HeaderContainer';

const MentorCardExpanded = (props) => {
    const {
        mentor: {
            displayName, region, story, skills, languages, gender, birthYear,
        },
        goToChat,
    } = props;

    return (
        <RN.SafeAreaView style={styles.container}>
            <HeaderContainer />
            <RN.ScrollView contentContainerStyle={styles.scrollContainer}>

                <RN.View style={styles.sectionContainer}>
                    <ProfileImage
                        languages={languages}
                        align="bottom"
                        gender={gender}
                    />
                </RN.View>

                <RN.View style={styles.sectionContainer}>
                    <RN.Text style={styles.nameText}>
                        {displayName}
                    </RN.Text>
                    <RN.Text style={styles.smallText}>
                        {2017 - birthYear} v. | {region}
                    </RN.Text>
                </RN.View>

                <RN.View style={styles.sectionContainer}>
                    <Button
                        text={I18n.t('mentorCardSendMessage')}
                        onPress={() => (goToChat())}
                    />
                </RN.View>

                <RN.View style={styles.sectionContainer}>
                    <RN.Text style={styles.bodyText}>
                        {story}
                    </RN.Text>
                </RN.View>

                <RN.View style={styles.sectionContainer}>
                    <ChipWrapper skills={skills} />
                </RN.View>

            </RN.ScrollView>
            <Footer />
        </RN.SafeAreaView>
    );
};

MentorCardExpanded.propTypes = {
    mentor: PropTypes.shape({
        displayName: PropTypes.string.isRequired,
        birthYear: PropTypes.number.isRequired,
        region: PropTypes.string.isRequired,
        story: PropTypes.string.isRequired,
        skills: PropTypes.arrayOf(PropTypes.string),
        languages: PropTypes.arrayOf(PropTypes.string),
        userId: PropTypes.string.isRequired,
    }).isRequired,
    goToChat: PropTypes.func.isRequired,
};

export default MentorCardExpanded;
