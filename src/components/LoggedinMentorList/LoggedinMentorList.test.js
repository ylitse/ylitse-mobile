import React from 'react';
import 'react-native';
import { shallow } from 'enzyme';

import LoggedinMentorList from './LoggedinMentorList';

describe('LoggedinMentorList', () => {
    const janteri = {
        id: 'asdf',
        displayName: 'J. Anteri',
        region: 'Kontula',
        story: 'This is a story',
        languages: ['Finnish', 'Swedish', 'English', 'Russian'],
        skills: ['Coffeemaking', 'Vim'],
        birthYear: 2000,
    };

    test('renders correctly', () => {
        const props = {
            mentorObject: { asdf: janteri },
            mentorId: '401290384wer',
        };
        const wrapper = shallow(<LoggedinMentorList {...props} />);

        expect(wrapper).toMatchSnapshot();
    });

    test('FlatList renders correctly', () => {
        const props = { mentorObject: { asdf: janteri } };
        const wrapper = shallow(<LoggedinMentorList {...props} />);

        wrapper.find('FlatList').dive().props().renderItem({ item: janteri });
        wrapper.find('FlatList').dive().props().keyExtractor(
            { item: janteri },
            'asdf',
        );
        expect(wrapper).toMatchSnapshot();
    });
});
