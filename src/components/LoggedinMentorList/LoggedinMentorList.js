import React from 'react';
import PropTypes from 'prop-types';

import { HeaderScreenContainer } from '../Container';
import ModalContainer from '../../containers/ModalContainer';
import MentorScrollList from '../MentorScrollList';

import compareMentors from '../../utils/compareMentors';

const LoggedinMentorList = ({ handleCardPress, mentorObject, mentorId }) => (
    <HeaderScreenContainer>
        <ModalContainer type="ok" />

        <MentorScrollList
            mentorList={
                Object.values(mentorObject).sort(compareMentors)
                    .filter(x => x.id !== mentorId)
            }
            loggedIn
            onPressCallback={handleCardPress}
        />

    </HeaderScreenContainer>
);

LoggedinMentorList.propTypes = {
    mentorId: PropTypes.string.isRequired,
    mentorObject: PropTypes.objectOf(PropTypes.shape({
        displayName: PropTypes.string.isRequired,
        birthYear: PropTypes.number.isRequired,
        region: PropTypes.string.isRequired,
        story: PropTypes.string.isRequired,
        skills: PropTypes.arrayOf(PropTypes.string),
        languages: PropTypes.arrayOf(PropTypes.string),
    })).isRequired,
    handleCardPress: PropTypes.func.isRequired,
};

export default LoggedinMentorList;
