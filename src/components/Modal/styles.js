import { StyleSheet, Dimensions } from 'react-native';

import { colors, fonts, fontSizes } from '../../styles';

const { width, height } = Dimensions.get('window');

export default StyleSheet.create({
    container: {
        position: 'absolute',
        left: 0,
        right: 0,
        flex: 1,
        width,
        height,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(0,0,0,0.4)',
        zIndex: 9999,
        elevation: 9999,
    },
    modalContainer: {
        minHeight: 150,
        minWidth: 250,
        maxWidth: width - 10,
        paddingHorizontal: 15,
        borderRadius: 10,
        backgroundColor: colors.WHITE,
        justifyContent: 'center',
        alignItems: 'center',
    },
    modalText: {
        color: colors.GREY_50,
        fontFamily: fonts.PRIMARY,
        fontSize: fontSizes.HEADING_3,
        marginBottom: 20,
    },
    buttons: {
        flexDirection: 'row',
    },
});
