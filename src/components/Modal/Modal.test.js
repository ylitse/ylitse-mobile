import React from 'react';
import 'react-native';
import { shallow } from 'enzyme';

import Modal from './Modal';

describe('Modal', () => {
    test('renders correctly', () => {
        const props = {
            message: 'Lorem ipsum',
            closeModal: () => { },
            type: 'ok',
        };
        const wrapper = shallow(<Modal {...props} />);

        expect(wrapper).toMatchSnapshot();
    });

    test('function is called on press', () => {
        const mockFunc = jest.fn();
        const props = {
            message: 'Lorem ipsum',
            closeModal: mockFunc,
        };
        const wrapper = shallow(<Modal {...props} />);

        wrapper.find('Button').simulate('Press');
        expect(mockFunc).toHaveBeenCalled();
    });

    test('default prop function is called on press', () => {
        const mockFunc = jest.fn();
        const props = {
            message: 'Lorem ipsum',
            closeModal: mockFunc,
            type: 'yes-no',
        };
        const wrapper = shallow(<Modal {...props} />);

        wrapper.find('Button').first().simulate('Press');
        wrapper.find('Button').at(1).simulate('Press');
        expect(mockFunc).toHaveBeenCalled();
    });
});
