import React from 'react';
import { Text, View } from 'react-native';
import PropTypes from 'prop-types';

import Button from '../Button';
import I18n from '../../i18n';

import styles from './styles';

const Modal = (props) => {
    const { type, closeModal, handleYes, message } = props;
    function renderButton() {
        switch (type) {
            case 'yes-no':
                return (
                    <View style={styles.buttons}>
                        <Button
                            onPress={closeModal}
                            text={I18n.t('modalNoButton')}
                        />
                        <Button
                            onPress={handleYes}
                            text={I18n.t('modalYesButton')}
                        />
                    </View>
                );

            case 'ok':
                return (
                    <Button
                        onPress={closeModal}
                        text={I18n.t('modalFailedButton')}
                    />
                );

            default:
                return (
                    <Button
                        onPress={closeModal}
                        text={I18n.t('modalFailedButton')}
                    />
                );
        }
    }

    return (
        <View style={styles.container}>
            <View style={styles.modalContainer}>
                <Text style={styles.modalText}>
                    {message}
                </Text>
                {renderButton()}
            </View>
        </View>
    );
};

Modal.propTypes = {
    message: PropTypes.string.isRequired,
    closeModal: PropTypes.func.isRequired,
    type: PropTypes.string.isRequired,
    handleYes: PropTypes.func,
};

Modal.defaultProps = {
    handleYes: () => { },
};

export default Modal;
