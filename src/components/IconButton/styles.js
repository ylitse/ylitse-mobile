import { StyleSheet } from 'react-native';

import { colors } from '../../styles';

export default StyleSheet.create({
    button: {
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: '100%',
    },
    icon: {
        fontSize: 25,
        color: colors.ORANGE,
    },
});
