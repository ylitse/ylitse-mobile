import React from 'react';
import 'react-native';
import { shallow } from 'enzyme';

import IconButton from './IconButton';

describe('IconButton', () => {
    test('renders correctly', () => {
        const props = {
            icon: 'arrow-left', onPress: () => {},
        };
        const wrapper = shallow(<IconButton {...props} />);

        expect(wrapper).toMatchSnapshot();
    });

    test('renders correctly when pressed', () => {
        const props = { icon: 'arrow-left' };
        const wrapper = shallow(<IconButton {...props} />);

        wrapper.simulate('Press');
        expect(wrapper).toMatchSnapshot();
    });

    test('function is called on press', () => {
        const mockFunc = jest.fn();
        const props = {
            icon: 'arrow-left', onPress: mockFunc,
        };
        const wrapper = shallow(<IconButton {...props} />);

        wrapper.simulate('Press');
        expect(mockFunc).toHaveBeenCalled();
    });
});
