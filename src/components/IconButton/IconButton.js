import React from 'react';
import PropTypes from 'prop-types';
import { TouchableOpacity, Text } from 'react-native';
import Icon from 'react-native-vector-icons/SimpleLineIcons';

import styles from './styles';

const IconButton = (props) => {
    const { icon, onPress, iconstyle } = props;

    return (
        <TouchableOpacity style={styles.button} onPress={onPress}>
            <Icon name={icon} style={iconstyle} />
        </TouchableOpacity>
    );
};

IconButton.propTypes = {
    icon: PropTypes.string,
    onPress: PropTypes.func,
    iconstyle: Text.propTypes.style,
};

IconButton.defaultProps = {
    icon: 'arrow-left',
    onPress: () => {},
    iconstyle: styles.icon,
};

export default IconButton;
