import { StyleSheet, Platform } from 'react-native';

import { colors, fonts, fontSizes } from '../../styles';

export default StyleSheet.create({
    headerContainer: {
        paddingTop: Platform.OS === 'ios' ? 20 : 0,
        height: 60,
        width: '100%',
        flexDirection: 'row',
        backgroundColor: colors.GREY_VERY_LIGHT,
        justifyContent: 'space-between',
    },
    textContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    headerText: {
        color: colors.ORANGE,
        fontFamily: fonts.PRIMARY,
        fontSize: fontSizes.HEADING_3,
    },
    iconContainer: {
        minWidth: 70,
        alignItems: 'center',
        justifyContent: 'center',
    },
});
