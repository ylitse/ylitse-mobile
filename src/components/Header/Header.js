import React from 'react';
import { View, Text } from 'react-native';
import PropTypes from 'prop-types';

import styles from './styles';

const Header = ({ left, right, title }) => (
    <View style={styles.headerContainer}>
        <View style={styles.iconContainer}>
            {left}
        </View>
        <View style={styles.textContainer}>
            <Text style={styles.headerText}>
                {title}
            </Text>
        </View>
        <View style={styles.iconContainer}>
            {right}
        </View>
    </View>
);

Header.propTypes = {
    title: PropTypes.string,
    left: PropTypes.node,
    right: PropTypes.node,
};

Header.defaultProps = {
    title: '',
    left: null,
    right: null,
};

export default Header;
