import React from 'react';
import 'react-native';
import { shallow } from 'enzyme';

import Header from './Header';

describe('Header', () => {
    test('renders correctly', () => {
        const wrapper = shallow(<Header />);

        expect(wrapper).toMatchSnapshot();
    });
});
