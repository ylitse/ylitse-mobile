import PropTypes from 'prop-types';
import RN from 'react-native';
import React from 'react';

import ChatBubble from '../ChatBubble';
import DateBubble from '../DateBubble';
import MessageInput from '../MessageInput';
import HeaderContainer from '../../containers/HeaderContainer';

import { colors } from '../../styles';

const { width } = RN.Dimensions.get('window');

const styles = RN.StyleSheet.create({
    container: {
        flex: 1,
        width,
        backgroundColor: colors.WHITE,
    },
});

const Input = React.memo(({ resetRenderCount }) => (
    <MessageInput onFocus={resetRenderCount} />
));

const ChatConversation = ({
    messages,
    increseRenderCount,
    resetRenderCount,
}) => {
    const behaviorProp = RN.Platform.OS === 'ios'
        ? { behavior: 'padding' }
        : {};
    return (
        <React.Fragment>
            <HeaderContainer />
            <RN.KeyboardAvoidingView
                {...behaviorProp}
                style={styles.container}
            >
                <RN.FlatList
                    bounces={false}
                    inverted
                    onEndReached={increseRenderCount}
                    data={messages}
                    contentContainerStyle={styles.contentContainer}
                    keyExtractor={item => item.id}
                    renderItem={({ item }) => {
                        if (item.dateBubble) {
                            return (
                                <React.Fragment>
                                    <ChatBubble
                                        message={item}
                                    />
                                    <DateBubble
                                        created={item.created}
                                    />
                                </React.Fragment>
                            );
                        }
                        return (
                            <React.Fragment>
                                <ChatBubble
                                    message={item}
                                />
                            </React.Fragment>
                        );
                    }}
                />
                <Input
                    resetRenderCount={resetRenderCount}
                />
            </RN.KeyboardAvoidingView>
        </React.Fragment>
    );
};

ChatConversation.propTypes = {
    increseRenderCount: PropTypes.func.isRequired,
    resetRenderCount: PropTypes.func.isRequired,
    messages: PropTypes.arrayOf(PropTypes.shape({
        created: PropTypes.string,
    })).isRequired,
};

export default ChatConversation;
