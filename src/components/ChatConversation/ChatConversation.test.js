import React from 'react';
import 'react-native';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import renderer from 'react-test-renderer';
import { Provider } from 'react-redux';
import { shallow } from 'enzyme';

import ChatConversation from './ChatConversation';

describe('ChatConversation', () => {
    const mockStore = configureMockStore([thunk]);
    const props = {
        messages: [
            {
                isOurs: false,
                content: 'this is the message',
                date: new Date(1893292),
                id: 'aa',
                opened: false,
            },
            {
                isOurs: true,
                content: 'this is the message 2',
                date: new Date(1893392),
                id: 'bb',
                opened: false,
            },
        ],
    };
    const state = { chats: { recipientId: 'asdfqwerasdf' } };

    test('renders correctly', () => {
        const store = mockStore(state);
        const wrapper = renderer.create(
            <Provider store={store}>
                <ChatConversation {...props} />
            </Provider>,
        );
        expect(wrapper).toMatchSnapshot();
    });

    test('renders correctly without messages', () => {
        const store = mockStore(state);
        const wrapper = shallow(
            <Provider store={store}>
                <ChatConversation messages={[]} />
            </Provider>,
        );

        // expect(wrapper.instance().props.messages).toEqual([]);
        expect(wrapper.find('ChatBubble').length).toEqual(0);
    });
});
