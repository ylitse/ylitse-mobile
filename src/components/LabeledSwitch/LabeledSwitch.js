import React from 'react';
import PropTypes from 'prop-types';
import { View, Switch } from 'react-native';

import { BlockText } from '../Text';

import { colors } from '../../styles';

const outerContainer = {
    flex: 1,
    flexDirection: 'row',
};
const innerContainer = {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
};

const LabeledSwitch = ({ onChange, text, state }) => (
    <View style={outerContainer}>
        <View style={innerContainer}>
            <Switch
                onTintColor={colors.GREEN_PRIMARY}
                tintColor={colors.ORANGE}
                thumbTintColor={colors.GREY_LIGHT}
                value={state}
                onValueChange={onChange}
            />
        </View>
        <View style={{ ...innerContainer, flex: 3 }}>
            <BlockText>
                {text}
            </BlockText>
        </View>
    </View>
);

LabeledSwitch.propTypes = {
    onChange: PropTypes.func.isRequired,
    text: PropTypes.string.isRequired,
    state: PropTypes.bool.isRequired,
};

export default LabeledSwitch;
