import {
    camelify,
    snakify,
    updateKeys,
} from './utils';

test('camels, snakes and keyupdates', () => {
    const snaky = {
        some_key1: 'val1',
        other_key2: 'val2',
    };
    const camely = {
        someKey1: 'val1',
        otherKey2: 'val2',
    };

    expect(updateKeys(snaky, camelify)).toEqual(camely);
    expect(updateKeys(camely, snakify)).toEqual(snaky);
});
