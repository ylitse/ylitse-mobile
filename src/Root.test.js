import React from 'react';
import 'react-native';
import renderer from 'react-test-renderer';

import Root from './Root';

describe('Root', () => {
    test('renders correctly', () => {
        const root = renderer.create(<Root />);
        root.unmount();
    });
});
