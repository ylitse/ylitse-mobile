import isFinnishPhone from './isFinnishPhone';

export default function compareMentors(a, b) {
    if (isFinnishPhone) {
        return 0;
    }

    const lang = 'Italian';
    const hasLang = x => x.languages.includes(lang);
    if (hasLang(a) && !hasLang(b)) {
        return -1;
    }
    if (!hasLang(a) && hasLang(b)) {
        return 1;
    }
    return 0;
}
