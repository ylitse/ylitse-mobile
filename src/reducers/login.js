import {
    LOGIN_REQUEST,
    LOGIN_RECEIVE,
    LOGIN_FAIL,
    LOGOUT,

    REFRESH_REQUEST,
    REFRESH_FAIL,
    REFRESH_RECEIVE,

    FCM_TOKEN,
} from '../actions/login';

export const initialState = {
    isLoggedIn: false,
    isFetching: false,
    isRefresing: false,
    accountId: '',
    userId: '',
    mentorId: '',
    accessToken: '',
    refreshToken: '',

    fcmToken: '',
};

export default function login(state = initialState, action) {
    switch (action.type) {
        case LOGIN_RECEIVE:
            return {
                ...state,
                isLoggedIn: true,
                isFetching: false,
                ...action.scopes,
                ...action.tokens,
            };

        case LOGIN_REQUEST:
            return {
                ...state,
                isLoggedIn: false,
                isFetching: true,
            };

        case LOGIN_FAIL:
            return {
                ...state,
                isFetching: false,
            };

        case REFRESH_REQUEST:
            return {
                ...state,
                isRefresing: true,
            };
        case REFRESH_FAIL:
            return {
                ...state,
                isRefresing: false,
            };
        case REFRESH_RECEIVE:
            return {
                ...state,
                isRefresing: false,
                accessToken: action.accessToken,
            };

        case FCM_TOKEN:
            return {
                ...state,
                fcmToken: action.fcmToken,
            };

        case LOGOUT:
            return {
                ...state,

                isLoggedIn: false,
                isFetching: false,

                accountId: '',
                userId: '',
                mentorId: '',

                accessToken: '',
                refreshToken: '',
            };

        default:
            return state;
    }
}
