import 'react-native';
import chats, { initialState } from './chats';
import {
    MESSAGES_REQUEST,
    MESSAGES_FAIL,

    OPEN_REQUEST,
    OPEN_RECEIVE,
    OPEN_FAIL,

    SEND_REQUEST,
    SEND_RECEIVE,
    SEND_FAIL,
} from '../actions/chats';

describe('Register reducers', () => {
    test('returns initial state', () => {
        expect(chats(undefined, {})).toEqual(initialState);
    });

    test('handles MESSAGES_REQUEST', () => {
        const state = {};
        const action = { type: MESSAGES_REQUEST };

        expect(chats(state, action)).toEqual({ isFetching: true });
    });

    /*
    test('handles MESSAGES_RECEIVE', () => {
        const chatsData = { asdf: [{ id: 'qwer' }] };

        const initial = {
            isFetching: true,
            isSending: false,
            isOpening: false,
            conversations: {},
        };

        const expectedState = {
            isFetching: false,
            isSending: false,
            isOpening: false,
            conversations: chatsData,
        };

        expect(chats(initial, {
            type: MESSAGES_RECEIVE,
            conversations: chatsData,
        })).toEqual(expectedState);
    });
    */

    test('handles MESSAGES_FAIL', () => {
        const state = {};
        const action = { type: MESSAGES_FAIL };
        expect(chats(state, action)).toEqual({ isFetching: false });
    });

    test('handles SEND_REQUEST', () => {
        const state = {};
        const action = { type: SEND_REQUEST };

        expect(chats(state, action)).toEqual({ isSending: true });
    });

    test('handles SEND_RECEIVE', () => {
        const state = { isSending: true };
        const action = { type: SEND_RECEIVE };

        expect(chats(state, action)).toEqual({ isSending: false });
    });

    test('handles SEND_FAIL', () => {
        const state = { isSending: true };
        const action = { type: SEND_FAIL };

        expect(chats(state, action)).toEqual({ isSending: false });
    });

    test('handles OPEN_REQUEST', () => {
        const state = {};
        const action = { type: OPEN_REQUEST };

        expect(chats(state, action)).toEqual({ isOpening: true });
    });

    test('handles OPEN_RECEIVE', () => {
        const state = { isOpening: true };
        const action = { type: OPEN_RECEIVE };

        expect(chats(state, action)).toEqual({ isOpening: false });
    });

    test('handles OPEN_FAIL', () => {
        const state = { isOpening: true };
        const action = { type: OPEN_FAIL };

        expect(chats(state, action)).toEqual({ isOpening: false });
    });
});
