import 'react-native';
import register, { initialState } from './register';
import { ActionTypes } from '../actions/register';

describe('Register reducers', () => {
    test('returns initial state', () => {
        expect(register(undefined, {})).toEqual(initialState);
    });

    test('handles REGISTER_REQUEST', () => {
        expect(register(
            { isRegistering: false },
            { type: ActionTypes.REGISTER_REQUEST },
        )).toEqual({ isRegistering: true });
    });

    test('handles REGISTER_RECEIVE', () => {
        expect(register(
            { isRegistering: true },
            { type: ActionTypes.REGISTER_RECEIVE },
        )).toEqual({ isRegistering: false });
    });

    test('handles REGISTER_FAIL', () => {
        expect(register(
            { isRegistering: true },
            { type: ActionTypes.REGISTER_FAIL },
        )).toEqual({ isRegistering: false });
    });

    test('handles REGISTER_UPDATE', () => {
        const fields = {
            loginName: 'janteri',
            password: 'secret',
            displayName: 'esko',
            region: 'kerava',
        };
        const initial = {
            newUser: {
                loginName: '',
                password: '',
                displayName: '',
                region: '',
            },
        };
        const expected = { newUser: { ...fields } };

        expect(register(
            initial,
            {
                type: ActionTypes.REGISTER_UPDATE,
                updatedFields: fields,
            },
        )).toEqual(expected);
    });
});
