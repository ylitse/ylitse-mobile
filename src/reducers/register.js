import { ActionTypes } from '../actions/register';

export const initialState = {
    newUser: {
        loginName: '',
        password: '',
        displayName: '',
        email: '',
        optIn: false,
    },
    isRegistering: false,
    query: false,
    availability: {},
};

export default function register(state = initialState, action) {
    switch (action.type) {
        case ActionTypes.REGISTER_UPDATE:
            return {
                ...state,
                newUser: {
                    ...state.newUser,
                    ...action.updatedFields,
                },
            };

        case ActionTypes.REGISTER_REQUEST:
            return {
                ...state,
                isRegistering: true,
            };

        case ActionTypes.REGISTER_RECEIVE:
            return initialState;

        case ActionTypes.REGISTER_FAIL:
            return {
                ...state,
                isRegistering: false,
            };

        case ActionTypes.QUERY_REQUEST:
            return {
                ...state,
                query: action.loginName,
            };

        case ActionTypes.QUERY_FAIL:
            return {
                ...state,
                query: false,
            };

        case ActionTypes.QUERY_RECEIVE:
            return {
                ...state,
                query: false,
                availability: {
                    ...state.availability,
                    ...action.availability,
                },
            };

        default:
            return state;
    }
}
