import 'react-native';
import error from './error';
import { ActionTypes } from '../actions/error';

describe('Error reducers', () => {
    test('returns initial state', () => {
        expect(error(undefined, {})).toEqual('');
    });

    test('handles ERROR_SET_MESSAGE', () => {
        const action = {
            type: ActionTypes.ERROR_SET_MESSAGE,
            errorMessage: ':(',
        };

        expect(error(undefined, action)).toEqual(':(');
    });

    test('handles ERROR_RESET_MESSAGE', () => {
        const action = {
            type: ActionTypes.ERROR_RESET_MESSAGE,
        };

        expect(error(undefined, action)).toEqual('');
    });
});
