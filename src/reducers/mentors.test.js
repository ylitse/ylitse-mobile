import 'react-native';
import mentors, { initialState } from './mentors';
import {
    MENTORS_REQUEST,
    MENTORS_RECEIVE,
    MENTORS_FAIL,
} from '../actions/mentors';

describe('Mentor reducers', () => {
    test('returns initial state', () => {
        expect(mentors(undefined, {})).toEqual(initialState);
    });

    test('handles MENTORS_REQUEST', () => {
        expect(mentors({}, { type: MENTORS_REQUEST }))
            .toEqual({ isFetching: true });
    });

    test('handles MENTORS_FAILED', () => {
        expect(mentors({}, { type: MENTORS_FAIL }))
            .toEqual({ isFetching: false });
    });

    test('handles MENTORS_RECEIVE', () => {
        const ms = { sd32f: { id: 'sd32f', displayName: 'janteri' } };

        const initial = {
            isFetching: true,
            mentorObject: {},
            mentorBuddies: {},
        };
        const expectedState = {
            isFetching: false,
            mentorObject: ms,
            mentorBuddies: ms,
        };

        expect(mentors(initial, {
            type: MENTORS_RECEIVE,
            mentorObject: ms,
            mentorBuddies: ms,
        })).toEqual(expectedState);
    });
});
