import {
    BUDDY_REQUEST,
    BUDDY_FAIL,
    BUDDY_RECEIVE,

    MESSAGES_REQUEST,
    MESSAGES_RECEIVE,
    MESSAGES_FAIL,

    OPEN_REQUEST,
    OPEN_RECEIVE,
    OPEN_FAIL,

    RESET_CHATS,

    RECIPIENT_ID,

    SEND_REQUEST,
    SEND_RECEIVE,
    SEND_FAIL,

    DELETE_REQUEST,
    DELETE_RECEIVE,
    DELETE_FAIL,

    SET_MESSAGE,
} from '../actions/chats';

export const initialState = {
    isFetching: false,
    isSending: false,
    isOpening: false,
    isDeleting: false,

    buddies: {},
    unseen: {},
    messages: {},

    sortedIds: [],

    recipientId: '',

    messageDrafts: {},
};

export function handleRecipientId(state, action) {
    const { recipientId } = action;
    return {
        ...state,
        recipientId,
    };
}

export function handleResetChats() {
    return initialState;
}
export function handleBuddyRequest(state, action) {
    const { buddies } = state;
    const { buddy } = action;
    return { ...state, buddies: { ...buddies, ...buddy } };
}
export function handleBuddyReceive(state, action) {
    const { buddies } = state;
    const { buddy } = action;
    return { ...state, buddies: { ...buddies, ...buddy } };
}

export function handleBuddyFail(state, action) {
    const { buddies } = state;
    const { failedBuddyId: failId = '' } = action;
    return {
        ...state,
        buddies: Object.keys(buddies).reduce(
            (acc, id) => {
                if (id === failId) {
                    return acc;
                }
                const { [id]: someBuddy } = buddies;
                return { ...acc, [id]: someBuddy };
            },
            {},
        ),
    };
}
export function handleMessageRequest(state) {
    return {
        ...state,
        isFetching: true,
    };
}
export function handleMessageReceive(state, action) {
    const {
        messages: messages = {},
        sortedIds: sortedIds = [],
        unseen: unseen = {},
    } = action;
    return {
        ...state,
        messages,
        sortedIds,
        unseen,
        isFetching: false,
    };
}

export function handleMessageFail(state) {
    return {
        ...state,
        isFetching: false,
    };
}
export function handleOpenRequest(state) {
    return {
        ...state,
        isOpening: true,
    };
}
export function handleOpenReceive(state) {
    return {
        ...state,
        isOpening: false,
    };
}

export function handleOpenFail(state) {
    return {
        ...state,
        isOpening: false,
    };
}

export function handleSendRequest(state) {
    return {
        ...state,
        isSending: true,
    };
}
export function handleSendReceive(state) {
    return {
        ...state,
        isSending: false,
    };
}
export function handleSendFail(state) {
    return {
        ...state,
        isSending: false,
    };
}

export function handleDeleteRequest(state) {
    return {
        ...state,
        isDeleting: true,
    };
}
export function handleDeleteReceive(state) {
    return {
        ...state,
        isDeleting: false,
    };
}
export function handleDeleteFail(state) {
    return {
        ...state,
        isDeleting: false,
    };
}

export default function chats(state = initialState, action) {
    const { type } = action;
    switch (type) {
        case BUDDY_REQUEST:
            return handleBuddyRequest(state, action);
        case BUDDY_RECEIVE:
            return handleBuddyReceive(state, action);
        case BUDDY_FAIL:
            return handleBuddyFail(state, action);

        case MESSAGES_REQUEST:
            return handleMessageRequest(state, action);
        case MESSAGES_RECEIVE:
            return handleMessageReceive(state, action);
        case MESSAGES_FAIL:
            return handleMessageFail(state, action);

        case OPEN_REQUEST:
            return handleOpenRequest(state, action);
        case OPEN_RECEIVE:
            return handleOpenReceive(state, action);
        case OPEN_FAIL:
            return handleOpenFail(state, action);

        case RECIPIENT_ID:
            return handleRecipientId(state, action);

        case RESET_CHATS:
            return handleResetChats(state, action);

        case SEND_REQUEST:
            return handleSendRequest(state, action);
        case SEND_RECEIVE:
            return handleSendReceive(state, action);
        case SEND_FAIL:
            return handleSendFail(state, action);

        case DELETE_REQUEST:
            return handleDeleteRequest(state, action);
        case DELETE_RECEIVE:
            return handleDeleteReceive(state, action);
        case DELETE_FAIL:
            return handleDeleteFail(state, action);

        case SET_MESSAGE:
            return {
                ...state,
                messageDrafts: {
                    ...state.messageDrafts,
                    [action.payload.recipientId]: action.payload.content,
                },
            };

        default:
            return state;
    }
}
