import {
    MENTORS_REQUEST,
    MENTORS_RECEIVE,
    MENTORS_FAIL,
    SELECT_MENTOR,
} from '../actions/mentors';

export const initialState = {
    isFetching: false,
    mentorObject: {},
    mentorBuddies: {},
    selectedMentor: undefined,
};

export default function mentors(state = initialState, action) {
    const {
        type,
        mentorObject,
        mentorBuddies,
        selectedMentor,
    } = action;
    switch (type) {
        case SELECT_MENTOR:
            return {
                ...state,
                selectedMentor,
            };
        case MENTORS_REQUEST:
            return {
                ...state,
                isFetching: true,
            };

        case MENTORS_RECEIVE:
            return {
                ...state,
                mentorObject,
                mentorBuddies,
                isFetching: false,
            };

        case MENTORS_FAIL:
            return {
                ...state,
                isFetching: false,
            };

        default:
            return state;
    }
}
