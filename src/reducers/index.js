import { combineReducers } from 'redux';
import navigation from './navigation';
import register from './register';
import errorMessage from './error';
import mentors from './mentors';
import login from './login';
import chats from './chats';
import user from './user';
import account from './account';

const rootReducer = combineReducers({
    errorMessage,
    mentors,
    register,
    chats,
    login,
    user,
    account,
    navigation,
});

export default rootReducer;
