import {
    NAVIGATE,
    BACK,
    CLEAR_HISTORY,
} from '../actions/navigation';

export const initialState = {
    history: [],
    currentScreen: {
        screen: 'MentorList',
        headerVisible: false,
        title: '',
        left: '',
        right: '',
    },
    dropdown: false,
};

export function isEqual(objectA, objectB) {
    return Object.keys(objectA).reduce(
        (acc, key) => {
            const { [key]: valueA } = objectA;
            const { [key]: valueB } = objectB;
            return acc && (valueA === valueB);
        },
        true,
    );
}

export function handleClearHistory(state) {
    return {
        ...state,
        history: [],
    };
}

export function handleNavigate(state, action) {
    const { currentScreen, history } = state;
    const { nextScreen = '' } = action;
    if (isEqual(currentScreen, nextScreen)) return state;
    return {
        ...state,
        history: [currentScreen, ...history],
        currentScreen: {
            headerVisible: false,
            title: '',
            left: '',
            right: '',
            ...nextScreen,
        },
    };
}

export function handleBack(state) {
    const { currentScreen, history } = state;
    const [prevScreen = currentScreen, ...rest] = history;
    return {
        ...state,
        history: rest,
        currentScreen: prevScreen,
    };
}

export default function navigation(state = initialState, action) {
    const { type } = action;
    switch (type) {
        case (NAVIGATE):
            return handleNavigate(state, action);

        case (BACK):
            return handleBack(state, action);

        case (CLEAR_HISTORY):
            return handleClearHistory(state, action);

        default:
            return state;
    }
}
