import { ActionTypes } from '../actions/account';

export const initialState = {
    isFetching: false,
    account: {},
};

export default function account(state = initialState, action) {
    switch (action.type) {
        case ActionTypes.ACCOUNT_REQUEST:
            return {
                ...state,
                isFetching: true,
            };

        case ActionTypes.ACCOUNT_RECEIVE:
            return {
                ...state,
                isFetching: false,
                account: action.account,
            };

        case ActionTypes.ACCOUNT_FAIL:
            return {
                ...state,
                isFetching: false,
            };

        case ActionTypes.UPDATE_PASSWORD_REQUEST:
            return {
                ...state,
                isChangingPassword: true,
            };

        case ActionTypes.UPDATE_PASSWORD_RECEIVE:
            return {
                ...state,
                isChangingPassword: false,
            };

        case ActionTypes.UPDATE_PASSWORD_FAIL:
            return {
                ...state,
                isChangingPassword: false,
            };

        default:
            return state;
    }
}
