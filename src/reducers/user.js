import { ActionTypes } from '../actions/user';

export const initialState = {
    isFetching: false,
    isPutting: false,
    user: {},
};

export default function user(state = initialState, action) {
    switch (action.type) {
        case ActionTypes.USER_REQUEST:
            return {
                ...state,
                isFetching: true,
            };

        case ActionTypes.USER_RECEIVE:
            return {
                user: action.user,
                isFetching: false,
            };

        case ActionTypes.USER_FAIL:
            return {
                ...state,
                isPutting: false,
            };

        case ActionTypes.PUT_REQUEST:
            return {
                ...state,
                isPutting: true,
            };

        case ActionTypes.PUT_RECEIVE:
            return {
                user: action.user,
                isPutting: false,
            };

        case ActionTypes.PUT_FAIL:
            return {
                ...state,
                isPutting: false,
            };

        default:
            return state;
    }
}
