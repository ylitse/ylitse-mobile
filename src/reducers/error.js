import { ActionTypes } from '../actions/error';

export default function errorMessage(state = '', action) {
    switch (action.type) {
        case ActionTypes.ERROR_SET_MESSAGE:
            return action.errorMessage;

        case ActionTypes.ERROR_RESET_MESSAGE:
            return '';

        default:
            return state;
    }
}
