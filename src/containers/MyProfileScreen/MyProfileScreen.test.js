import React from 'react';
import 'react-native';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { shallow } from 'enzyme';

import MyProfileScreen from './MyProfileScreen';

describe('MyProfileScreen', () => {
    const mockStore = configureMockStore([thunk]);
    const mockFunc = jest.fn();

    const props = {
        logout: mockFunc,
    };

    test('renders mentee correctly', () => {
        const state = {
            login: {
                userId: 'asdf',
            },
            user: {
                user: {
                    isFetching: false,
                    user: {},
                    displayName: 'janteri',
                },
            },
            account: {
                account: {
                    role: 'mentee',
                    loginName: 'janteri',
                },
            },
            mentors: {
                mentorMap: {},
            },
        };
        const store = mockStore(state);
        const wrapper = shallow(
            <MyProfileScreen
                {...props}
                store={store}
            />,
        );

        expect(wrapper).toMatchSnapshot();
    });

    test('logout function is called on press', () => {
        const state = {
            login: {
                userId: 'asdf',
            },
            user: {
                user: {
                    isFetching: false,
                    user: {},
                    displayName: 'janteri',
                },
            },
            account: {
                account: {
                    role: 'mentee',
                    loginName: 'janteri',
                },
            },
            mentors: {
                mentorMap: {},
            },
        };
        const store = mockStore(state);
        const wrapper = shallow(<MyProfileScreen
            {...props}
            store={store}
        />);

        wrapper.dive().find('MyMenteeProfile').dive()
            .find('Button')
            .at(0)
            .dive()
            .find('TouchableOpacity')
            .simulate('Press');

        expect(wrapper).toMatchSnapshot();
    });
});
