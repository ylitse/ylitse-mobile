import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import MyMenteeProfile from '../../components/MyMenteeProfile';
import MyMentorProfile from '../../components/MyMentorProfile';
import { logout as logoutAction } from '../../actions/login';
import {
    getAccount as getAccountAction,
    changePassword as changePasswordAction,
    deleteAccount as deleteAccountAction,
} from '../../actions/account';
import { getUser as getUserAction } from '../../actions/user';

const mapStateToProps = state => (
    {
        account: state.account.account,
        user: state.user.user,
        mentorId: state.login.mentorId,
        mentorObject: state.mentors.mentorObject,
    }
);

const mapDispatchToProps = dispatch => (
    {
        getAccount: () => {
            dispatch(getAccountAction());
        },
        getUser: () => {
            dispatch(getUserAction());
        },
        logout: () => {
            dispatch(logoutAction());
        },
        changePassword: (currentPassword, newPassword) => {
            dispatch(changePasswordAction(currentPassword, newPassword));
        },
        deleteAccount: () => { dispatch(deleteAccountAction()); },
    }
);

export class MyProfile extends React.Component {
    componentDidMount() {
        const { getAccount, getUser } = this.props;
        getUser();
        getAccount();
    }

    render() {
        const {
            mentorId,
            mentorObject,
            account,
            user,
            logout,
            changePassword,
            deleteAccount,
        } = this.props;
        if (mentorId) {
            const mentor = mentorObject[mentorId];
            if (!mentor) {
                throw Error;
            }
            return (
                <MyMentorProfile
                    account={account}
                    user={user}
                    mentor={mentor}
                    logout={logout}
                    changePassword={changePassword}
                    deleteAccount={deleteAccount}
                />
            );
        }
        return (
            <MyMenteeProfile
                account={account}
                user={user}
                logout={logout}
                changePassword={changePassword}
                deleteAccount={deleteAccount}
            />
        );
    }
}

MyProfile.propTypes = {
    mentorObject: PropTypes.objectOf(PropTypes.shape({
        displayName: PropTypes.string.isRequired,
        birthYear: PropTypes.number.isRequired,
        region: PropTypes.string.isRequired,
        story: PropTypes.string.isRequired,
        skills: PropTypes.arrayOf(PropTypes.string),
        languages: PropTypes.arrayOf(PropTypes.string),
    })).isRequired,
    mentorId: PropTypes.string.isRequired,
    logout: PropTypes.func.isRequired,
    deleteAccount: PropTypes.func.isRequired,
    changePassword: PropTypes.func.isRequired,
    getUser: PropTypes.func.isRequired,
    getAccount: PropTypes.func.isRequired,
    account: PropTypes.shape({
        loginName: PropTypes.string,
    }).isRequired,
    user: PropTypes.shape({
        displayName: PropTypes.string,
    }).isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(MyProfile);
