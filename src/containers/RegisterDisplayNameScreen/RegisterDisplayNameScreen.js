import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { updateRegister } from '../../actions/register';
import { back, navigate } from '../../actions/navigation';

import RegisterFormDisplayName from '../../components/RegisterFormDisplayName';

const mapStateToProps = state => (
    {
        loginName: state.register.newUser.loginName,
    }
);

const mapDispatchToProps = dispatch => (
    {
        handleSubmit: (args) => {
            dispatch(updateRegister(args));
            dispatch(navigate('RegisterEmail'));
        },
        goBack: () => dispatch(back()),
    }
);

export const RegisterDisplayNameScreen = ({
    goBack,
    handleSubmit,
    loginName,
}) => (
    <RegisterFormDisplayName
        goBack={goBack}
        handleSubmit={handleSubmit}
        loginName={loginName}
    />
);

RegisterDisplayNameScreen.propTypes = {
    goBack: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    loginName: PropTypes.string.isRequired,
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(RegisterDisplayNameScreen);
