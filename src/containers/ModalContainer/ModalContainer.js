import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import Modal from '../../components/Modal';
import { resetErrorMessage } from '../../actions/error';

const mapStateToProps = state => (
    {
        message: state.errorMessage,
        isVisible: Boolean(state.errorMessage),
    }
);

const mapDispatchToProps = dispatch => (
    { closeModal: () => dispatch(resetErrorMessage()) }
);

export const BaseModal = props => (
    props.isVisible && (
        <Modal
            message={props.message}
            closeModal={props.closeModal}
            type={props.type}
        />
    )
);

BaseModal.propTypes = {
    isVisible: PropTypes.bool.isRequired,
    message: PropTypes.string.isRequired,
    closeModal: PropTypes.func.isRequired,
    type: PropTypes.string.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(BaseModal);
