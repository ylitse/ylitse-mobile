import React from 'react';
import 'react-native';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { shallow } from 'enzyme';

import ModalContainer from './ModalContainer';

describe('ModalContainer', () => {
    const mockStore = configureMockStore([thunk]);

    test('renders correctly', () => {
        const initialState = {
            errorMessage: ':(',
        };
        const store = mockStore(initialState);
        const wrapper = shallow(<ModalContainer type="ok" store={store} />);
        wrapper.dive().props().closeModal();

        expect(wrapper).toMatchSnapshot();
    });
});
