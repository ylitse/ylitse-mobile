import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import LoggedinMentorList from '../../components/LoggedinMentorList';
import MentorList from '../../components/MentorList';
import { getMentors, selectMentor } from '../../actions/mentors';
import { navigate } from '../../actions/navigation';

const mapStateToProps = state => (
    {
        mentorObject: state.mentors.mentorObject,
        isLoggedIn: state.login.isLoggedIn,
        mentorId: state.login.mentorId,
    }
);

const mapDispatchToProps = dispatch => (
    {
        fetchData: () => dispatch(getMentors()),
        handleSubmit: () => dispatch(navigate('Register')),
        handleCardPress: (mentor) => {
            dispatch(selectMentor(mentor));
            dispatch(navigate('MentorProfile'));
        },
    }
);

export class Mentors extends React.Component {
    constructor(props) {
        super(props);
        const { fetchData } = this.props;
        this.state = {
            timer: setInterval(() => fetchData(), 2000),
        };
    }

    componentDidMount() {
        const { fetchData } = this.props;
        fetchData();
    }

    componentWillUnmount() {
        const { timer } = this.state;
        clearInterval(timer);
    }

    render() {
        const {
            isLoggedIn,
            handleCardPress,
            handleSubmit,
            mentorObject,
            mentorId,
        } = this.props;
        if (isLoggedIn) {
            return (
                <LoggedinMentorList
                    handleCardPress={handleCardPress}
                    mentorObject={mentorObject}
                    mentorId={mentorId}
                />
            );
        }
        return (
            <MentorList
                handleSubmit={handleSubmit}
                mentorObject={mentorObject}
            />
        );
    }
}

Mentors.propTypes = {
    fetchData: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    handleCardPress: PropTypes.func.isRequired,
    mentorId: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.oneOf([null]),
    ]).isRequired,
    mentorObject: PropTypes.objectOf(PropTypes.shape({
        id: PropTypes.string.isRequired,
        displayName: PropTypes.string.isRequired,
        birthYear: PropTypes.number.isRequired,
        region: PropTypes.string.isRequired,
        story: PropTypes.string.isRequired,
        skills: PropTypes.arrayOf(PropTypes.string),
        languages: PropTypes.arrayOf(PropTypes.string),
    })).isRequired,
    isLoggedIn: PropTypes.bool.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(Mentors);
