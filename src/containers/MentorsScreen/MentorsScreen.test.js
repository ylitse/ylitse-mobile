import React from 'react';
import 'react-native';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { shallow } from 'enzyme';

import MentorsScreen from './MentorsScreen';

describe('Mentors', () => {
    jest.useFakeTimers();
    const mockStore = configureMockStore([thunk]);
    const janteri = {
        id: '0',
        displayName: 'J. Anteri',
        region: 'Kontula',
        story: 'This is a story',
        languages: ['Finnish', 'Swedish', 'English', 'Russian'],
        skills: ['Coffeemaking', 'Vim'],
        birthYear: 2000,
    };
    const loggedOutState = {
        mentors: {
            mentorObject: { 0: janteri },
        },
        login: { isLoggedIn: false },
        user: {
            currentUser: {
                id: '1',
                role: 'admin',
            },
        },
    };
    const isLoggedInState = {
        mentors: { mentorObject: { 0: janteri } },
        login: { isLoggedIn: true },
        user: {
            currentUser: {
                id: '1',
                role: 'admin',
            },
        },
    };
    const loggedOutStore = mockStore(loggedOutState);
    const isLoggedInStore = mockStore(isLoggedInState);

    test('renders correctly', () => {
        const wrapper = shallow(<MentorsScreen store={loggedOutStore} />);

        expect(wrapper).toMatchSnapshot();
        wrapper.dive().instance();
        jest.advanceTimersByTime(3000);
        wrapper.dive().instance().componentWillUnmount();
    });

    test('card press function is called on press', () => {
        const wrapper = shallow(<MentorsScreen store={isLoggedInStore} />);
        wrapper.dive();
        wrapper.props().handleCardPress();
        expect(wrapper).toMatchSnapshot();
    });

    test('submit function is called on press', () => {
        const wrapper = shallow(<MentorsScreen store={loggedOutStore} />);

        wrapper.dive()
            .find('MentorList').dive()
            .find('Button')
            .simulate('Press');
        expect(wrapper).toMatchSnapshot();
    });
});
