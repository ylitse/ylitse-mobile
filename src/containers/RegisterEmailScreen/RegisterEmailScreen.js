import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { updateRegister } from '../../actions/register';
import { back, navigate } from '../../actions/navigation';

import RegisterFormEmail from '../../components/RegisterFormEmail';

const mapDispatchToProps = dispatch => (
    {
        handleSubmit: (args) => {
            dispatch(updateRegister(args));
            dispatch(navigate('RegisterPrivacy'));
        },
        skipRegister: () => {
            dispatch(updateRegister({ email: '' }));
            dispatch(navigate('RegisterPrivacy'));
        },
        goBack: () => dispatch(back()),
    }
);

export const RegisterEmailScreen = ({ goBack, handleSubmit, skipRegister }) => (
    <RegisterFormEmail
        handleSubmit={handleSubmit}
        skipRegister={skipRegister}
        goBack={goBack}
    />
);

RegisterEmailScreen.propTypes = {
    goBack: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    skipRegister: PropTypes.func.isRequired,
};

export default connect(null, mapDispatchToProps)(RegisterEmailScreen);
