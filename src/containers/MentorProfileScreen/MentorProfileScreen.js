import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import MentorCardExpanded from '../../components/MentorCardExpanded';
import { navigate } from '../../actions/navigation';
import { setRecipientId } from '../../actions/chats';

export const MentorProfile = ({ selectedMentor, navigateToChat }) => {
    const { userId } = selectedMentor;
    const goToChat = () => navigateToChat(userId);
    return (
        <MentorCardExpanded
            mentor={selectedMentor}
            goToChat={goToChat}
        />
    );
};

MentorProfile.propTypes = {
    selectedMentor: PropTypes.shape({
        displayName: PropTypes.string.isRequired,
        birthYear: PropTypes.number.isRequired,
        region: PropTypes.string.isRequired,
        story: PropTypes.string.isRequired,
        skills: PropTypes.arrayOf(PropTypes.string),
        languages: PropTypes.arrayOf(PropTypes.string),
    }).isRequired,
    navigateToChat: PropTypes.func.isRequired,
};

export default connect(
    ({ mentors: { selectedMentor } }) => ({ selectedMentor }),
    dispatch => ({
        navigateToChat: (id) => {
            dispatch(setRecipientId(id));
            dispatch(navigate('Chatting'));
        },
    }),
)(MentorProfile);
