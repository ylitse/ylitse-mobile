import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import ChatList from '../../components/ChatList';
import {
    getMessages as getMessagesAction,
    getBuddies as getBuddiesAction,
    setRecipientId as setRecipientIdAction,
    deleteConversation as deleteConversationAction,
} from '../../actions/chats';
import { navigate as navigateAction } from '../../actions/navigation';

const mapStateToProps = (state) => {
    const { chats: { buddies, unseen, sortedIds } } = state;
    return {
        buddies,
        unseen,
        sortedIds,
    };
};

const mapDispatchToProps = dispatch => (
    {
        getMessages: () => dispatch(getMessagesAction()),
        getBuddies: () => dispatch(getBuddiesAction()),
        navigateToChat: (id) => {
            dispatch(setRecipientIdAction(id));
            dispatch(navigateAction('Chatting'));
        },
        deleteConversation: id => dispatch(deleteConversationAction(id)),
    }
);

export class Chats extends React.Component {
    constructor(props) {
        super(props);
        const { getBuddies, getMessages } = this.props;
        this.state = {
            timerChats: setInterval(() => getMessages(), 1000),
            timerContacts: setInterval(() => getBuddies(), 1000),
        };
    }

    componentDidMount() {
        const { getBuddies, getMessages } = this.props;
        getMessages();
        getBuddies();
    }

    componentWillUnmount() {
        const { timerChats, timerContacts } = this.state;
        clearInterval(timerChats);
        clearInterval(timerContacts);
    }

    render() {
        const {
            buddies,
            unseen,
            sortedIds,
            navigateToChat,
            deleteConversation,
        } = this.props;
        return (
            <ChatList
                goToConversation={navigateToChat}
                buddies={buddies}
                unseen={unseen}
                sortedIds={sortedIds}
                deleteConversation={deleteConversation}
            />
        );
    }
}

Chats.propTypes = {
    deleteConversation: PropTypes.func.isRequired,
    navigateToChat: PropTypes.func.isRequired,
    getBuddies: PropTypes.func.isRequired,
    getMessages: PropTypes.func.isRequired,
    unseen: PropTypes.objectOf(PropTypes.string).isRequired,
    sortedIds: PropTypes.arrayOf(PropTypes.string).isRequired,
    buddies: PropTypes.objectOf(PropTypes.shape({
        displayName: PropTypes.string.isRequired,
    })).isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(Chats);
