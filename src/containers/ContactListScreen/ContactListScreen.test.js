import React from 'react';
import 'react-native';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { shallow } from 'enzyme';

import ContactListScreen from './ContactListScreen';

describe('ContactListScreen', () => {
    jest.useFakeTimers();
    const mockStore = configureMockStore([thunk]);
    const initialState = {
        chats: {
            conversations: {
                asdf: {
                    messages: [
                        {
                            id: 'poiu',
                            isOurs: false,
                            sender_id: 'asdf',
                            recipient_id: 'qwer',
                            date: new Date(2147483647),
                            opened: true,
                            content: 'Message 1 content goes here.',
                        },
                    ],
                },
            },
        },
        contacts: [
            { asdf: { displayName: 'Janteri' } },
        ],
        login: {
            userId: {
                id: 'qwer',
            },
        },
    };
    const props = {
        navigation: {
            navigate: jest.fn(),
        },
    };

    test('renders correctly', () => {
        const store = mockStore(initialState);
        const wrapper = shallow(
            <ContactListScreen store={store} {...props} />,
        );
        expect(wrapper).toMatchSnapshot();
        wrapper.dive().instance();
        jest.advanceTimersByTime(3000);
        wrapper.dive().instance().componentWillUnmount();
    });

    test('unmount correctly', () => {
        const store = mockStore(initialState);
        const wrapper = shallow(<ContactListScreen
            {...props}
            store={store}
        />);

        wrapper.instance().componentWillUnmount();
        expect(wrapper).toMatchSnapshot();
    });
});
