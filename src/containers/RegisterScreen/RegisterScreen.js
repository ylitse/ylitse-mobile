import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { initialSubmit } from '../../actions/register';
import { setErrorMessage } from '../../actions/error';
import { navigate, back } from '../../actions/navigation';

import RegisterForm from '../../components/RegisterForm';

const mapStateToProps = state => (
    { nav: state.nav }
);

const mapDispatchToProps = dispatch => (
    {
        handleSubmit: (args) => {
            dispatch(initialSubmit(args));
        },

        handleCheck: (errorMsg) => {
            dispatch(setErrorMessage(errorMsg));
        },

        loginHere: () => {
            dispatch(navigate('Login'));
        },

        goBack: () => {
            dispatch(back());
        },
    }
);

const RegisterScreen = ({
    handleSubmit,
    handleCheck,
    loginHere,
    goBack,
}) => (
    <RegisterForm
        handleSubmit={handleSubmit}
        handleCheck={handleCheck}
        loginHere={loginHere}
        goBack={goBack}
    />
);

RegisterScreen.propTypes = {
    goBack: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    handleCheck: PropTypes.func.isRequired,
    loginHere: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(RegisterScreen);
