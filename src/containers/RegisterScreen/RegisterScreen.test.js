import React from 'react';
import 'react-native';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { shallow } from 'enzyme';

import RegisterScreen from './RegisterScreen';

describe('RegisterScreen', () => {
    const mockStore = configureMockStore([thunk]);

    test('renders correctly', () => {
        const store = mockStore();
        const wrapper = shallow(<RegisterScreen store={store} />);
        wrapper.dive().props().handleSubmit();
        wrapper.dive().props().handleCheck();
        wrapper.dive().props().loginHere();

        expect(wrapper).toMatchSnapshot();
    });
});
