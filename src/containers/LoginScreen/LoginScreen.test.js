import React from 'react';
import 'react-native';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { shallow } from 'enzyme';

import LoginScreen from './LoginScreen';

const mockStore = configureMockStore([thunk]);
describe('LoginScreen', () => {
    test('renders correctly', async () => {
        const store = mockStore({
            login: { isFetching: false },
        });
        const wrapper = shallow(<LoginScreen store={store} />);
        wrapper.dive().props().handleSubmit('user', 'pass');
        wrapper.dive().props().handleCheck();

        expect(wrapper).toMatchSnapshot();
    });
});
