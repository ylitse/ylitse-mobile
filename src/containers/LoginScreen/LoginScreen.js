import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { login } from '../../actions/login';
import { back } from '../../actions/navigation';

import { getUser } from '../../actions/user';
import { getAccount } from '../../actions/account';

import LoginForm from '../../components/LoginForm';

const mapStateToProps = state => (
    { nav: state.nav }
);

const mapDispatchToProps = dispatch => (
    {
        handleSubmit: (user, pass) => dispatch(
            login(
                user,
                pass,
                async () => {
                    await dispatch(getAccount());
                    await dispatch(getUser());
                },
            ),
        ),
        goBack: () => dispatch(back()),
        handleCheck: () => {},
    }
);

class Login extends React.Component {
    // componentDidMount() {
    //     this.props.handleSubmit('janteri', 'janteri');
    // }
    render() {
        const { handleSubmit, handleCheck, goBack } = this.props;
        return (
            <LoginForm
                handleSubmit={handleSubmit}
                handleCheck={handleCheck}
                goBack={goBack}
            />
        );
    }
}

Login.propTypes = {
    goBack: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    handleCheck: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
