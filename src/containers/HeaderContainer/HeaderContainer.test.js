describe('HeaderContainer', () => {
    test('renders correctly', () => {});
});
/*
import React from 'react';
import 'react-native';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { shallow } from 'enzyme';

import HeaderContainer from './HeaderContainer';

describe('HeaderContainer', () => {
    const mockStore = configureMockStore([thunk]);
    jest.useFakeTimers();
    const mockFunc = jest.fn();
    const navigation = {
        navigate: mockFunc,
        state: {
            index: 0,
            routes: [{
                key: 'id-1',
                routeName: 'LoggedinMentorsScreen',
                params: {
                    user: {
                        displayName: 'Janteri',
                    },
                },
            }],

        },
    };

    test('renders correctly', () => {
        const store = mockStore();
        const props = {
            title: 'Hipsun hapsun',
            left: '',
            right: '',
            navigation,
        };
        const wrapper = shallow(<HeaderContainer {...props} store={store} />);
        expect(wrapper).toMatchSnapshot();
    });

    test('can show different buttons', () => {
        const store = mockStore();
        const props1 = {
            title: 'A', left: 'back', right: 'login', navigation,
        };
        const props2 = {
            title: 'B', left: 'menu', right: 'profile', navigation,
        };
        const wrapper1 = shallow(<HeaderContainer {...props1} store={store} />);
        const wrapper2 = shallow(<HeaderContainer
            {...props2}
            store={store}
        />);

        const wrapper1Buttons = wrapper1.dive().find('Header').dive().dive()
            .find('IconButton');
        wrapper1Buttons.at(0).simulate('Press');
        wrapper1Buttons.at(1).simulate('Press');

        const wrapper2Buttons = wrapper2.dive().find('Header').dive().dive()
            .find('IconButton');
        wrapper2Buttons.at(0).simulate('Press');
        wrapper2Buttons.at(1).simulate('Press');

        expect(wrapper1).toMatchSnapshot();
        expect(wrapper2).toMatchSnapshot();
    });

    test('dropdown buttons work', () => {
        const store = mockStore();
        const props = {
            title: 'Hipsun hapsun',
            left: 'menu',
            right: '',
            navigation,
        };
        const wrapper = shallow(<HeaderContainer {...props} store={store} />);
        wrapper.dive().instance().toggleDropdown();

        const dropdownButtons = wrapper.dive()
            .find('HeaderDropdown').dive().find('TouchableOpacity');
        dropdownButtons.at(0).simulate('Press');
        dropdownButtons.at(1).simulate('Press');

        wrapper.dive().find('TouchableWithoutFeedback').props().onPressIn();
    });

    test('android back listeners get added and removed', () => {
        const store = mockStore();
        const props = {
            title: 'Hipsun hapsun',
            left: 'menu',
            right: '',
            navigation,
        };
        const wrapper = shallow(<HeaderContainer {...props} store={store} />);

        const headerInstance = wrapper.dive().instance();
        headerInstance.componentDidUpdate();
        headerInstance.toggleDropdown();
        headerInstance.componentDidUpdate();
    });

    test('title get set correctly', () => {
        const store = mockStore();
        const wrapper = shallow(
            <HeaderContainer
                navigation={navigation}
                store={store}
            />,
        );

        expect(wrapper.dive().find('Header').props().title).toEqual('Janteri');
    });
});
*/
