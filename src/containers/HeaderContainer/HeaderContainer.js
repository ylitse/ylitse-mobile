import React from 'react';
import {
    Animated,
    View,
    TouchableWithoutFeedback,
    BackHandler,
    SafeAreaView,
} from 'react-native';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import I18n from '../../i18n';

import Header from '../../components/Header';
import HeaderDropdown from '../../components/HeaderDropdown';
import IconButton from '../../components/IconButton';

import { navigate, back } from '../../actions/navigation';

class HeaderContainer extends React.Component {
    state = {
        anim: new Animated.Value(0),
        dropdownVisible: false,
    }

    entries = [
        {
            routeName: 'ChatListScreen',
            icon: 'bubbles',
            text: I18n.t('headerDropdownConversations'),
            current: false,
            callback: () => {
                const { navigateTo } = this.props;
                this.toggleDropdown(0);
                navigateTo('ContactList');
            },
        },
        {
            routeName: 'LoggedinMentorsScreen',
            icon: 'notebook',
            text: I18n.t('headerDropdownMentorList'),
            current: false,
            callback: () => {
                const { navigateTo } = this.props;
                this.toggleDropdown(0);
                navigateTo('LoggedInMentorList');
            },
        },
    ];

    componentDidUpdate() {
        const { dropdownVisible } = this.state;
        if (dropdownVisible) {
            BackHandler
                .addEventListener('hardwareBackPress', this.toggleDropdown);
        } else {
            BackHandler
                .removeEventListener('hardwareBackPress', this.toggleDropdown);
        }
    }

    getEntries = () => {
        const { screen } = this.props;
        return this.entries.map(
            (entry) => {
                const { routeName } = entry;
                const isCurrent = screen === routeName;
                return {
                    ...entry,
                    current: isCurrent,
                };
            },
        );
    }

    toggleDropdown = (duration) => {
        const { anim, dropdownVisible } = this.state;
        Animated.timing(
            anim,
            {
                toValue: Number(!dropdownVisible),
                duration,
            },
        ).start();
        this.setState(prevState => (
            { dropdownVisible: !prevState.dropdownVisible }
        ));
        return true;
    };

    closeDropdown = () => {
        const { anim } = this.state;
        Animated.timing(
            anim,
            {
                toValue: 0,
                duration: 0,
            },
        ).start();
        this.setState({ dropdownVisible: false });
        return true;
    }

    buttonSelector = (type) => {
        const { goBack, navigateTo } = this.props;
        switch (type) {
            case 'back':
                return (
                    <IconButton
                        icon="arrow-left"
                        onPress={goBack}
                    />
                );

            case 'mentors':
                return (
                    <IconButton
                        icon="notebook"
                        onPress={() => navigateTo('LoggedInMentorList')}
                    />
                );

            case 'chat':
                return (
                    <IconButton
                        icon="bubbles"
                        onPress={() => navigateTo('ContactList')}
                    />
                );

            case 'menu':
                return (
                    <IconButton
                        icon="menu"
                        onPress={() => this.toggleDropdown(300)}
                    />
                );

            case 'profile':
                return (
                    <IconButton
                        icon="user"
                        onPress={() => {
                            this.closeDropdown();
                            navigateTo('MyProfile');
                        }}
                    />
                );

            default:
                return null;
        }
    };

    render() {
        const {
            left,
            right,
            title,
        } = this.props;
        const { anim } = this.state;
        return (
            <SafeAreaView style={{ zIndex: 999999 }}>
                <Animated.View style={{
                    backgroundColor: 'black',
                    opacity: anim.interpolate({
                        inputRange: [0, 1],
                        outputRange: [0, 0.3],
                    }),
                    top: 60,
                    height: anim.interpolate({
                        inputRange: [0, 1],
                        outputRange: [-1, 10000],
                    }),
                    width: '100%',
                    position: 'absolute',
                }}
                >
                    <TouchableWithoutFeedback
                        onPressIn={() => this.toggleDropdown(300)}
                    >
                        <View style={{
                            height: '100%',
                            width: '100%',
                        }}
                        />
                    </TouchableWithoutFeedback>
                </Animated.View>
                <Animated.View style={[
                    { position: 'absolute', width: '100%' },
                    {
                        top: anim.interpolate({
                            inputRange: [0, 1],
                            outputRange: [-200, 60],
                        }),
                    },
                ]}
                >
                    <HeaderDropdown entries={this.getEntries()} />
                </Animated.View>
                <Header
                    title={title}
                    left={this.buttonSelector(left)}
                    right={this.buttonSelector(right)}
                />
            </SafeAreaView>
        );
    }
}

HeaderContainer.propTypes = {
    goBack: PropTypes.func.isRequired,
    navigateTo: PropTypes.func.isRequired,
    left: PropTypes.string.isRequired,
    right: PropTypes.string.isRequired,
    screen: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
};

export default connect(
    ({
        navigation: {
            currentScreen: {
                headerVisible,
                left,
                right,
                screen,
                title,
            },
        },
    }) => ({
        headerVisible,
        left,
        right,
        screen,
        title,
    }),
    dispatch => ({
        goBack: () => dispatch(back()),
        navigateTo: screen => dispatch(navigate(screen)),
    }),
)(HeaderContainer);
