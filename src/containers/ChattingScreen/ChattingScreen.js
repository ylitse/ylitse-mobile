import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import ChatConversation from '../../components/ChatConversation';
import {
    getMessages as getMessagesAction,
    openMessage as openMessageAction,
} from '../../actions/chats';

const rcInc = 10;

export class Conversation extends React.Component {
    constructor(props) {
        super(props);
        const { getMessages } = this.props;
        this.state = {
            timer: setInterval(() => getMessages(), 1000),
            renderCount: rcInc,
        };
    }

    componentDidMount() {
        const {
            recipientId,
            open,
        } = this.props;
        open(recipientId);
    }

    componentWillUnmount() {
        const { timer } = this.state;
        clearInterval(timer);

        const { recipientId, open } = this.props;
        open(recipientId);
    }

    render() {
        const { messages, recipientId } = this.props;
        const { [recipientId]: conversation = [] } = messages;
        const { renderCount } = this.state;
        const renderedMessages = conversation.slice(0, renderCount);
        return (
            <ChatConversation
                messages={renderedMessages}
                increseRenderCount={
                    () => this.setState((s) => {
                        const { renderCount: rc } = s;
                        return { renderCount: rc + rcInc };
                    })
                }
                resetRenderCount={
                    () => this.setState({ renderCount: rcInc })
                }
            />
        );
    }
}

Conversation.propTypes = {
    getMessages: PropTypes.func.isRequired,
    open: PropTypes.func.isRequired,
    recipientId: PropTypes.string.isRequired,
    messages: PropTypes.objectOf(PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.string.isRequired,
        opened: PropTypes.bool.isRequired,
        isOurs: PropTypes.bool.isRequired,
        content: PropTypes.string.isRequired,
        created: PropTypes.string.isRequired,
    }))).isRequired,
};

export default connect(
    ({ chats: { messages, recipientId } }) => ({ messages, recipientId }),
    dispatch => ({
        getMessages: () => dispatch(getMessagesAction()),
        open: id => dispatch(openMessageAction(id)),
    }),
)(Conversation);
