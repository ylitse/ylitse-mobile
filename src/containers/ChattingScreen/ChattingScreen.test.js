import React from 'react';
import 'react-native';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { shallow } from 'enzyme';

import ChattingScreen from './ChattingScreen';

describe('ChattingScreen', () => {
    jest.useFakeTimers();
    const mockStore = configureMockStore([thunk]);

    const props = {
        recipient: '',
        navigation: {
            navigate: jest.fn(),
            state: {
                params: {
                    user: {
                        id: '100',
                    },
                },
            },
        },
    };

    const initialState = {
        login: {
            userId: 'qwer',
        },
        chats: {
            messages: {
                zxcv: [
                    {
                        recipient_id: 'qwer',
                        opened: true,
                    },
                ],
            },
        },
    };

    test('renders correctly', () => {
        const store = mockStore(initialState);
        const wrapper = shallow(
            <ChattingScreen store={store} {...props} />,
        );
        expect(wrapper).toMatchSnapshot();
        wrapper.dive().instance();
        jest.advanceTimersByTime(3000);
        wrapper.dive().instance().componentWillUnmount();
    });

    test('renders correctly without conversations', () => {
        const store = mockStore(
            {
                ...initialState,
                chats: { messages: {} },
            },
        );

        const wrapper = shallow(
            <ChattingScreen store={store} {...props} />,
        );

        wrapper.dive();

        expect(wrapper).toMatchSnapshot();
    });

    test('goToConversation is called', () => {
        const store = mockStore({
            ...initialState,
        });
        const wrapper = shallow(
            <ChattingScreen store={store} {...props} />,
        );
        wrapper.dive();

        expect(wrapper).toMatchSnapshot();
    });
});
