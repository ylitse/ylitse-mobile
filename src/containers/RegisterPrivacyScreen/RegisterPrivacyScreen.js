import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { register as registerAction } from '../../actions/register';
import { back } from '../../actions/navigation';

import RegisterFormPrivacy from '../../components/RegisterFormPrivacy';

const mapDispatchToProps = dispatch => (
    {
        goBack: () => dispatch(back()),
        register: () => dispatch(registerAction()),
    }
);

export const RegisterPrivacyScreen = ({
    goBack,
    register,
}) => (
    <RegisterFormPrivacy
        register={register}
        goBack={goBack}
    />
);

RegisterPrivacyScreen.propTypes = {
    goBack: PropTypes.func.isRequired,
    register: PropTypes.func.isRequired,
};

export default connect(null, mapDispatchToProps)(RegisterPrivacyScreen);
