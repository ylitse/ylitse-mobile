export function updateKeys(obj, func) {
    return Object.keys(obj).reduce(
        (updated, key) => ({ ...updated, [func(key)]: obj[key] }),
        {},
    );
}

export function camelify(str) {
    return str.replace(/_(\w|$)/g, (_, ch) => ch.toUpperCase());
}

export function snakify(str) {
    return str.replace(/([a-z\d])([A-Z])/g, '$1_$2').toLowerCase();
}

export async function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}
