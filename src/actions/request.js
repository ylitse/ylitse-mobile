import API_URL from '../../config';
import { updateKeys, camelify, snakify } from '../utils';

async function defaultFetch(
    endpoint,
    method,
    body,
    headers,
) {
    return fetch(API_URL + endpoint, { method, body, headers });
}
/* eslint-disable */
export class Request {
/* eslint-enable */
    constructor(fetchFunc = defaultFetch) {
        this.fetch = fetchFunc;
    }

    async handleRequest(...args) {
        const resp = await this.fetch(...args);
        const contentType = resp.headers.get('Content-Type');
        const isJson = contentType && contentType.includes('application/json');
        if (!resp.ok || resp.status >= 400) {
            if (isJson) {
                const json = await resp.json();
                throw new Error(json.message || resp.statusText);
            }

            throw new Error(resp.statusText);
        }
        return isJson ? resp.json() : null;
    }

    GET(
        endpoint,
        requestAction,
        failureAction,
        receiveAction,
    ) {
        return async (dispatch) => {
            await dispatch(requestAction());

            try {
                const data = await this.handleRequest(
                    endpoint,
                    'GET',
                );

                if (data && 'resources' in data) {
                    await dispatch(receiveAction(
                        data.resources.map(r => updateKeys(r, camelify)),
                    ));
                } else {
                    await dispatch(receiveAction(updateKeys(data, camelify)));
                }
            } catch (error) {
                await dispatch(failureAction());
            }
        };
    }

    write(
        endpoint,
        object,
        requestAction, failureAction, receiveAction,
        method,
    ) {
        return async (dispatch) => {
            await dispatch(requestAction());
            try {
                const headers = { 'Content-Type': 'application/json' };
                const body = JSON.stringify(updateKeys(object, snakify));
                const data = await this.handleRequest(
                    endpoint,
                    method,
                    body,
                    headers,
                );
                await dispatch(receiveAction(updateKeys(data, camelify)));
            } catch (error) {
                await dispatch(failureAction());
            }
        };
    }

    POST(...args) {
        return this.write(...args, 'POST');
    }

    PUT(...args) {
        return this.write(...args, 'PUT');
    }

    DELETE(
        endpoint,
        requestAction, failureAction, receiveAction,
    ) {
        return async (dispatch) => {
            dispatch(requestAction());

            try {
                await this.handleRequest(endpoint, 'DELETE');
                await dispatch(receiveAction());
            } catch (error) {
                await dispatch(failureAction());
            }
        };
    }
}

// export default { Fetcher, defaultFetch };
