import 'react-native';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import fetchMock from 'fetch-mock';
import API_URL from '../../config';
import I18n from '../i18n';
import {
    ActionTypes,
    getAccount,
    postAccount,
    putAccount,
    changePassword,
    deleteAccount,
} from './account';

const mockStore = configureMockStore([thunk]);

describe('Test getAccount', () => {
    afterEach(fetchMock.restore);
    test('test get account', async () => {
        const accountId = 'asdfqer';
        fetchMock.getOnce(
            `${API_URL}/accounts/${accountId}`,
            {
                body: {
                    id: accountId,
                    login_name: 'janteri',
                },
                headers: { 'Content-Type': 'application/json' },
            },
        );
        const store = mockStore({
            login: {
                accountId,
            },
            account: {
                account: {},
                isFetching: false,
            },
        });
        const expectedActions = [
            { type: ActionTypes.ACCOUNT_REQUEST },
            {
                type: ActionTypes.ACCOUNT_RECEIVE,
                account: {
                    id: accountId,
                    loginName: 'janteri',
                },
            },
        ];
        await store.dispatch(getAccount());
        await expect(store.getActions()).toEqual(expectedActions);
    });
    test('test already fetching account', async () => {
        const accountId = 'asdfqer';
        const store = mockStore({
            login: {
                accountId,
            },
            account: {
                account: {},
                isFetching: true,
            },
        });
        const expectedActions = [];
        await store.dispatch(getAccount());
        await expect(store.getActions()).toEqual(expectedActions);
    });
});

describe('Test postAccount', () => {
    afterEach(fetchMock.restore);
    test('test post account', async () => {
        fetchMock.postOnce(
            `${API_URL}/accounts`,
            {
                body: {
                    id: 'asdf',
                    login_name: 'janteri',
                },
                headers: { 'Content-Type': 'application/json' },
            },
        );
        const store = mockStore({
            account: {
                account: {},
                isFetching: false,
            },
        });
        const expectedActions = [
            { type: ActionTypes.ACCOUNT_REQUEST },
            {
                type: ActionTypes.ACCOUNT_RECEIVE,
                account: {
                    id: 'asdf',
                    loginName: 'janteri',
                },
            },
        ];
        await store.dispatch(postAccount({ loginName: 'janteri' }));
        await expect(store.getActions()).toEqual(expectedActions);
    });
    test('already fetching', async () => {
        const store = mockStore({
            account: {
                account: {},
                isFetching: true,
            },
        });
        const expectedActions = [];
        await store.dispatch(postAccount({ loginName: 'janteri' }));
        await expect(store.getActions()).toEqual(expectedActions);
    });
});

describe('Test putAccount', () => {
    afterEach(fetchMock.restore);
    test('test put account', async () => {
        const accountId = 'asdf';
        fetchMock.putOnce(
            `${API_URL}/accounts/${accountId}`,
            {
                body: {
                    id: accountId,
                    login_name: 'janteri',
                    email: 'asdf@asdf.com',
                },
                headers: { 'Content-Type': 'application/json' },
            },
        );
        const store = mockStore({
            login: { accountId },
            account: {
                accountId,
                account: { accountId },
                isFetching: false,
            },
        });
        const expectedActions = [
            { type: ActionTypes.ACCOUNT_REQUEST },
            {
                type: ActionTypes.ACCOUNT_RECEIVE,
                account: {
                    id: accountId,
                    loginName: 'janteri',
                    email: 'asdf@asdf.com',
                },
            },
        ];
        await store.dispatch(putAccount({ loginName: 'janteri' }));
        await expect(store.getActions()).toEqual(expectedActions);
    });
    test('already fetching', async () => {
        const store = mockStore({
            login: { accountId: 'asdf' },
            account: {
                account: {},
                isFetching: true,
            },
        });
        const expectedActions = [];
        await store.dispatch(putAccount({ loginName: 'janteri' }));
        await expect(store.getActions()).toEqual(expectedActions);
    });
});

describe('Test deleteAccount', () => {
    afterEach(fetchMock.restore);
    test('test delete account', async () => {
        const accountId = 'asdf';
        fetchMock.deleteOnce(
            `${API_URL}/accounts/${accountId}`,
            {
                body: {
                    id: accountId,
                    login_name: 'janteri',
                    email: 'asdf@asdf.com',
                },
                headers: { 'Content-Type': 'application/json' },
            },
        );
        const store = mockStore({
            login: { accountId },
            account: {
                accountId,
                account: { accountId },
                isFetching: false,
            },
        });
        const expectedActions = [
            { type: ActionTypes.ACCOUNT_REQUEST },
            {
                type: ActionTypes.ACCOUNT_RECEIVE,
                account: {},
            },
            { type: 'LOGOUT' },
            {
                type: 'NAVIGATE',
                nextScreen: {
                    screen: 'MentorList',
                    headerVisible: false,
                },
            },
            { type: 'CLEAR_HISTORY' },
        ];
        await store.dispatch(deleteAccount());
        await expect(store.getActions()).toEqual(expectedActions);
    });
    test('already fetching', async () => {
        const store = mockStore({
            login: { accountId: 'asdf' },
            account: {
                account: {},
                isFetching: true,
            },
        });
        const expectedActions = [];
        await store.dispatch(deleteAccount());
        await expect(store.getActions()).toEqual(expectedActions);
    });
});

describe('Test changePassword', () => {
    afterEach(fetchMock.restore);
    test('test change password', async () => {
        const accountId = 'asdf';
        fetchMock.putOnce(
            `${API_URL}/accounts/${accountId}/password`,
            {
                message: 'password changed',
                headers: { 'Content-Type': 'application/json' },
            },
        );
        const store = mockStore({
            login: { accountId },
            account: {
                accountId,
                account: { accountId },
                isFetching: false,
            },
        });
        const expectedActions = [
            { type: ActionTypes.UPDATE_PASSWORD_REQUEST },
            {
                errorMessage: I18n.t('passwordUpdateOk'),
                type: 'ERROR_SET_MESSAGE',
            },
            { type: ActionTypes.UPDATE_PASSWORD_RECEIVE },
        ];
        await store.dispatch(changePassword('liirum', 'laarum'));
        await expect(store.getActions()).toEqual(expectedActions);
    });
    test('already fetching', async () => {
        const store = mockStore({
            login: { accountId: 'asdf' },
            account: {
                account: {},
                isFetching: true,
            },
        });
        const expectedActions = [];
        await store.dispatch(deleteAccount());
        await expect(store.getActions()).toEqual(expectedActions);
    });
});
