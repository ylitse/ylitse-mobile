import API_URL from '../../config';

import I18n from '../i18n';
import { snakify, updateKeys } from '../utils';

import { setErrorMessage } from './error';
import { login, loggedInRequest } from './login';
import { Request } from './request';
import { navigate } from './navigation';

export const ActionTypes = {
    REGISTER_UPDATE: 'REGISTER_UPDATE',

    REGISTER_REQUEST: 'REGISTER_REQUEST',
    REGISTER_RECEIVE: 'REGISTER_RECEIVE',
    REGISTER_FAIL: 'REGISTER_FAIL',

    QUERY_REQUEST: 'QUERY_REQUEST',
    QUERY_FAIL: 'QUERY_FAIL',
    QUERY_RECEIVE: 'QUERY_RECEIVE',
};

export function queryLoginName(loginName) {
    return async (dispatch, getState) => {
        const { register: { query } } = getState();
        if (query) {
            return;
        }
        await dispatch({ type: ActionTypes.QUERY_REQUEST, loginName });
        let resp;
        try {
            resp = await fetch(
                `${API_URL}/search?login_name=${loginName}`,
                { method: 'HEAD' },
            );
        } catch (e) {
            await dispatch(setErrorMessage(I18n.t('registerFetchFail')));
            await dispatch({ type: ActionTypes.QUERY_FAIL });
            return;
        }
        switch (resp.status) {
            case 200:
                await dispatch({
                    type: ActionTypes.QUERY_RECEIVE,
                    availability: { [loginName]: false },
                });
                return;

            case 204:
                await dispatch({
                    type: ActionTypes.QUERY_RECEIVE,
                    availability: { [loginName]: true },
                });
                return;

            default:
                await dispatch(setErrorMessage(I18n.t('registerFetchFail')));
                await dispatch({ type: ActionTypes.QUERY_FAIL });
        }
    };
}

export function updateRegister(fields) {
    return {
        type: ActionTypes.REGISTER_UPDATE,
        updatedFields: {
            ...fields,
        },
    };
}

export function initialSubmit(fields) {
    return async (dispatch, getState) => {
        const { loginName } = fields;
        await dispatch(queryLoginName(loginName));
        const {
            register: {
                availability: {
                    [loginName]: isAvailable = false,
                },
            },
        } = getState();
        if (isAvailable) {
            await dispatch(updateRegister(fields));
            await dispatch(navigate('RegisterDisplayName'));
        } else {
            await dispatch(setErrorMessage(I18n.t('loginNameTaken')));
        }
    };
}

const onFailureRegister = () => (dispatch) => {
    dispatch({
        type: ActionTypes.REGISTER_FAIL,
    });
    dispatch(setErrorMessage(I18n.t('registerFetchFail')));
    dispatch(navigate('MentorList'));
};

export const register = () => async (dispatch, getState) => {
    const {
        register: {
            isRegistering,
            newUser: {
                password, displayName, email, loginName, optIn,
            },
        },
    } = getState();

    if (isRegistering) {
        return;
    }

    const request = new Request();
    await dispatch(request.POST(
        '/accounts',
        {
            account: updateKeys(
                {
                    role: 'mentee',
                    loginName,
                    email,
                    optIn,
                }, snakify,
            ),
            password,
        },
        () => ({ type: ActionTypes.REGISTER_REQUEST }),
        onFailureRegister,
        created => async () => {
            await dispatch(login(loginName, password, async () => {
                const newRequest = loggedInRequest(dispatch, getState);
                await dispatch(newRequest.PUT(
                    `/users/${created.user.id}`,
                    {
                        ...created.user,
                        displayName,
                    },
                    () => ({ type: ActionTypes.REGISTER_REQUEST }),
                    onFailureRegister,
                    () => ({ type: ActionTypes.REGISTER_RECEIVE }),
                ));
            }));
        },
    ));
};
