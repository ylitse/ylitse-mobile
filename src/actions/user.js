import { setErrorMessage } from './error';
import I18n from '../i18n';
import { loggedInRequest } from './login';

export const ActionTypes = {
    USER_REQUEST: 'USER_REQUEST',
    USER_RECEIVE: 'USER_RECEIVE',
    USER_FAIL: 'USER_FAIL',

    PUT_REQUEST: 'PUT_REQUEST',
    PUT_RECEIVE: 'PUT_RECEIVE',
    PUT_FAIL: 'PUT_FAIL',
};

function onFailure() {
    return (dispatch) => {
        dispatch({
            type: ActionTypes.USER_FAIL,
        });

        dispatch(setErrorMessage(I18n.t('userFetchFail')));
    };
}

export function getUser() {
    return async (dispatch, getState) => {
        const { user: { isFetching }, login: { userId } } = getState();
        if (isFetching) {
            return null;
        }
        const request = loggedInRequest(dispatch, getState);
        return dispatch(request.GET(
            `/users/${userId}`,
            () => ({ type: ActionTypes.USER_REQUEST }),
            onFailure,
            user => ({
                type: ActionTypes.USER_RECEIVE,
                user,
            }),
        ));
    };
}

function onFailurePutUser() {
    return (dispatch) => {
        dispatch({
            type: ActionTypes.PUT_FAIL,
        });

        dispatch(setErrorMessage(I18n.t('userPutFail')));
    };
}

export function putUser(updatedUser) {
    return async (dispatch, getState) => {
        const {
            user: { user, isPutting },
            login: { userId },
        } = getState();
        if (isPutting) {
            return null;
        }
        const request = loggedInRequest(dispatch, getState);
        return dispatch(request.PUT(
            `/users/${userId}`,
            { ...user, ...updatedUser },
            () => ({ type: ActionTypes.PUT_REQUEST }),
            onFailurePutUser,
            json => ({
                type: ActionTypes.PUT_RECEIVE,
                user: json,
            }),
        ));
    };
}
