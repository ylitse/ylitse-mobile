import { Platform } from 'react-native';
import firebase from 'react-native-firebase';

import { navigate, clearHistory } from './navigation';
import API_URL from '../../config';
import I18n from '../i18n';
import { camelify, updateKeys, sleep } from '../utils';
import { setErrorMessage } from './error';
import { Request } from './request';
import { storeData, retrieveData, removeData } from './persistence';

export const LOGIN_REQUEST = 'LOGIN_REQUEST';
export const LOGIN_RECEIVE = 'LOGIN_RECEIVE';
export const LOGIN_FAIL = 'LOGIN_FAIL';

export const LOGOUT = 'LOGOUT';

export const REFRESH_REQUEST = 'REFRESH_REQUEST';
export const REFRESH_FAIL = 'REFRESH_FAIL';
export const REFRESH_RECEIVE = 'REFRESH_RECEIVE';

export const FCM_TOKEN = 'FCM_TOKEN';

export async function backOff(predicate, milliseconds, maxTime) {
    await sleep(milliseconds);
    if (predicate()) return true;
    if (milliseconds * 2 > maxTime) return false;
    return backOff(predicate, 2 * milliseconds, maxTime);
}

export async function refresh(dispatch, getState) {
    if (getState().login.isRefreshing) {
        const backOffSuccess = await backOff(
            () => !getState().login.isRefreshing,
            10,
            500,
        );
        if (backOffSuccess) {
            return;
        }
        throw Error;
    }
    const request = new Request();
    await dispatch(request.POST(
        '/refresh',
        { refreshToken: getState().login.refreshToken },
        () => ({ type: REFRESH_REQUEST }),
        () => ({ type: REFRESH_FAIL }),
        json => ({
            type: REFRESH_RECEIVE,
            ...json,
        }),
    ));
}

export function loggedInFetch(dispatch, getState) {
    return async (endpoint, method, body, headers, retryOnFail = true) => {
        const { login: { accessToken } } = getState();
        let resp = await fetch(
            API_URL + endpoint,
            {
                method,
                body,
                headers: { ...headers, Authorization: `Bearer ${accessToken}` },
            },
        );
        if (resp.status === 401 && retryOnFail) {
            await refresh(dispatch, getState);
            const newFetch = loggedInFetch(dispatch, getState);
            resp = await newFetch(endpoint, method, body, headers, false);
        }
        return resp;
    };
}

export function loggedInRequest(dispatch, getState) {
    if (dispatch === undefined || getState === undefined) {
        throw Error;
    }
    return new Request(loggedInFetch(dispatch, getState));
}

export const checkFcmToken = () => async (dispatch, getState) => {
    const fcmToken = await firebase.messaging().getToken();
    const request = loggedInRequest(dispatch, getState);
    const { login: { userId } } = getState();
    await dispatch(request.PUT(
        `/users/${userId}/device`,
        { token: fcmToken, platform: Platform.OS },
        () => ({ type: null }),
        () => ({ type: null }),
        json => ({
            type: FCM_TOKEN,
            token: json.token,
        }),
    ));
};

function onLoginFailure() {
    return (dispatch) => {
        dispatch({
            type: LOGIN_FAIL,
        });
        dispatch(setErrorMessage(I18n.t('modalLoginFailed')));
    };
}

export function setLoggedInState(loginData) {
    return async (dispatch) => {
        await dispatch({
            type: LOGIN_RECEIVE,
            ...loginData,
        });

        await dispatch(navigate('LoggedInMentorList'));
        const hasBuddies = await retrieveData('hasBuddies');
        if (hasBuddies) {
            await dispatch(navigate('ContactList'));
        }
        await dispatch(clearHistory());
        await dispatch(checkFcmToken());
    };
}

export function tryRestoreLogin() {
    return async (dispatch) => {
        const loginData = await retrieveData('LOGIN');
        if (loginData) {
            await dispatch(setLoggedInState(loginData));
        }
    };
}

export function handleLogin(json, callback) {
    return async (dispatch) => {
        let { tokens, scopes } = json;
        [tokens, scopes] = [tokens, scopes].map(x => updateKeys(x, camelify));
        const loginData = { tokens, scopes };
        await storeData('LOGIN', loginData);
        await dispatch(setLoggedInState(loginData));
        if (callback) {
            await callback();
        }
    };
}

export function login(loginName, password, callback) {
    return async (dispatch, getState) => {
        if (getState().login.isFetching) {
            return;
        }
        const request = new Request();
        await dispatch(request.POST(
            '/login',
            { login_name: loginName, password },
            () => ({ type: LOGIN_REQUEST }),
            onLoginFailure,
            json => handleLogin(json, callback),
        ));
    };
}

export function logout() {
    return async (dispatch) => {
        dispatch({ type: LOGOUT });
        await removeData('LOGIN');
        await removeData('hasBuddies');
        await dispatch(navigate('MentorList'));
        await dispatch(clearHistory());
    };
}
