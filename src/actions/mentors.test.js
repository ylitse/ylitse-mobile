import 'react-native';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import fetchMock from 'fetch-mock';
import API_URL from '../../config';
import I18n from '../i18n';

import {
    getMentors,
    MENTORS_FAIL,
    MENTORS_RECEIVE,
    MENTORS_REQUEST,
} from './mentors';

const mockStore = configureMockStore([thunk]);
describe('Mentor actions', () => {
    afterEach(fetchMock.restore);

    test('fetch is stopped if loading', async () => {
        const store = mockStore({
            mentors: {
                isFetching: true,
                mentorObject: {},
            },
        });
        const expectedActions = [];

        await store.dispatch(getMentors());
        await expect(store.getActions()).toEqual(expectedActions);
    });

    test('Works', async () => {
        fetchMock.getOnce(
            `${API_URL}/mentors`,
            {
                body: {
                    resources: [
                        { userId: 'asdf', id: 'asdf', a: 'qwer' },
                    ],
                },
                headers: { 'Content-Type': 'application/json' },
            },
        );
        const expectedActions = [
            { type: MENTORS_REQUEST },
            {
                type: MENTORS_RECEIVE,
                mentorObject: {
                    asdf: { userId: 'asdf', id: 'asdf', a: 'qwer' },
                },
                mentorBuddies: {
                    asdf: { userId: 'asdf', id: 'asdf', a: 'qwer' },
                },
            },
        ];
        const store = mockStore({ mentors: { isFetching: false } });
        await store.dispatch(getMentors());
        await expect(store.getActions()).toEqual(expectedActions);
    });

    test('Fails', async () => {
        fetchMock.getOnce(
            `${API_URL}/mentors`,
            {
                status: 400,
            },
        );
        const expectedActions = [
            { type: MENTORS_REQUEST },
            { type: MENTORS_FAIL },
            {
                type: 'ERROR_SET_MESSAGE',
                errorMessage: I18n.t('mentorFetchFail'),
            },
        ];
        const store = mockStore({ mentors: { isFetching: false } });
        await store.dispatch(getMentors());
        await expect(store.getActions()).toEqual(expectedActions);
    });
});
