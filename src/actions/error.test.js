import 'react-native';

import {
    ActionTypes,
    setErrorMessage,
    resetErrorMessage,
} from './error';

describe('Error actions', () => {
    test('message is set', () => {
        const expectedAction = {
            type: ActionTypes.ERROR_SET_MESSAGE,
            errorMessage: 'oopsie',
        };

        expect(setErrorMessage('oopsie')).toEqual(expectedAction);
    });

    test('message is reset', () => {
        const expectedAction = {
            type: ActionTypes.ERROR_RESET_MESSAGE,
        };

        expect(resetErrorMessage()).toEqual(expectedAction);
    });
});
