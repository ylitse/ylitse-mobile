import 'react-native';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import fetchMock from 'fetch-mock';
import {
    ActionTypes,
    getUser,
    putUser,
} from './user';
import { updateKeys, camelify } from '../utils';
import API_URL from '../../config';
import I18n from '../i18n';

const dummyUser = {
    display_name: 'janteri',
    role: 'mentor',
    account_id: 'Rilz0n8zCxI_AP2XIgR_Aga_fgxgy4N62xRdKybz84I',
    id: 'asdfqwer',
    created: '2018-08-20T07:35:36.636228',
    updated: '2018-08-20T07:35:36.636697',
    active: true,
};

const mockStore = configureMockStore([thunk]);

describe('Get user action', () => {
    afterEach(fetchMock.restore);
    test('fetch is stopped if loading', async () => {
        const store = mockStore({
            user: { isFetching: true },
            login: { userId: dummyUser.id },
        });
        const expectedActions = [];
        await store.dispatch(getUser());
        await expect(store.getActions()).toEqual(expectedActions);
    });

    test('Works', async () => {
        fetchMock.getOnce(
            `${API_URL}/users/${dummyUser.id}`,
            {
                body: dummyUser,
                headers: { 'Content-Type': 'application/json' },
                status: 200,
            },
        );
        const expectedActions = [
            { type: ActionTypes.USER_REQUEST },
            {
                type: ActionTypes.USER_RECEIVE,
                user: updateKeys(dummyUser, camelify),
            },
        ];
        const store = mockStore({
            user: { isFetching: false },
            login: { userId: dummyUser.id },
        });
        await store.dispatch(getUser());
        await expect(store.getActions()).toEqual(expectedActions);
    });

    test('Fails', async () => {
        fetchMock.getOnce(
            `${API_URL}/users/${dummyUser.id}`,
            {
                body: dummyUser,
                headers: { 'Content-Type': 'application/json' },
                status: 400,
            },
        );
        const expectedActions = [
            { type: ActionTypes.USER_REQUEST },
            {
                type: ActionTypes.USER_FAIL,
            },
            {
                errorMessage: I18n.t('userFetchFail'),
                type: 'ERROR_SET_MESSAGE',
            },
        ];
        const store = mockStore({
            user: { isFetching: false },
            login: { userId: dummyUser.id },
        });
        await store.dispatch(getUser());
        await expect(store.getActions()).toEqual(expectedActions);
    });
});

describe('Put user action', () => {
    afterEach(fetchMock.restore);
    test('Putting fetch is stopped if loading', async () => {
        const store = mockStore({
            user: { isPutting: true },
            login: { userId: dummyUser.id },
        });
        const expectedActions = [];
        await store.dispatch(putUser(dummyUser));
        await expect(store.getActions()).toEqual(expectedActions);
    });

    test('Works', async () => {
        fetchMock.putOnce(
            `${API_URL}/users/${dummyUser.id}`,
            {
                body: dummyUser,
                headers: { 'Content-Type': 'application/json' },
                status: 200,
            },
        );
        const expectedActions = [
            { type: ActionTypes.PUT_REQUEST },
            {
                type: ActionTypes.PUT_RECEIVE,
                user: updateKeys(dummyUser, camelify),
            },
        ];
        const store = mockStore({
            user: { isPutting: false },
            login: { userId: dummyUser.id },
        });
        await store.dispatch(putUser(dummyUser));
        await expect(store.getActions()).toEqual(expectedActions);
    });

    test('Fails', async () => {
        fetchMock.putOnce(
            `${API_URL}/users/${dummyUser.id}`,
            {
                body: dummyUser,
                headers: { 'Content-Type': 'application/json' },
                status: 400,
            },
        );
        const expectedActions = [
            { type: ActionTypes.PUT_REQUEST },
            {
                type: ActionTypes.PUT_FAIL,
            },
            {
                errorMessage: I18n.t('userPutFail'),
                type: 'ERROR_SET_MESSAGE',
            },
        ];
        const store = mockStore({
            user: { isPutting: false },
            login: { userId: dummyUser.id },
        });
        await store.dispatch(putUser(dummyUser));
        await expect(store.getActions()).toEqual(expectedActions);
    });
});
