import 'react-native';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import fetchMock from 'fetch-mock';
import API_URL from '../../config';

import {
    ActionTypes,
    updateRegister,
    register,
} from './register';

const mockStore = configureMockStore([thunk]);

const accountFromAPI = {
    role: 'mentee',
    login_name: 'esko',
    id: 'asdfqwer1234',
};

const userFromAPI1 = {
    role: accountFromAPI.role,
    display_name: 'janteri',
    account_id: accountFromAPI.id,
    id: 'asdf',
};

const userFromAPI2 = {
    role: accountFromAPI.role,
    display_name: 'esko',
    account_id: accountFromAPI.id,
    id: userFromAPI1.id,
};

describe('Register actions', () => {
    const user = {
        loginName: 'janteri',
        password: 'secret',
        displayName: 'esko',
        region: 'kerava',
    };
    afterEach(fetchMock.restore);

    test('update is created', () => {
        const update = { new: 'field' };
        const expectedAction = {
            type: ActionTypes.REGISTER_UPDATE,
            updatedFields: update,
        };

        expect(updateRegister(update)).toEqual(expectedAction);
    });

    test('successful register creates account and logs in', async () => {
        fetchMock.postOnce(
            `${API_URL}/accounts`,
            {
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    account: accountFromAPI,
                    user: userFromAPI1,
                }),
                credentials: 'same-origin',
            },
        );
        fetchMock.putOnce(
            `${API_URL}/users/${accountFromAPI.id}`,
            {
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(userFromAPI2),
                credentials: 'same-origin',
            },
        );
        const store = mockStore({
            register: { isRegistering: false, newUser: user },
        });

        await store.dispatch(register());

        fetchMock.postOnce(
            `${API_URL}/login`,
            { status: 200 },
        );
    });
});
