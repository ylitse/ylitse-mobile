import 'react-native';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import fetchMock from 'fetch-mock';

import API_URL from '../../config';

import { Request } from './request';
import { sleep } from '../utils';

const mockStore = configureMockStore([thunk]);

describe('Test Request GET', () => {
    afterEach(fetchMock.restore);

    test('GET a list of things with custom fetchFunc', async () => {
        fetchMock.getOnce(
            `${API_URL}/asdf`,
            {
                body: { resources: [{ foo_fuu: 'asdf' }] },
                headers: { 'Content-Type': 'application/json' },
            },
        );
        const expectedActions = [
            { type: 'A' },
            {
                type: 'B',
                someList: [{ fooFuu: 'asdf' }],
            },
        ];
        const store = mockStore({});

        const customFetch = async (endpoint, method, body, headers) => {
            await sleep(1);
            return fetch(API_URL + endpoint, { method, body, headers });
        };
        const request = new Request(customFetch);
        await store.dispatch(request.GET(
            '/asdf',
            () => ({ type: 'A' }),
            () => ({ type: 'FAIL' }),
            someList => ({ type: 'B', someList }),
        ));

        await expect(store.getActions()).toEqual(expectedActions);
    });

    test('GET a list of things', async () => {
        fetchMock.getOnce(
            `${API_URL}/asdf`,
            {
                body: { resources: [{ foo_fuu: 'asdf' }] },
                headers: { 'Content-Type': 'application/json' },
            },
        );
        const expectedActions = [
            { type: 'A' },
            {
                type: 'B',
                someList: [{ fooFuu: 'asdf' }],
            },
        ];
        const store = mockStore({});

        const request = new Request();
        await store.dispatch(request.GET(
            '/asdf',
            () => ({ type: 'A' }),
            () => ({ type: 'FAIL' }),
            someList => ({ type: 'B', someList }),
        ));

        await expect(store.getActions()).toEqual(expectedActions);
    });

    test('GET a one thing', async () => {
        fetchMock.getOnce(
            `${API_URL}/asdf`,
            {
                body: { foo_fuu: 'asdf' },
                headers: { 'Content-Type': 'application/json' },
            },
        );
        const expectedActions = [
            { type: 'A' },
            {
                type: 'B',
                oneThing: { fooFuu: 'asdf' },
            },
        ];
        const store = mockStore({});

        const request = new Request();
        await store.dispatch(request.GET(
            '/asdf',
            () => ({ type: 'A' }),
            () => ({ type: 'FAIL' }),
            oneThing => ({ type: 'B', oneThing }),
        ));

        await expect(store.getActions()).toEqual(expectedActions);
    });

    test('GET FAIL 404', async () => {
        fetchMock.getOnce(
            `${API_URL}/asdf`,
            { status: 404 },
        );
        const expectedActions = [
            { type: 'A' },
            {
                type: 'FAIL',
            },
        ];
        const store = mockStore({});

        const request = new Request();
        await store.dispatch(request.GET(
            '/asdf',
            () => ({ type: 'A' }),
            () => ({ type: 'FAIL' }),
            some => ({ type: 'B', some }),
        ));

        await expect(store.getActions()).toEqual(expectedActions);
    });
});

describe('Test Request POST', () => {
    afterEach(fetchMock.restore);

    test('POST a thing', async () => {
        fetchMock.postOnce(
            `${API_URL}/asdf`,
            {
                body: { foo_fuu: 'asdf' },
                headers: { 'Content-Type': 'application/json' },
                status: 201,
            },
        );
        const expectedActions = [
            { type: 'A' },
            {
                type: 'B',
                some: { fooFuu: 'asdf' },
            },
        ];
        const store = mockStore({});

        const request = new Request();
        await store.dispatch(request.POST(
            '/asdf',
            { fooFuu: 'asdf' },
            () => ({ type: 'A' }),
            () => ({ type: 'FAIL' }),
            some => ({ type: 'B', some }),
        ));

        await expect(store.getActions()).toEqual(expectedActions);
    });

    test('POST a thing and FAIL', async () => {
        fetchMock.postOnce(
            `${API_URL}/asdf`,
            {
                body: { fail_message: 'fail' },
                headers: { 'Content-Type': 'application/json' },
                status: 400,
            },
        );
        const expectedActions = [
            { type: 'A' },
            {
                type: 'FAIL',
            },
        ];
        const store = mockStore({});

        const request = new Request();
        await store.dispatch(request.POST(
            '/asdf',
            { fooFuu: 'asdf' },
            () => ({ type: 'A' }),
            () => ({ type: 'FAIL' }),
            some => ({ type: 'B', some }),
        ));

        await expect(store.getActions()).toEqual(expectedActions);
    });
});

describe('Test Request PUT', () => {
    afterEach(fetchMock.restore);

    test('PUT a thing', async () => {
        fetchMock.putOnce(
            `${API_URL}/asdf`,
            {
                body: { foo_fuu: 'asdf' },
                headers: { 'Content-Type': 'application/json' },
                status: 200,
            },
        );
        const expectedActions = [
            { type: 'A' },
            {
                type: 'B',
                some: { fooFuu: 'asdf' },
            },
        ];
        const store = mockStore({});

        const request = new Request();
        await store.dispatch(request.PUT(
            '/asdf',
            { fooFuu: 'asdf' },
            () => ({ type: 'A' }),
            () => ({ type: 'FAIL' }),
            some => ({ type: 'B', some }),
        ));

        await expect(store.getActions()).toEqual(expectedActions);
    });

    test('PUT a thing and FAIL', async () => {
        fetchMock.putOnce(
            `${API_URL}/asdf`,
            {
                body: { fail_message: 'fail' },
                headers: { 'Content-Type': 'application/json' },
                status: 400,
            },
        );
        const expectedActions = [
            { type: 'A' },
            {
                type: 'FAIL',
            },
        ];
        const store = mockStore({});

        const request = new Request();
        await store.dispatch(request.PUT(
            '/asdf',
            { fooFuu: 'asdf' },
            () => ({ type: 'A' }),
            () => ({ type: 'FAIL' }),
            some => ({ type: 'B', some }),
        ));

        await expect(store.getActions()).toEqual(expectedActions);
    });
});

describe('Test Request DELETE', () => {
    afterEach(fetchMock.restore);

    test('DELETE a thing', async () => {
        fetchMock.deleteOnce(
            `${API_URL}/asdf`,
            {
                status: 204,
            },
        );
        const expectedActions = [
            { type: 'A' },
            { type: 'B' },
        ];
        const store = mockStore({});

        const request = new Request();
        await store.dispatch(request.DELETE(
            '/asdf',
            () => ({ type: 'A' }),
            () => ({ type: 'FAIL' }),
            () => ({ type: 'B' }),
        ));

        await expect(store.getActions()).toEqual(expectedActions);
    });

    test('DELETE a thing and FAIL', async () => {
        fetchMock.deleteOnce(
            `${API_URL}/asdf`,
            {
                status: 400,
            },
        );
        const expectedActions = [
            { type: 'A' },
            { type: 'FAIL' },
        ];
        const store = mockStore({});

        const request = new Request();
        await store.dispatch(request.DELETE(
            '/asdf',
            () => ({ type: 'A' }),
            () => ({ type: 'FAIL' }),
            () => ({ type: 'B' }),
        ));

        await expect(store.getActions()).toEqual(expectedActions);
    });
});
