import * as persistence from './persistence';
import { loggedInRequest } from './login';

export const BUDDY_REQUEST = 'BUDDY_REQUEST';
export const BUDDY_FAIL = 'BUDDY_FAIL';
export const BUDDY_RECEIVE = 'BUDDY_RECEIVE';

export const MESSAGES_REQUEST = 'MESSAGES_REQUEST';
export const MESSAGES_RECEIVE = 'MESSAGES_RECEIVE';
export const MESSAGES_FAIL = 'MESSAGES_FAIL';

export const OPEN_REQUEST = 'OPEN_REQUEST';
export const OPEN_RECEIVE = 'OPEN_RECEIVE';
export const OPEN_FAIL = 'OPEN_FAIL';

export const RESET_CHATS = 'RESET_CHATS';

export const SEND_REQUEST = 'SEND_REQUEST';
export const SEND_RECEIVE = 'SEND_RECEIVE';
export const SEND_FAIL = 'SEND_FAIL';

export const DELETE_REQUEST = 'DELETE_REQUEST';
export const DELETE_RECEIVE = 'DELETE_RECEIVE';
export const DELETE_FAIL = 'DELETE_FAIL';

export const RECIPIENT_ID = 'RECIPIENT_ID';

export const SET_MESSAGE = 'SET_MESSAGE';

export function setRecipientId(id) {
    return {
        type: RECIPIENT_ID,
        recipientId: id,
    };
}

function onFailureChats() {
    return (dispatch) => {
        dispatch({
            type: MESSAGES_FAIL,
        });
    };
}

export function getBuddy(id) {
    return async (dispatch, getState) => {
        const { chats: { buddies: { [id]: thisBuddy = false } } } = getState();
        if (thisBuddy) {
            return null;
        }
        const request = loggedInRequest(dispatch, getState);
        await persistence.storeData('hasBuddies', true);
        return dispatch(request.GET(
            `/users/${id}`,
            () => ({
                type: BUDDY_REQUEST,
                buddy: { [id]: { displayName: 'fetching...', id } },
            }),
            () => ({
                type: BUDDY_FAIL,
                failedBuddyId: id,
            }),
            buddy => ({
                type: BUDDY_RECEIVE,
                buddy: { [id]: buddy },
            }),
        ));
    };
}

export function sortedIdList(messages) {
    return Object.keys(messages).sort((idA, idB) => {
        const { [idA]: [{ created: a }], [idB]: [{ created: b }] } = messages;
        return ((new Date(a)) > (new Date(b))) ? -1 : 1;
    });
}

export function unseenMessages(messages) {
    return Object.keys(messages).reduce(
        (acc, id) => {
            const { [id]: [{ isOurs, opened, id: messageId }] } = messages;
            if (isOurs || opened) {
                return acc;
            }
            return {
                ...acc,
                [id]: messageId,
            };
        },
        {},
    );
}

export function normalizeMessages(messageList, userId) {
    messageList.sort((messageA, messageB) => {
        const [{ created: a }, { created: b }] = [messageA, messageB];
        return (new Date(a)) > (new Date(b)) ? -1 : 1;
    });
    const messages = messageList.reduce(
        (acc, message) => {
            const { senderId, recipientId } = message;
            const isOurs = (senderId === userId);
            const id = isOurs ? recipientId : senderId;
            const { [id]: list = [] } = acc;
            list.push({
                ...message,
                isOurs,
                dateBubble: false,
                bubbleSpike: true,
            });
            acc[id] = list;
            return acc;
        },
        {},
    );
    Object.values(messages).forEach((list) => {
        const { length } = list;
        for (let i = 0; i < length - 1; i += 1) {
            const currentMessage = list[i];
            const nextMessage = list[i + 1];
            const { created: A } = currentMessage;
            const { created: B } = nextMessage;
            if (A.split('T')[0] !== B.split('T')[0]) {
                currentMessage.dateBubble = true;
            }
        }
        const message = list[length - 1];
        message.dateBubble = true;
    });
    return messages;
}

export function getBuddies() {
    return async (dispatch, getState) => {
        const {
            chats: { buddies, sortedIds },
        } = getState();
        const missing = sortedIds.filter(id => !buddies[id]);
        await Promise.all(missing.map(async id => dispatch(getBuddy(id))));
    };
}

export function getMessages() {
    return async (dispatch, getState) => {
        const { chats: { isFetching }, login: { userId } } = getState();
        if (isFetching) {
            return;
        }
        const request = loggedInRequest(dispatch, getState);
        await dispatch(request.GET(
            `/users/${userId}/messages`,
            () => ({ type: MESSAGES_REQUEST }),
            onFailureChats,
            (messageList) => {
                const messages = normalizeMessages(messageList, userId);
                const sortedIds = sortedIdList(messages);
                const unseen = unseenMessages(messages);
                return {
                    type: MESSAGES_RECEIVE,
                    messages,
                    sortedIds,
                    unseen,
                };
            },
        ));
    };
}

export function deleteConversation(buddyId) {
    return async (dispatch, getState) => {
        const {
            login: { userId },
            chats: { isDeleting },
        } = getState();
        if (isDeleting) {
            return;
        }
        const request = loggedInRequest(dispatch, getState);
        await dispatch(request.DELETE(
            `/users/${userId}/messages?buddy_id=${buddyId}`,
            () => ({ type: DELETE_REQUEST }),
            () => ({ type: DELETE_FAIL }),
            () => ({ type: DELETE_RECEIVE }),
        ));
    };
}

export function openMessage(buddyId) {
    return async (dispatch, getState) => {
        const {
            login: { userId },
            chats: {
                isOpening,
                unseen,
            },
        } = getState();
        const { [buddyId]: messageId = '' } = unseen;
        if (isOpening || !messageId) {
            return null;
        }
        const request = loggedInRequest(dispatch, getState);
        return dispatch(request.PUT(
            `/users/${userId}/messages/${messageId}`,
            {},
            () => ({ type: OPEN_REQUEST }),
            () => ({ type: OPEN_FAIL }),
            () => ({ type: OPEN_RECEIVE }),
        ));
    };
}

export function onFailureSend() {
    return async dispatch => dispatch({ type: SEND_FAIL });
}

export function sendMessage(recipientId, content) {
    return async (dispatch, getState) => {
        const { chats: { isSending }, login: { userId } } = getState();
        if (isSending) {
            return null;
        }
        const request = loggedInRequest(dispatch, getState);
        return dispatch(request.POST(
            `/users/${userId}/messages`,
            {
                content,
                recipientId,
                senderId: userId,
                opened: false,
            },
            () => ({ type: SEND_REQUEST }),
            () => onFailureSend(),
            () => ({ type: SEND_RECEIVE }),
        ));
    };
}

export const setMessage = (recipientId, content) => ({
    type: SET_MESSAGE, payload: { recipientId, content },
});
