import 'react-native';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import fetchMock from 'fetch-mock';
import I18n from '../i18n';

import API_URL from '../../config';
import {
    backOff,
    refresh,
    loggedInFetch,
    login,
    logout,
    LOGIN_REQUEST,
    LOGIN_RECEIVE,
    LOGIN_FAIL,
    LOGOUT,
    REFRESH_REQUEST,
    REFRESH_RECEIVE,
} from './login';
import { NAVIGATE, CLEAR_HISTORY } from './navigation';
import { Request } from './request';

const mockStore = configureMockStore([thunk]);

describe('Test refresh', () => {
    afterEach(fetchMock.restore);
    test('Fail backoff', async () => {
        const b = await backOff(() => false, 1, 5);
        await expect(b).toEqual(false);
    });
    test('Success backoff', async () => {
        const b = await backOff(() => true, 1, 5);
        await expect(b).toEqual(true);
    });

    test('refresh', async () => {
        fetchMock.postOnce(
            `${API_URL}/refresh`,
            {
                body: { access_token: 'qwer' },
                headers: { 'Content-Type': 'application/json' },
                status: 201,
            },
        );

        const expectedActions = [
            { type: REFRESH_REQUEST },
            {
                type: REFRESH_RECEIVE,
                accessToken: 'qwer',
            },
        ];
        const store = mockStore({
            login: {
                isRefreshing: false,
                refreshToken: 'asdf',
            },
        });
        await refresh(store.dispatch, store.getState);
        await expect(store.getActions()).toEqual(expectedActions);
    });

    test('refresh', async () => {
        const expectedActions = [];
        const store = mockStore({});
        let time = 0;
        const mockGetState = () => {
            time += 5;
            return {
                login: { isRefreshing: (time < 10) },
            };
        };
        await refresh(store.dispatch, mockGetState);
        await expect(store.getActions()).toEqual(expectedActions);
    });
});

describe('Test loggedInFetch', () => {
    afterEach(fetchMock.restore);
    test('test resp ok', async () => {
        fetchMock.getOnce(
            `${API_URL}/asdf`,
            {
                body: { resources: [{ foo_fuu: 'asdf' }] },
                headers: { 'Content-Type': 'application/json' },
            },
        );
        const store = mockStore({
            login: {
                accessToken: 'asdf',
            },
        });
        const fun = loggedInFetch(store.dispatch, store.getState);
        const res = await fun('/asdf', 'GET');
        await expect(res.ok).toEqual(true);
    });
    test('GET one thing', async () => {
        fetchMock.getOnce(
            `${API_URL}/asdf`,
            {
                body: { foo_fuu: 'asdf' },
                headers: { 'Content-Type': 'application/json' },
            },
        );
        const expectedActions = [
            { type: 'A' },
            {
                type: 'B',
                oneThing: { fooFuu: 'asdf' },
            },
        ];
        const store = mockStore({ login: { accessToken: 'asdf' } });

        const request = new Request(
            loggedInFetch(store.dispatch, store.getState),
        );
        await store.dispatch(request.GET(
            '/asdf',
            () => ({ type: 'A' }),
            () => ({ type: 'FAIL' }),
            oneThing => ({ type: 'B', oneThing }),
        ));

        await expect(store.getActions()).toEqual(expectedActions);
    });
});

describe('Test login', () => {
    afterEach(fetchMock.restore);
    test('test login ok', async () => {
        fetchMock.postOnce(
            `${API_URL}/login`,
            {
                body: {
                    tokens: {
                        access_token: 'a',
                        refresh_token: 'b',
                    },
                    scopes: {
                        account_id: 'c',
                        user_id: 'd',
                        mentor_id: 'e',
                    },
                },
                headers: { 'Content-Type': 'application/json' },
            },
        );
        const store = mockStore({
            login: {
                isFetching: false,
            },
        });
        const expectedActions = [
            { type: LOGIN_REQUEST },
            {
                type: LOGIN_RECEIVE,
                tokens: {
                    accessToken: 'a',
                    refreshToken: 'b',
                },
                scopes: {
                    accountId: 'c',
                    userId: 'd',
                    mentorId: 'e',
                },
            },
            {
                type: NAVIGATE,
                nextScreen: {
                    headerVisible: true,
                    left: 'chat',
                    right: 'profile',
                    title: 'Mentor List',
                    screen: 'LoggedInMentorList',
                },
            },
            { type: CLEAR_HISTORY },
        ];
        await store.dispatch(login('liirum', 'laarum'));
        await expect(store.getActions()).toEqual(expectedActions);
    });
    test('test login ok with callback', async () => {
        fetchMock.postOnce(
            `${API_URL}/login`,
            {
                body: {
                    tokens: {
                        access_token: 'a',
                        refresh_token: 'b',
                    },
                    scopes: {
                        account_id: 'c',
                        user_id: 'd',
                        mentor_id: 'e',
                    },
                },
                headers: { 'Content-Type': 'application/json' },
            },
        );
        const store = mockStore({
            login: {
                isFetching: false,
            },
        });
        const expectedActions = [
            { type: LOGIN_REQUEST },
            {
                type: LOGIN_RECEIVE,
                tokens: {
                    accessToken: 'a',
                    refreshToken: 'b',
                },
                scopes: {
                    accountId: 'c',
                    userId: 'd',
                    mentorId: 'e',
                },
            },
            {
                type: NAVIGATE,
                nextScreen: {
                    headerVisible: true,
                    left: 'chat',
                    right: 'profile',
                    title: 'Mentor List',
                    screen: 'LoggedInMentorList',
                },
            },
            { type: CLEAR_HISTORY },
        ];
        const callback = jest.fn();
        await store.dispatch(login('liirum', 'laarum', callback));
        await expect(store.getActions()).toEqual(expectedActions);
        expect(callback.mock.calls.length).toBe(1);
    });
    test('Already request', async () => {
        const store = mockStore({
            login: {
                isFetching: true,
            },
        });
        const expectedActions = [];
        await store.dispatch(login('liirum', 'laarum'));
        await expect(store.getActions()).toEqual(expectedActions);
    });
    test('test login fail', async () => {
        fetchMock.postOnce(
            `${API_URL}/login`,
            {
                body: {
                    message: 'FAIL',
                },
                headers: { 'Content-Type': 'application/json' },
            },
        );
        const store = mockStore({
            login: {
                isFetching: false,
            },
        });
        const expectedActions = [
            { type: LOGIN_REQUEST },
            {
                type: LOGIN_FAIL,
            },
            {
                errorMessage: I18n.t('modalLoginFailed'),
                type: 'ERROR_SET_MESSAGE',
            },
        ];
        await store.dispatch(login('liirum', 'laarum'));
        await expect(store.getActions()).toEqual(expectedActions);
    });
});

describe('Test logout', () => {
    test('test logout ok', async () => {
        const expectedActions = [
            { type: LOGOUT },
            {
                type: NAVIGATE,
                nextScreen: {
                    headerVisible: false,
                    screen: 'MentorList',
                },
            },
            { type: CLEAR_HISTORY },
        ];
        const store = mockStore({});
        await store.dispatch(logout());
        await expect(store.getActions()).toEqual(expectedActions);
    });
});
