import { Request } from './request';

export const MENTORS_REQUEST = 'MENTORS_REQUEST';
export const MENTORS_RECEIVE = 'MENTORS_RECEIVE';
export const MENTORS_FAIL = 'MENTORS_FAIL';

export const SELECT_MENTOR = 'SELECT_MENTOR';

function onFailure() {
    return (dispatch) => {
        dispatch({
            type: MENTORS_FAIL,
        });
    };
}

export function selectMentor(selectedMentor) {
    return {
        type: SELECT_MENTOR,
        selectedMentor,
    };
}

export function getMentors() {
    return async (dispatch, getState) => {
        const { mentors: { isFetching } } = getState();
        if (isFetching) {
            return;
        }
        const request = new Request();
        await dispatch(request.GET(
            '/mentors',
            () => ({ type: MENTORS_REQUEST }),
            onFailure,
            mentors => ({
                type: MENTORS_RECEIVE,
                mentorObject: mentors.reduce(
                    (acc, x) => ({ ...acc, [x.id]: x }), {},
                ),
                mentorBuddies: mentors.reduce(
                    (acc, x) => ({ ...acc, [x.userId]: x }), {},
                ),
            }),
        ));
    };
}
