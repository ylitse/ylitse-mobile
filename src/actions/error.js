export const ActionTypes = {
    ERROR_SET_MESSAGE: 'ERROR_SET_MESSAGE',
    ERROR_RESET_MESSAGE: 'ERROR_RESET_MESSAGE',
};

export function setErrorMessage(message) {
    return {
        type: ActionTypes.ERROR_SET_MESSAGE,
        errorMessage: message,
    };
}

export function resetErrorMessage() {
    return {
        type: ActionTypes.ERROR_RESET_MESSAGE,
    };
}
