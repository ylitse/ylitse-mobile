import I18n from '../i18n';

export const NAVIGATE = 'NAVIGATE';
export const BACK = 'BACK';
export const CLEAR_HISTORY = 'CLEAR_HISTORY';

export function navigate(screen) {
    const headers = {
        LoggedInMentorList: {
            left: 'chat',
            title: I18n.t('headerMentorList'),
            right: 'profile',
        },
        MentorProfile: {
            left: 'back',
            title: I18n.t('headerFullStory'),
            right: '',
        },
        MyProfile: {
            left: 'back',
            title: I18n.t('headerMyProfile'),
            right: '',
        },
        ContactList: {
            left: 'mentors',
            title: I18n.t('headerConversations'),
            right: '',
        },
        Chatting: {
            left: 'back',
            right: '',
        },
    };
    const { [screen]: headerContent = {} } = headers;
    if (screen === 'Chatting') {
        return (dispatch, getState) => {
            const {
                chats: { recipientId, buddies },
                mentors: { mentorBuddies },
            } = getState();

            let { [recipientId]: buddy = false } = buddies;
            if (!buddy) {
                const {
                    [recipientId]: mentorBuddy = { displayName: '' },
                } = mentorBuddies;
                buddy = mentorBuddy;
            }
            const { displayName } = buddy;
            dispatch({
                type: NAVIGATE,
                nextScreen: {
                    ...headerContent,
                    headerVisible: true,
                    title: displayName,
                    screen,
                },
            });
        };
    }
    return {
        type: NAVIGATE,
        nextScreen: {
            ...headerContent,
            headerVisible: Object.keys(headerContent).length !== 0,
            screen,
        },
    };
}

export function clearHistory() {
    return {
        type: CLEAR_HISTORY,
    };
}

export function back() {
    return {
        type: BACK,
    };
}
