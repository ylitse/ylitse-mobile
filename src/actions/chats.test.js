import 'react-native';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import fetchMock from 'fetch-mock';

import {
    getMessages,
    openMessage,
    sendMessage,
    getBuddy,
    getBuddies,
    normalizeMessages,
    sortedIdList,
    unseenMessages,

    BUDDY_REQUEST,
    BUDDY_FAIL,
    BUDDY_RECEIVE,

    MESSAGES_REQUEST,
    MESSAGES_RECEIVE,
    MESSAGES_FAIL,

    OPEN_REQUEST,
    OPEN_RECEIVE,
    OPEN_FAIL,

    SEND_REQUEST,
    SEND_RECEIVE,
    SEND_FAIL,
} from './chats';
import API_URL from '../../config';
import I18n from '../i18n';

const userId = 'myId';
const otherId = 'noMyId';

const msg1Their = {
    content: 'msg1',
    recipientId: userId,
    senderId: otherId,
    opened: false,
    id: 'DZ0Iseiot_IOszY1ARWhUipY1GPCkv9u2pG5eqWkDYI',
    created: '2018-08-17T05:51:25.915640',
    updated: '2018-08-17T05:51:25.915649',
    active: true,
};

const msg2Our = {
    content: 'msg2',
    recipientId: otherId,
    senderId: userId,
    opened: false,
    id: '4Kh6t9bwVkZFcoSEQ22dfsiukvOuaIqp5UY5dfJAS20',
    created: '2018-08-17T06:39:12.483811',
    updated: '2018-08-17T06:39:12.483822',
    active: true,
};

const msg3Their = {
    content: 'msg3',
    recipientId: userId,
    senderId: otherId,
    opened: true,
    id: 'g5h6WKcXy-1ijXHxYwR2H7pl3ZAbbh0cA6SiOysIQhE',
    created: '2018-08-17T07:04:11.975608',
    updated: '2018-08-17T07:04:11.975618',
    active: true,
};

const mockStore = configureMockStore([thunk]);

describe('getBuddy action', () => {
    afterEach(fetchMock.restore);

    test('fetch is stopped if loading', async () => {
        const store = mockStore({
            chats: { buddies: {} },
            login: { userId },
        });
        const expectedActions = [
            {
                type: BUDDY_REQUEST,
                buddy: { asdf: { displayName: 'fetching...', id: 'asdf' } },
            },
            {
                type: BUDDY_FAIL,
                failedBuddyId: 'asdf',
            },
        ];
        await store.dispatch(getBuddy('asdf'));
        await expect(store.getActions()).toEqual(expectedActions);
    });

    test('Works', async () => {
        const dummyBuddy = {
            user_id: 'asdf',
            display_name: 'janteri',
            role: 'mentor',
        };
        fetchMock.getOnce(
            `${API_URL}/users/asdf`,
            {
                body: { ...dummyBuddy },
                headers: { 'Content-Type': 'application/json' },
            },
        );
        const expectedActions = [
            {
                type: BUDDY_REQUEST,
                buddy: { asdf: { displayName: 'fetching...', id: 'asdf' } },
            },
            {
                type: BUDDY_RECEIVE,
                buddy: {
                    asdf: {
                        userId: 'asdf',
                        displayName: 'janteri',
                        role: 'mentor',
                    },
                },
            },
        ];
        const store = mockStore({
            chats: { buddies: {} },
            login: { userId },
        });
        await store.dispatch(getBuddy('asdf'));
        await expect(store.getActions()).toEqual(expectedActions);
    });

    test('Fails', async () => {
        fetchMock.getOnce(
            `${API_URL}/users/asdf`,
            {
                status: 400,
            },
        );
        const expectedActions = [
            {
                type: BUDDY_REQUEST,
                buddy: { asdf: { displayName: 'fetching...', id: 'asdf' } },
            },
            {
                type: BUDDY_FAIL,
                failedBuddyId: 'asdf',
            },
        ];
        const store = mockStore({
            chats: { buddies: {} },
            login: { userId },
        });
        await store.dispatch(getBuddy('asdf'));
        await expect(store.getActions()).toEqual(expectedActions);
    });
});

describe('fetchMessages helper functions', () => {
    test('Normalize messages', () => {
        const messageList = [msg3Their, msg2Our, msg1Their];
        const normalized = normalizeMessages(messageList, userId);
        const expectedObject = {
            [otherId]: [
                {
                    isOurs: false,
                    bubbleSpike: true,
                    dateBubble: false,
                    ...msg3Their,
                },
                {
                    isOurs: true,
                    bubbleSpike: true,
                    dateBubble: false,
                    ...msg2Our,
                },
                {
                    isOurs: false,
                    bubbleSpike: true,
                    dateBubble: true,
                    ...msg1Their,
                },
            ],
        };
        expect(normalized).toEqual(expectedObject);
    });
    test('List ids of buddies', () => {
        const messageList = [msg3Their, msg2Our, msg1Their];
        const normalized = normalizeMessages(messageList, userId);
        const expectedList = [otherId];
        expect(sortedIdList(normalized)).toEqual(expectedList);
    });
    test('Unseen messages 1', () => {
        const messageList = [msg3Their, msg2Our, msg1Their];
        const normalized = normalizeMessages(messageList, userId);
        const expectedObject = {};
        expect(unseenMessages(normalized)).toEqual(expectedObject);
    });
    test('Unseen messages 2', () => {
        const msg3 = {
            ...msg3Their,
            opened: false,
            bubbleSpike: true,
            dateBubble: false,
            isOurs: false,
        };
        const messageList = [
            msg3,
            msg2Our,
            msg1Their,
        ];
        const { id: messageId } = msg3;
        const normalized = normalizeMessages(messageList, userId);
        const expectedObject = { [otherId]: messageId };
        expect(unseenMessages(normalized)).toEqual(expectedObject);
    });
});

describe('Get missing buddies', () => {
    afterEach(fetchMock.restore);
    test('does not do anything if no missing buddies', async () => {
        const store = mockStore({
            chats: {
                isFetching: true,
                buddies: { [otherId]: { displayName: 'janteri' } },
                sortedIds: [otherId],
            },
            login: { userId },
        });
        const expectedActions = [];
        await store.dispatch(getBuddies([otherId]));
        await expect(store.getActions()).toEqual(expectedActions);
    });
    test('Tries to fetch buddy if there are missing ones', async () => {
        const dummyBuddy1 = {
            user_id: 'asdf',
            display_name: 'asdf',
            role: 'mentee',
        };
        const dummyBuddy2 = {
            user_id: 'qwer',
            display_name: 'qwer',
            role: 'mentee',
        };
        fetchMock.getOnce(
            `${API_URL}/users/asdf`,
            {
                body: { ...dummyBuddy1 },
                headers: { 'Content-Type': 'application/json' },
            },
        );
        fetchMock.getOnce(
            `${API_URL}/users/qwer`,
            {
                body: { ...dummyBuddy2 },
                headers: { 'Content-Type': 'application/json' },
            },
        );
        const store = mockStore({
            chats: {
                isFetching: true,
                buddies: {},
                sortedIds: ['asdf', 'qwer'],
            },
            login: { userId },
        });
        await store.dispatch(getBuddies());
        const actions = store.getActions();
        await expect(actions).toContainEqual({
            type: BUDDY_REQUEST,
            buddy: { asdf: { displayName: 'fetching...', id: 'asdf' } },
        });
        await expect(actions).toContainEqual({
            type: BUDDY_REQUEST,
            buddy: { qwer: { displayName: 'fetching...', id: 'qwer' } },
        });
        await expect(actions).toContainEqual({
            type: BUDDY_RECEIVE,
            buddy: {
                asdf: { userId: 'asdf', displayName: 'asdf', role: 'mentee' },
            },
        });
        await expect(actions).toContainEqual({
            type: BUDDY_RECEIVE,
            buddy: {
                qwer: { userId: 'qwer', displayName: 'qwer', role: 'mentee' },
            },
        });
    });
});

describe('Get messages', () => {
    afterEach(fetchMock.restore);

    test('fetch is stopped if loading', async () => {
        const store = mockStore({
            chats: {
                isFetching: true,
                buddies: {},
                messages: {},
            },
            login: { userId },
        });
        const expectedActions = [];
        await store.dispatch(getMessages());
        await expect(store.getActions()).toEqual(expectedActions);
    });

    test('Works', async () => {
        const msglist = [msg2Our, msg1Their, msg3Their];
        fetchMock.getOnce(
            `${API_URL}/users/${userId}/messages`,
            {
                body: { resources: msglist },
                headers: { 'Content-Type': 'application/json' },
            },
        );
        const store = mockStore({
            chats: {
                isFetching: false,
                buddies: { [otherId]: { displayName: 'janteri' } },
            },
            login: { userId },
        });
        const messages = normalizeMessages(msglist, userId);
        const unseen = unseenMessages(messages);
        const sortedIds = sortedIdList(messages);
        const expectedActions = [
            { type: MESSAGES_REQUEST },
            {
                type: MESSAGES_RECEIVE,
                messages,
                unseen,
                sortedIds,
            },
        ];
        await store.dispatch(getMessages());
        await expect(store.getActions()).toEqual(expectedActions);
    });

    test('Fails', async () => {
        fetchMock.getOnce(
            `${API_URL}/users/${userId}/messages`,
            {
                status: 400,
            },
        );
        const expectedActions = [
            { type: MESSAGES_REQUEST },
            { type: MESSAGES_FAIL },
            {
                type: 'ERROR_SET_MESSAGE',
                errorMessage: I18n.t('chatFetchFailed'),
            },
        ];
        const store = mockStore({
            chats: { isFetching: false },
            login: { userId },
        });
        await store.dispatch(getMessages());
        await expect(store.getActions()).toEqual(expectedActions);
    });
});

describe('Open message action', () => {
    afterEach(fetchMock.restore);

    test('fetch is stopped if loading', async () => {
        const store = mockStore({
            chats: {
                unseen: {},
                isOpening: true,
            },
            login: { userId },
        });
        const expectedActions = [];
        await store.dispatch(openMessage('buddyid'));
        await expect(store.getActions()).toEqual(expectedActions);
    });

    test('Works', async () => {
        const { id } = msg3Their;
        fetchMock.putOnce(
            `${API_URL}/users/${userId}/messages/${id}`,
            {
                body: { ...msg3Their, opened: true },
                headers: { 'Content-Type': 'application/json' },
            },
        );
        const expectedActions = [
            { type: OPEN_REQUEST },
            { type: OPEN_RECEIVE },
        ];
        const store = mockStore({
            chats: {
                isOpening: false,
                unseen: { [otherId]: id },
            },
            login: { userId },
        });
        await store.dispatch(openMessage(otherId));
        await expect(store.getActions()).toEqual(expectedActions);
    });

    test('Fails', async () => {
        fetchMock.putOnce(
            `${API_URL}/users/${userId}/messages/asdf`,
            {
                status: 404,
            },
        );
        const expectedActions = [
            { type: OPEN_REQUEST },
            { type: OPEN_FAIL },
        ];
        const store = mockStore({
            chats: {
                isOpening: false,
                unseen: { asdf: { id: 'asdf' } },
            },
            login: { userId },
        });
        await store.dispatch(openMessage('asdf'));
        await expect(store.getActions()).toEqual(expectedActions);
    });
});

describe('send message action', () => {
    afterEach(fetchMock.restore);

    test('fetch is stopped if loading', async () => {
        const store = mockStore({
            chats: { isSending: true },
            login: { userId },
        });
        const expectedActions = [];
        await store.dispatch(sendMessage(otherId, msg3Their.content));
        await expect(store.getActions()).toEqual(expectedActions);
    });

    test('Works', async () => {
        fetchMock.postOnce(
            `${API_URL}/users/${userId}/messages`,
            {
                body: msg3Their,
                headers: { 'Content-Type': 'application/json' },
            },
        );
        const expectedActions = [
            { type: SEND_REQUEST },
            { type: SEND_RECEIVE },
        ];
        const store = mockStore({
            chats: { isSending: false },
            login: { userId },
        });
        await store.dispatch(sendMessage(otherId, msg3Their.content));
        await expect(store.getActions()).toEqual(expectedActions);
    });

    test('Fails', async () => {
        fetchMock.postOnce(
            `${API_URL}/users/${userId}/messages`,
            {
                status: 400,
            },
        );
        const expectedActions = [
            { type: SEND_REQUEST },
            { type: SEND_FAIL },
            {
                type: 'ERROR_SET_MESSAGE',
                errorMessage: I18n.t('sendMessageFailed'),
            },
        ];
        const store = mockStore({
            chats: { isSending: false },
            login: { userId },
        });
        await store.dispatch(sendMessage(otherId, msg3Their));
        await expect(store.getActions()).toEqual(expectedActions);
    });
});

