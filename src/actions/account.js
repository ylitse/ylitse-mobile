import { setErrorMessage } from './error';
import I18n from '../i18n';
import { logout, loggedInRequest } from './login';
import { Request } from './request';

export const ActionTypes = {
    ACCOUNT_REQUEST: 'ACCOUNT_REQUEST',
    ACCOUNT_RECEIVE: 'ACCOUNT_RECEIVE',
    ACCOUNT_FAIL: 'ACCOUNT_FAIL',

    UPDATE_PASSWORD_REQUEST: 'UPDATE_PASSWORD_REQUEST',
    UPDATE_PASSWORD_RECEIVE: 'UPDATE_PASSWORD_SUCCESS',
    UPDATE_PASSWORD_FAIL: 'UPDATE_PASSWORD_FAIL',
};

function onFailure() {
    return (dispatch) => {
        dispatch({
            type: ActionTypes.ACCOUNT_FAIL,
        });

        dispatch(setErrorMessage(I18n.t('accountFetchFail')));
    };
}

export function getAccount() {
    return async (dispatch, getState) => {
        const { account: { isFetching }, login: { accountId } } = getState();
        if (isFetching) {
            return;
        }
        const request = loggedInRequest(dispatch, getState);
        await dispatch(request.GET(
            `/accounts/${accountId}`,
            () => ({ type: ActionTypes.ACCOUNT_REQUEST }),
            onFailure,
            account => ({
                type: ActionTypes.ACCOUNT_RECEIVE,
                account,
            }),
        ));
    };
}

export function postAccount(account) {
    return async (dispatch, getState) => {
        const { account: { isFetching } } = getState();
        if (isFetching) {
            return;
        }
        const request = new Request();
        await dispatch(request.POST(
            '/accounts',
            account,
            () => ({ type: ActionTypes.ACCOUNT_REQUEST }),
            onFailure,
            json => ({
                type: ActionTypes.ACCOUNT_RECEIVE,
                account: json,
            }),
        ));
    };
}

export function putAccount(updatedAccount) {
    return async (dispatch, getState) => {
        const {
            account: { account, isFetching },
            login: { accountId },
        } = getState();
        if (isFetching) {
            return;
        }
        const request = loggedInRequest(dispatch, getState);
        await dispatch(request.PUT(
            `/accounts/${accountId}`,
            { ...account, ...updatedAccount },
            () => ({ type: ActionTypes.ACCOUNT_REQUEST }),
            onFailure,
            json => ({
                type: ActionTypes.ACCOUNT_RECEIVE,
                account: json,
            }),
        ));
    };
}

export function changePassword(currentPassword, newPassword) {
    return async (dispatch, getState) => {
        const {
            account: { isFetching }, login: { accountId },
        } = getState();

        if (isFetching) {
            return;
        }
        const request = loggedInRequest(dispatch, getState);
        await dispatch(request.PUT(
            `/accounts/${accountId}/password`,
            { currentPassword, newPassword },
            () => ({ type: ActionTypes.UPDATE_PASSWORD_REQUEST }),
            () => {
                dispatch(setErrorMessage(I18n.t('passwordUpdateFail')));
                return { type: ActionTypes.UPDATE_PASSWORD_RECEIVE };
            },
            () => {
                dispatch(setErrorMessage(I18n.t('passwordUpdateOk')));
                return { type: ActionTypes.UPDATE_PASSWORD_RECEIVE };
            },
        ));
    };
}

export function deleteAccount() {
    return async (dispatch, getState) => {
        const { account: { isFetching }, login: { accountId } } = getState();
        if (isFetching) {
            return;
        }

        const request = loggedInRequest(dispatch, getState);
        await dispatch(request.DELETE(
            `/accounts/${accountId}`,
            () => ({ type: ActionTypes.ACCOUNT_REQUEST }),
            () => {
                dispatch(setErrorMessage(I18n.t('deleteAccountFail')));
                return { type: ActionTypes.ACCOUNT_FAIL };
            },
            () => ({
                type: ActionTypes.ACCOUNT_RECEIVE,
                account: {},
            }),
        ));
        await dispatch(logout());
    };
}
