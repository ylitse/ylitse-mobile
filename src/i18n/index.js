import isFinnishPhone from '../utils/isFinnishPhone';

import fi from './locales/fi';
import en from './locales/en';

const lang = isFinnishPhone ? fi : en;

const tranlator = {
    t: str => lang[str],
};

export default tranlator;
