export default {
    headerMentorList: 'Mentor List',
    headerFullStory: 'Full Story',
    headerMyProfile: 'My Profile',
    headerConversations: 'Conversations',
    headerAccountSettings: 'Account Settings',

    loginFormHeaderSubtitle: 'Peer support mentoring service',
    loginFormSignupTitle: 'Log in',
    loginFormRememberDevice: 'Remember this device',
    loginFormSignInButton: 'Login',
    loginFormPrefilledUsername: 'Username',
    loginFormPrefilledPassword: 'Password',

    myMenteeProfileUsername: 'Username (private)',
    myMenteeProfilePassword: 'Password',
    myMenteeProfileChangePassword: 'Change password',
    myMenteeProfileCurrentPassword: 'Current password',
    myMenteeProfileNewPassword: 'New password',
    myMenteeProfileRepeatPassword: 'Repeat password',
    myMenteeProfilePasswordCancel: 'Cancel',
    myMenteeProfilePasswordSave: 'Save',
    myMenteeProfileOtherInformation: 'Other information',
    myMenteeProfileUserGuide: 'User guide',
    myMenteeProfileFeedback: 'Give feedback',
    myMenteeProfileTermsConditions: 'Terms & conditions',
    myMenteeProfileLogout: 'Log out',
    myMenteeProfileDeleteAccount: 'Delete account',

    MyProfileAge: 'yo',
    MyProfileAreaDefault: 'No area',
    MyProfileAgeDefault: 'Ageless',
    MyProfileStoryDefault: 'No story...',
    MyProfilePublicView: 'Public view',
    MyProfileMenteesView: 'Mentee\'s view',

    registerFetchFail: 'Registration failed',

    registerFormHeaderSubtitle: 'Peer support mentoring service',
    registerFormSignupTitle: 'Sign up as mentee',
    registerFormSignupButton: 'Sign up...',
    registerFormPrefilledUsername: 'Username',
    registerFormPrefilledPassword: 'Password',
    registerFormOldAccount: 'Already have an account?',
    registerFormLoginHere: 'Login here!',
    registerFormPasswordEmpty: 'Password cannot be empty',
    registerFormUsernameTooShort: 'Username must be at least 4 characters long',

    registerFormSkipButton: 'Skip this step',

    registerFormEmailHeaderTitle: 'Email address',
    registerFormEmailSubHeaderTitle: 'What if you forget your password?',
    registerFormEmailInputTitle: 'Email address (optional)',
    registerFormEmailInputPlaceholder: 'Email address',
    registerFormEmailDescription: 'Entering your email address wil help you retreive your password later, in case you lose it. We will not use it for anything else.',
    registerFormEmailSaveButton: 'Save & continue',

    RegisterFormDisplayNameHeaderTitle: 'Display name',
    RegisterFormDisplayNameHeaderSubtitle:
        'You can optionally choose a display name',
    RegisterFormDisplayNameTextName: 'How would you like to be called?',
    RegisterFormDisplayNamePrefilledName: 'Display name',
    RegisterFormDisplayNameDescription: '* If you want to stay anonymous to our mentors, please enter a loginName that can\'t identify you.',
    RegisterFormDisplayNameButton: 'Save & continue',

    RegisterFormPrivacyHeaderTitle: 'Data Privacy & Security',
    RegisterFormPrivacyDescription1: 'We only use your data and message history to make this service possible. We follow high security standards and do our best to keep your conversations private and safe.',
    RegisterFormPrivacyDescription2: 'Please note that we use anonymous statistics to evaluate the service and its usefulness.',
    RegisterFormPrivacyDescription3: 'By continuing you agree to trust us with your data. Read these for more details:',
    RegisterFormPrivacyLink: 'Data Privacy document',
    RegisterFormPrivacyTermsLink: 'Terms & conditions',
    RegisterFormPrivacyButton: 'Agree and Sign up',

    accountSettingsChangePassword: 'Change password',
    accountSettingsCurrentPassword: 'Current password',
    accountSettingsNewPassword: 'New password',
    accountSettingsRepeatPassword: 'Repeat new password',
    accountSettingsCancel: 'Cancel',
    accountSettingsSave: 'Save',
    accountSettingsOtherInformation: 'Other information',
    accountSettingsUserGuide: 'User guide',
    accountSettingsTermsConditions: 'Terms & conditions',
    accountSettingsDeleteAccount: 'Delete my account',
    accountSettingsDeleteConfirmation: 'Are you sure you want to delete your account?',

    mentorListMeetOurMentors: 'Meet our mentors',
    mentorListCreateAccount: 'Create an account to contact mentors',
    mentorListGetStarted: 'Get started',
    mentorListLoginButton: 'Login here!',
    mentorListPrefilledSearch: 'Search by tag or location',

    mentorCardSendMessage: 'Send a message',
    mentorCardFullStory: 'View full story',

    mentorFetchFail: 'Fetching mentors failed',
    menteeFetchFail: 'Fetching mentees failed',

    chatListEmpty: 'No conversations yet...',
    chatAvailableFor: 'I\'m available for',
    chatInputPrefill: 'Write a message',

    chipWrapperICanHelpWith: 'I can help with',

    footerBroughtToYou: 'Brought to you by SOS-Lapsikylä',

    headerDropdownConversations: 'Conversations',
    headerDropdownMentorList: 'Mentor list',

    modalLoginFailed: 'Login failed :(',
    modalFailedButton: 'Ok',
    modalNoButton: 'No',
    modalYesButton: 'Yes',

    chatFetchFailed: 'Fetching the chats failed',
    userFetchFailed: 'Fetching your account failed',
    userDeleteFailed: 'Your account couldn\'t be deleted',

    sendMessageFailed: 'Error when sending your message',
    openMessageFailed: 'Error when opening your message',

    today: 'Today',
    yesterday: 'Yesterday',
    months: ['January', 'February', 'March', 'April', 'May', 'June',
        'July', 'August', 'September', 'October', 'November', 'December'],

    userFetchFail: 'User fetching failed.',
    userPutFail: 'User update failed',
    passwordUpdateYouSure: 'Are you sure you want to update your password?',
    passwordUpdateOk: 'Password updated.',
    passwordUpdateFail: 'Password update failed.',

    LogOutYouSure: 'Are you sure you want to logout?',
    LogOutOk: 'Logged out.',
    LogOutFail: 'Logout failed',

    deleteAccountYouSure: 'Are you sure you want to delete your account.',
    deleteAccountOk: 'Account deleted.',
    deleteAccountFail: 'Account deletion failed.',

    manageMyAccountOnWeb: 'Manage my account (web)',

    loginNameTaken: 'Login name is not available.',

    goBack: 'Back',
    optInForSciece: 'I allow my data to be used in scientific research.',
    toggleBasicTerms: 'I agree',

    areYouSureRemoveConversation: 'Are you sure you want to delete this conversation?',

    accountFetchFail: 'Fetching account failed.',
};
