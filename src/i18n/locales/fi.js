export default {
    headerMentorList: 'Mentorit',
    headerFullStory: 'Tarinani',
    headerMyProfile: 'Profiilini',
    headerConversations: 'Keskustelut',
    headerAccountSettings: 'Asetukset',

    loginFormHeaderSubtitle: 'MentorApp',
    loginFormSignupTitle: 'Kirjaudu palveluun',
    loginFormRememberDevice: 'Muista minut',
    loginFormSignInButton: 'Kirjaudu',
    loginFormPrefilledUsername: 'Käyttäjätunnus',
    loginFormPrefilledPassword: 'Salasana',

    myMenteeProfileUsername: 'Käyttäjätunnus',
    myMenteeProfilePassword: 'Salasana',
    myMenteeProfileChangePassword: 'Vaihda salasana',
    myMenteeProfileCurrentPassword: 'Nykyinen salasana',
    myMenteeProfileNewPassword: 'Uusi salasana',
    myMenteeProfileRepeatPassword: 'Toista salasana',
    myMenteeProfilePasswordCancel: 'Peru',
    myMenteeProfilePasswordSave: 'Tallenna',
    myMenteeProfileOtherInformation: 'Muuta tietoa',
    myMenteeProfileUserGuide: 'Opas mentoreille',
    myMenteeProfileFeedback: 'Anna palautetta',
    myMenteeProfileTermsConditions: 'Käyttöehdot',
    myMenteeProfileLogout: 'Kirjaudu ulos',
    myMenteeProfileDeleteAccount: 'Poista tili',

    MyProfileAge: 'Ikä',
    MyProfileAreaDefault: 'Ei aluetta',
    MyProfileAgeDefault: 'Ei ikää',
    MyProfileStoryDefault: 'Ei omaa tarinaa',
    MyProfilePublicView: 'Julkinen näkymä',
    MyProfileMenteesView: 'Julkinen näkymä',

    registerFetchFail: 'Rekisteröityminen epäonnistui',

    registerFormHeaderSubtitle: 'MentorApp',
    registerFormSignupTitle: 'Rekistöröidy palveluun',
    registerFormTermsConditions: 'Hyväksyn käyttöehdot',
    registerFormSignupButton: 'Rekisteröidy',
    registerFormPrefilledEmail: 'Sähköpostiosoite',
    registerFormPrefilledUsername: 'Käyttäjätunnus',
    registerFormPrefilledPassword: 'Salasana',
    registerFormPrefilledRepeat: 'Toista salasana',
    registerFormOldAccount: 'Minulla on jo tunnukset',
    registerFormLoginHere: 'Kirjaudu sisään',
    registerFormPasswordMismatch: 'Salasanat eivät täsmää',
    registerFormPasswordEmpty: 'Kirjoita salasana',
    registerFormUsernameTooShort:
        'Käyttäjätunnuksessa tulee olla väh. 4 merkkiä',

    registerFormSkipButton: 'Ohita tämä vaihe',

    registerFormEmailHeaderTitle: 'Sähköpostiosoite',
    registerFormEmailSubHeaderTitle: 'Mitä jos unohdat salasanasi?',
    registerFormEmailInputTitle: 'Sähköpostiosoite (vapaaehtoinen)*',
    registerFormEmailInputPlaceholder: 'Sähköpostiosoite',
    registerFormEmailDescription: '* Sähköpostiosoitteen tallentaminen mahdollistaa salasanan palauttamisen. Sähköpostiosoitetta ei käytetä muihin tarkoituksiin, eikä sitä luovuteta kolmansille osapuolille.',
    registerFormEmailSaveButton: 'Tallenna ja jatka',

    RegisterFormDisplayNameHeaderTitle: 'Melkein valmista!',
    RegisterFormDisplayNameHeaderSubtitle: 'Kerro jotain itsestäsi',
    RegisterFormDisplayNameTextName: 'Keksi itsellesi nimimerkki *',
    RegisterFormDisplayNamePrefilledName: 'Nimimerkki',
    RegisterFormDisplayNameDescription: '* Jos haluat pysyä nimettömänä, valitse nimimerkki, josta et ole tunnistettavissa.',
    RegisterFormDisplayNameTextCity: 'Missä asut?',
    RegisterFormDisplayNamePrefilledCity: 'Alue',
    RegisterFormDisplayNameButton: 'Tallenna ja jatka',

    RegisterFormPrivacyHeaderTitle: 'Yksityisyydensuoja',
    RegisterFormPrivacyDescription1: 'Käytämme tietojasi ainoastaan tämän palvelun mahdollistamiseksi. Noudatamme tarkkoja tietoturvastandardeja ja teemme parhaamme, jotta tietosi säilyvät turvassa ja yksityisinä.',
    RegisterFormPrivacyDescription2: 'Huomaathan, että keräämme anonyymejä tilastoja, jotta voimme kehittää palvelua entistä paremmaksi.',
    RegisterFormPrivacyDescription3: 'Rekisteröitymällä palvelun käyttäjäksi hyväksyt, että käsittelemme sinua koskevia tietoja. Voit lukea lisää seuraavista linkeistä:',
    RegisterFormPrivacyLink: 'Tietoja yksityisyydensuojasta',
    RegisterFormPrivacyTermsLink: 'Sovelluksen käyttöehdot',
    RegisterFormPrivacyButton: 'Hyväksy ja rekisteröidy',

    accountSettingsChangePassword: 'Vaihda salasana',
    accountSettingsCurrentPassword: 'Nykyinen salasana',
    accountSettingsNewPassword: 'Uusi salasana',
    accountSettingsRepeatPassword: 'Toista uusi salasana',
    accountSettingsCancel: 'Peruuta',
    accountSettingsSave: 'Tallenna',
    accountSettingsOtherInformation: 'Muut tiedot',
    accountSettingsUserGuide: 'Käyttöopas',
    accountSettingsTermsConditions: 'Käyttöehdot ja tietosuojaseloste',
    accountSettingsDeleteAccount: 'Poista tilini',
    accountSettingsDeleteConfirmation: 'Oletko varma?',

    mentorListMeetOurMentors: 'Tapaa mentorimme',
    mentorListCreateAccount: 'Luo tili aloittaaksesi',
    mentorListGetStarted: 'Aloita tästä',
    mentorListLoginButton: 'Kirjaudu sisään',
    mentorListPrefilledSearch: 'Etsi',

    mentorCardSendMessage: 'Lähetä viesti',
    mentorCardFullStory: 'Lue esittelyni',

    mentorFetchFail: 'Jotain meni pieleen',
    menteeFetchFail: 'Jotain meni pieleen',

    chatListEmpty: 'Et ole aloittanut vielä keskustelua',
    chatAvailableFor: 'Olen käytettävissä',
    chatInputPrefill: 'Kirjoita viesti',

    chipWrapperICanHelpWith: 'Voin auttaa:',
    footerBroughtToYou: 'Palvelun tuottaa SOS-Lapsikylä',

    headerDropdownConversations: 'Keskustelut',
    headerDropdownMentorList: 'Mentorit',

    modalFailedButton: 'Ok',

    modalLoginFailed: 'Kirjautuminen epäonnistui!',
    modalLoginFailedButton: 'Ikävä juttu',
    modalNoButton: 'Ei',
    modalYesButton: 'Kyllä',

    chatFetchFailed: 'Keskustelujen haku epäonnistui',
    userFetchFailed: 'Profiilisi haku ei onnistunut',
    userDeleteFailed: 'Profiilia ei voitu poistaa',

    sendMessageFailed: 'Viestiäsi ei voitu lähettää',
    openMessageFailed: 'Viestiäsi ei voi avata',

    today: 'Tänään',
    yesterday: 'Eilen',
    months: ['Tammikuu', 'Helmikuu', 'Maaliskuu', 'Huhtikuu', 'Toukokuu',
        'Kesäkuu', 'Heinäkuu', 'Elokuu', 'Syyskuu', 'Lokakuu', 'Marraskuu',
        'Joulukuu'],

    userFetchFail: 'Käytäjän haku epäoninstui.',
    userPutFail: 'Käyttäjän päivitys epäonnistui.',
    passwordUpdateYouSure: 'Haluatko varmasti vaihtaa salasanasi?',
    passwordUpdateOk: 'Salasana päivitetty.',
    passwordUpdateFail: 'Salasanan päivitys epäonnistui',

    LogOutYouSure: 'Haluatko varmasti kirjautua ulos?',
    LogOutOk: 'Ulos kirjauduttu.',
    LogOutFail: 'Epäonnistuimme',

    deleteAccountYouSure: 'Haluatko varmasti poistaa käyttäjäsi?',
    deleteAccountOk: 'Käyttäjä poistettu.',
    deleteAccountFail: 'Epäonnistuimme.',

    manageMyAccountOnWeb: 'Muokkaa profiilia (web)',

    loginNameTaken: 'Käyttäjänimi on varattu.',

    goBack: 'Takaisin',
    optInForSciece: 'Tietojani saa käyttää tieteelliseen tutkimukseen',
    toggleBasicTerms: 'Selvä pyy!',

    areYouSureRemoveConversation: 'Oletko varma, että haluat poistaa keskustelun?',

    accountFetchFail: 'Käyttäjän haku epäonnistui.'
};
