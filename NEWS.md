Ylitse Mobile NEWS
==================

Release notes for Ylitse Mobile. This project follows [Semantic Versioning][].

[Semantic Versioning]: http://semver.org/spec/v2.0.0.html

0.1.0+git (unreleased)
----------------------

New features:

* Project setup including testing frameworks etc.
