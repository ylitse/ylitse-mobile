const BASE_URL = 'https://admin.ylitse.fi';
const API_URL = `${BASE_URL}/api`;
export const MANAGE_ACCOUNT_URL = `${BASE_URL}/login`;
export const TERMS_URL = 'https://sos-lapsikyla.fi/fileadmin/user_upload/tiedostot/kuvat/pdf-tiedostot/Ylitse_MentorApp_kayttyoehdot_tietosuojaseloste.pdf';
export const USER_GUIDE_URL = 'https://www.sos-lapsikyla.fi/fileadmin/user_upload/tiedostot/kuvat/pdf-tiedostot/Ylitse-vertaismentorointiopas-web.pdf';
export const FEEDBACK_URL = 'https://forms.office.com/Pages/ResponsePage.aspx?id=UGBhp7WfhU21Tu69_G6kO5HyhsO7ItpBu8SARGpVxBdURTBPRk05Wk8yMTlNTlZFRERNTjY4N0lPMC4u';

export default API_URL;
