Ylitse Mobile README
====================

[Ylitse Project][], started by SOS Children's Villages Finland, aims at
reducing the need for intergenerational child protecting services by
supporting young parents with a foster care background.

Ylitse Mobile is a mobile client for [Ylitse API][]. It is intended to
be used by the target group of Ylitse Project.

[Ylitse Project]: https://www.sos-lapsikyla.fi/mita-me-teemme/kehittamistyo/ylitse-projekti/
[Ylitse API]: https://gitlab.com/ylitse/ylitse-api/

Dependencies
------------

Ylitse Mobile is a [React Native][] application so you will need [npm][]
package manager to install dependencies and `react-native-cli` to run some
command line commands. It is also recommended to have [make][] in order to run
preconfigured targets.

The project depends on the following libraries:

* [Babel][]
* [ESLint][]
* [Jest][]
* [React][]
* [React Native][]
* [React Navigation][]

To have them installed just run:

    make install

[React Native]: https://facebook.github.io/react-native/
[npm]: https://www.npmjs.com/
[make]: https://www.gnu.org/software/make/
[Babel]: https://babeljs.io/
[ESLint]: https://eslint.org/
[Jest]: https://facebook.github.io/jest/
[React]: https://reactjs.org/
[React Navigation]: https://reactnavigation.org/

Usage
-----

Run the app on emulator or physical device (if connected):

    make run-android

Start a packager instance:

    make start

Development
-----------

Ylitse Mobile in written in JavaScript ES6 following [Airbnb style guide][]
(with some small exceptions, see `.eslintrc`). Always make sure that your code
passes all style tests:

    make lint

[Airbnb style guide]: https://github.com/airbnb/javascript

Releasing
---------

To bump a version, edit `package.json` and NEWS, where [ISO 8601][]
formatted dates should be used:

    0.1.1 (2017-12-10)

After a release bump the version again with `+git` suffix and start a new NEWS
entry:

    0.1.1+git (unreleased)

Always tag releases:

    git tag v$(npm run version --silent)
    git push --tags

[ISO 8601]: https://www.iso.org/iso-8601-date-and-time-format.html

Deployment
----------

FIXME

License
-------

Ylitse Mobile is offered under the MIT license.
