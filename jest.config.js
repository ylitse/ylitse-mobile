const config = {
    preset: 'react-native',
    collectCoverageFrom: ['src/**/*.js'],
    testPathIgnorePatterns: ['/node_modules/'],
    transform: {
        '^.+\\.js$': '<rootDir>/node_modules/react-native/jest/preprocessor.js',
    },
    transformIgnorePatterns: [
        'node_modules/(?!react-native|native-base|react-navigation)',
    ],
    setupFiles: ['./enzyme.config.js'],
    coverageReporters: ['text-summary', 'html'],
};

module.exports = config;
